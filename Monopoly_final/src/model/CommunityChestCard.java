/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *<p>CommunityChestCard.java - Meglepetéskártyákat megvalósító osztály</p>
 *
 *<p>Monopolyban két fő kártyatípust különböztetünk meg a szerencsekártyát és a meglepetéskártyát.
 * Ez az osztály a meglepetéskártyákat valósítja meg.</p>
 * 
 * @see Card
 * 
 * @author Osbáth Gergely
 */
public class CommunityChestCard extends Card{
    
    public CommunityChestCard(String text, int moveTo, int amount) {
        super(text, moveTo, amount);
    }
    
}
