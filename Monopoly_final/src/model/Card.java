/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *<p>Card.java - Kártyákat megvalósító osztály</p>
 *<p>Ennek az osztálynak a segítségével valósítjuk meg a kártyahúzás akcióját.</p>
 * <p>Adattagjai:</p>
 * <ul>
 *  <li>Text: Kártyahúzás után egy felugró ablakban megjelenik a kártya szövege.</li>
 *  <li>moveTo: Ha a játékos a kártya hatására lépnie kell, ezzel a változóval adjuk meg, hogy mennyit. (Egyébként 0)</li>
 *  <li>amount: Ha a játékos a kártya hatására pénzt kap/pénz kell levonni tőle ezzel a változóval adjuk meg, hogy mennyit.(Egyébként 0)</li>
 * </ul>
 * @author Osbáth Gergely
 */
public class Card {
    
    private String text;
    private int moveTo;
    private int amount;

    public Card(String text, int moveTo, int amount) {
        this.text = text;
        this.moveTo = moveTo;
        this.amount = amount;
    }

    public String getText() {
        return text;
    }

    public int getMoveTo() {
        return moveTo;
    }

    public int getAmount() {
        return amount;
    }
    
    
}
