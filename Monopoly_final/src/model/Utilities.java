/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import view.Tile;
import view.TileType;

/**
 *<p>Utilities.java - A Utility típusú mezők osztálya.</p>
 * 
 *<p>A Tile osztályból származó Utility mezőre specializálódott osztály.</p>
 *<p>Adattagjai:</p>
 *<ul>
 * <li>name: A mező neve</li>
 * <li>price: Ár</li>
 * <li>mortgageValue: Jelzálog értéke (75)</li>
 *</ul>
 *
 * 
 * @see Tile
 * @see TileType
 * 
 * @author Osbáth Gergely
 */
public class Utilities extends Tile{
    private int price;
    private int mortgageValue;
    private boolean mortgaged;
    
    public Utilities(String name, int price, TileType type){
        super(type);
        super.setName(name);
        this.price = price;
        this.mortgageValue = 75;
       
    }
    
    public boolean isMortgaged() {
        return mortgaged;
    }

    public void setMortgaged(boolean mortgaged) {
        this.mortgaged = mortgaged;
    }

    public int getMortgageValue() {
        return mortgageValue;
    }

    public int getPrice() {
        return price;
    }
}
