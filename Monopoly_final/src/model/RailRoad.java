/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import view.Tile;
import view.TileType;

/**
 *<p>RailRoad.java - A RailRoad típusú mezők osztálya.</p>
 * 
 *<p>A Tile osztályból származó RailRoad mezőre specializálódott osztály.</p>
 *<p>Adattagjai:</p>
 *<ul>
 * <li>name: A mező neve</li>
 * <li>price: Ár</li>
 * <li>rent: Bérleti díj</li>
 * <li>mortgageValue: Jelzálog értéke (75)</li>
 * <li>mortgaged: Jelzálog alatt áll-e</li>
 *</ul>
 *
 * 
 * @see Tile
 * @see TileType
 * 
 * @author Osbáth Gergely
 */
public class RailRoad extends Tile{
    private int rent;
    private int price;
    private int mortgageValue;
    private boolean mortgaged;
    
    public RailRoad(String name, int price, TileType type){
        super(type);
        super.setName(name);
        this.price = price;
        this.rent = 25;
        this.mortgageValue = 100;
        this.mortgaged = false;
    }
    
    public boolean isMortgaged() {
        return mortgaged;
    }

    public void setMortgaged(boolean mortgaged) {
        this.mortgaged = mortgaged;
    }

    public int getMortgageValue() {
        return mortgageValue;
    }

    public int getPrice() {
        return price;
    }

    public int getRent() {
        return rent;
    }

    public void setRent(int rent) {
        this.rent = rent;
    }
    

}
