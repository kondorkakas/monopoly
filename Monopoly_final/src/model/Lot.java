/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import view.Tile;
import view.TileGroup;
import view.TileType;

/**
 *<p>Lot.java - A Lot típusú mezők osztálya.</p>
 * 
 *<p>A Tile osztályból származó Lot mezőre specializálódott osztály.</p>
 *<p>Ezek azok a mezők, amelyekre lehet építkezni is.</p>
 *<p>Adattagjai:</p>
 *<ul>
 * <li>name: A mező neve</li>
 * <li>price: Ár</li>
 * <li>houses: Házak száma (max. 4/mező)</li>
 * <li>hotels: Hotelek száma (max. 1/mező)</li>
 * <li>rent: Bérleti díj</li>
 * <li>canBuild: Beépíthető-e</li>
 * <li>defaultRent: Alap bérleti díj (házak számától függően növekszik a bérleti díj)</li>
 * <li>mortgageValue: Jelzálog értéke</li>
 * <li>group: Csoport (TileGroup)</li>
 * <li>buildedInThisRound: Ebben a körben építkeztek-e rajta</li>
 *</ul>
 *
 * 
 * @see Tile
 * @see TileType
 * @see TileGroup
 * 
 * @author Osbáth Gergely
 */
public class Lot extends Tile {

    private int price;
    private double rent;
    private double defaultRent;
    private TileGroup group;
    private int houses;
    private int buildPrice;
    private int hotel;
    private boolean mortgaged;
    private int mortgageValue;
    private boolean buildedInThisRound;
    private boolean canBuild;

    public Lot(String name, int price, int rent, TileType type, TileGroup group, int mortgageValue) {
        super(type);
        super.setName(name);
        this.price = price;
        this.houses = 0;
        this.hotel = 0;
        this.rent = rent;
        this.canBuild = false;
        this.mortgaged = false;
        this.defaultRent = rent;
        this.mortgageValue = mortgageValue;
        this.group = group;
        calculatePrice(group);
        this.buildedInThisRound = true;
    }

    public boolean isBuildedInThisRound() {
        return buildedInThisRound;
    }

    public void setBuildedInThisRound(boolean buildedInThisRound) {
        this.buildedInThisRound = buildedInThisRound;
    }

    public boolean isCanBuild() {
        return canBuild;
    }

    public void setCanBuild(boolean canBuild) {
        this.canBuild = canBuild;
    }
    
    public int getPrice() {
        return price;
    }

    public double getRent() {
        return rent;
    }

    public void setRent(int rent) {
        this.rent = rent;
    }

    public boolean isMortgaged() {
        return mortgaged;
    }

    public void setMortgaged(boolean mortgaged) {
        this.mortgaged = mortgaged;
    }

    public int getMortgageValue() {
        return mortgageValue;
    }

    public int getBuildPrice() {
        return buildPrice;
    }

    public void setBuildPrice(int buildPrice) {
        this.buildPrice = buildPrice;
    }

    public int getHotel() {
        return hotel;
    }

    public int getHouse() {
        return houses;
    }

    public void setHouses(int houses) {
        this.houses = houses;
    }

    public void setHotel(int hotel) {
        this.hotel = hotel;
    }

    public void calculatePrice(TileGroup g) {
        switch (g) {
            case G1:
            case G2:
                this.buildPrice = 50;
                calculateLotRent();
                break;
            case G3:
            case G4:
                this.buildPrice = 100;
                calculateLotRent();
                break;
            case G5:
            case G6:
                this.buildPrice = 150;
                calculateLotRent();
                break;
            case G7:
            case G8:
                this.buildPrice = 200;
                calculateLotRent();
                break;
        }

    }
    
    public void calculateLotRent(){
        if(this.houses == 1){
            this.rent = this.rent * 5;
            System.out.println("1 house: " + this.rent);
        }
        if(this.houses == 2){
            this.rent = this.defaultRent;
            this.rent = this.rent * 5 * 3;
            System.out.println("2 house: " + this.rent);
        }
        if(this.houses == 3){
            this.rent = this.defaultRent;
            this.rent = this.rent * 5 * 3 * 3;
            System.out.println("3 house: " + this.rent);
        }
        if(this.houses == 4){
            this.rent = this.defaultRent;
            this.rent = this.rent * 5 * 3 * 3 * 1.5;
            System.out.println("4 house: " + this.rent);
        }
        if(this.hotel == 1){
            this.rent = this.defaultRent;
            this.rent = this.rent * 5 * 3 * 3 * 1.5 * 1.5;
            System.out.println("1 hotel: " + this.rent);
        }

    }

    public TileGroup getGroup(){
        return this.group;
    }
}
