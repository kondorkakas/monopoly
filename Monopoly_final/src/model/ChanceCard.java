/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import monopoly_final.Monopoly_final;

/**
 *<p>ChanceCard.java - Szerencsekártyákat megvalósító osztály</p>
 *<p>Monopolyban két fő kártyatípust különböztetünk meg a szerencsekártyát és a meglepetéskártyát.
 * Ez az osztály a szerencsekártyákat valósítja meg.</p>
 * 
 * @see Card
 * 
 * @author Osbáth Gergely
 */
public class ChanceCard extends Card{

    public ChanceCard(String text, int moveTo, int amount) {
        super(text, moveTo, amount);
    }

}
