/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import view.Tile;
import view.TileType;

/**
 *<p>OtherTiles.java - Az egyik TileType alá sem tartozó mezők osztálya.</p>
 * 
 *<p>A Tile osztályból származó egyik TileType (Mező típus) alá sem tartozó osztály.</p>
 *<p>Adattagjai:</p>
 *<ul>
 * <li>A mező neve</li>
 *</ul>
 *
 * 
 * @see Tile
 * @see TileType
 * 
 * @author Osbáth Gergely
 */
public class OtherTiles extends Tile{
    
    public OtherTiles(String name, TileType type){
        super(type);
        super.setName(name);
    }
}
