/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Random;
import java.util.ArrayList;
import java.awt.Color;
import java.util.Collections;
import javax.swing.JOptionPane;
import monopoly_final.Monopoly_final;
import view.*;

/**
 * <p>Player.java - A játékost megvalósító osztály.</p>
 *
 *<p>Főbb adattagjai:</p>
 *<ul>
 * <li>A játékos neve</li>
 * <li>Egyenlege</li>
 * <li>Börtönben van-e</li>
 * <li>Csődbe ment-e</li>
 * <li>Börtönben töltendő körök száma</li>
 * <li>Pozíciója a táblán</li>
 * <li>Szabadulás a börtönből kártyák száma</li>
 *</ul>
 * 
 * 
 * @author Osbáth Gergely
 */
public class Player {

    private final String name;
    private int balance;
    private boolean reThrow;
    private boolean throwedYet;
    private boolean inJail;
    private boolean bankrupt;
    private int turnsInJail;
    private int positionOnBoard;
    private Color color;
    private int playerID;
    private static int playerCounter = 0;
    private int outOfJailCards;

    private ArrayList<Tile> ownedTiles;
/**
 * <p>A játékost létrehozó konstruktor.</p>
 * <p>Csak a játékos nevét kérjük be, mivel a többi adattagnak alapértelmezett értéke van.</p>
 * @param name A játékos neve
 */
    public Player(String name) {
        this.name = name;
        this.balance = 1500;
        this.reThrow = false;
        this.throwedYet = false;
        this.inJail = false;
        this.bankrupt = false;
        this.positionOnBoard = 0;
        this.turnsInJail = 0;
        this.outOfJailCards = 0;
        this.playerID = playerCounter;
        this.ownedTiles = new ArrayList<Tile>();

        switch (Player.playerCounter) {
            case 0:
                this.color = Color.RED;
                break;
            case 1:
                this.color = Color.BLUE;
                break;
            case 2:
                this.color = Color.GREEN;
                break;
            case 3:
                this.color = Color.YELLOW;
                break;
            case 4:
                this.color = Color.MAGENTA;
                break;
            case 5:
                this.color = Color.ORANGE;
                break;
            default:
                break;
        }
        playerCounter++;
    }

    public boolean isThrowedYet() {
        return throwedYet;
    }

    public void setThrowedYet(boolean throwedYet) {
        this.throwedYet = throwedYet;
    }

    public boolean isBankrupt() {
        return bankrupt;
    }

    public void setBankrupt(boolean bankrupt) {
        this.bankrupt = bankrupt;
    }
    
    public ArrayList<Tile> getOwnedTiles() {
        return ownedTiles;
    }

    public String getName() {
        return this.name;
    }

    public int getPlayerID() {
        return this.playerID;
    }

    public int getBalance() {
        return this.balance;
    }

    public boolean getReThrow() {
        return this.reThrow;
    }

    public void setReThrowFalse() {
        this.reThrow = false;
    }

    public void setReThrowTrue() {
        this.reThrow = true;
    }

    public boolean isInJail() {
        return inJail;
    }

    public void setInJail(boolean inJail) {
        this.inJail = inJail;
    }

    public int getTurnsInJail() {
        return turnsInJail;
    }

    public void setTurnsInJail(int turnsInJail) {
        this.turnsInJail = turnsInJail;
    }

    public int getOutOfJailCards() {
        return outOfJailCards;
    }

    public void setOutOfJailCards(int outOfJailCards) {
        this.outOfJailCards = outOfJailCards;
    }

    public int getPositionOnBoard() {
        return this.positionOnBoard;
    }

    public void setPositionOnBoard(int x) {
        this.positionOnBoard = x;
    }

    public Color getColor() {
        return this.color;
    }

    public void addToOwnedTiles(Tile tile) {
        this.ownedTiles.add(tile);
    }

    /**
     * <p>Ez a függvény feldolgozza a soronkövetkező meglepetéskártyát, ha a pakli elfogyott újrakeveri és a tetejéről húz lapot</p>
     * @param board 'Melyik' táblán lévő meglepetéskártyákkal dolgozunk
     */
    public void processNextCommunityChestCard(Board board) {

        if (Monopoly_final.DeckOfCommunityChestCards.isEmpty()) {
            System.out.println("Reshufflin...");
            Monopoly_final.DeckOfCommunityChestCards.addAll(Monopoly_final.DeckOfDiscardedCommunityChestCards);
            Collections.shuffle(Monopoly_final.DeckOfCommunityChestCards);
            Monopoly_final.DeckOfDiscardedCommunityChestCards.clear();
        }

        JOptionPane.showMessageDialog(Monopoly_final.frame, Monopoly_final.DeckOfCommunityChestCards.get(0).getText());

        // get out of jail card
        if (Monopoly_final.DeckOfCommunityChestCards.get(0).getText().equals("Get out of Jail Free. This card may be kept until needed, or traded/sold.")) {
            this.outOfJailCards = this.outOfJailCards + 1;

        }

        //collect 50 from every player
        if (Monopoly_final.DeckOfCommunityChestCards.get(0).getText().equals("Grand Opera Night. Collect $50 from every player for opening night seats.")) {
            for (int i = 0; i < Monopoly_final.players.size(); i++) {
                if (Monopoly_final.players.get(i) != Monopoly_final.getPlayerById(Monopoly_final.currentPlayer)) {
                    Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).increaseMoney(50);
                    Monopoly_final.players.get(i).decreaseMoney(50);
                }
            }
        }

        // collect 10 from every player
        if (Monopoly_final.DeckOfCommunityChestCards.get(0).getText().equals("It is your birthday. Collect $10 from every player.")) {
            for (int i = 0; i < Monopoly_final.players.size(); i++) {
                if (Monopoly_final.players.get(i) != Monopoly_final.getPlayerById(Monopoly_final.currentPlayer)) {
                    Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).increaseMoney(10);
                    Monopoly_final.players.get(i).decreaseMoney(10);
                }
            }
        }
        //Pay 40 per house and $115 per hotel you own.
        if (Monopoly_final.DeckOfChanceCards.get(0).getText().equals("You are assessed for street repairs: Pay $40 per house and $115 per hotel you own.")) {
            int sumOfHouses = 0;
            int sumOfHotels = 0;
            for (int i = 0; i < Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).getOwnedTiles().size(); i++) {
                if (Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).getOwnedTiles().get(i).getType() == TileType.LOT) {
                    Lot lot = (Lot) Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).getOwnedTiles().get(i);
                    sumOfHouses = sumOfHouses + lot.getHouse();
                    sumOfHotels = sumOfHotels + lot.getHotel();
                }
            }
            Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).decreaseMoney(sumOfHouses * 40);
            Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).decreaseMoney(sumOfHotels * 115);
        }

        // Basic card process
        board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(null);
        if (Monopoly_final.DeckOfCommunityChestCards.get(0).getMoveTo() != -1) {
            if (Monopoly_final.DeckOfCommunityChestCards.get(0).getMoveTo() < getPositionOnBoard()) {
                increaseMoney(200);
            }
            setPositionOnBoard(Monopoly_final.DeckOfCommunityChestCards.get(0).getMoveTo());

        }
        board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(this.color);
        if (Monopoly_final.DeckOfCommunityChestCards.get(0).getAmount() > 0) {
            increaseMoney(Monopoly_final.DeckOfCommunityChestCards.get(0).getAmount());
        } else {
            increaseMoney(Monopoly_final.DeckOfCommunityChestCards.get(0).getAmount());
        }

        Monopoly_final.DeckOfDiscardedCommunityChestCards.add(Monopoly_final.DeckOfCommunityChestCards.get(0));
        Monopoly_final.DeckOfCommunityChestCards.remove(0);

        buttonAllowance(board);

        updateGui();
    }
    /**
     * <p>Ez a függvény feldolgozza szerencsekártya pakli következő lapját, ha a pakli elfogyott újrakeveri és a tetejéről húz egy lapot.</p>
     * @param board 'Melyik' táblán lévő szerencsekártyákkal dolgozunk
     */
    public void processNextCard(Board board) {

        //Reshuffle cards if deck is empty.
        if (Monopoly_final.DeckOfChanceCards.isEmpty()) {
            System.out.println("Reshufflin...");
            //Collections.copy(Monopoly_final.DeckOfDiscardedChanceCards,Monopoly_final.DeckOfChanceCards);
            Monopoly_final.DeckOfChanceCards.addAll(Monopoly_final.DeckOfDiscardedChanceCards);
            Collections.shuffle(Monopoly_final.DeckOfChanceCards);
            Monopoly_final.DeckOfDiscardedChanceCards.clear();
        }

        JOptionPane.showMessageDialog(Monopoly_final.frame, Monopoly_final.DeckOfChanceCards.get(0).getText());

        // UNIQUE CARD PROCESSES
        //Advance to nearest Railroad
        if (Monopoly_final.DeckOfChanceCards.get(0).getText().equals("Advance token to the nearest Railroad and pay owner twice the rental to which he/she {he} is otherwise entitled. If Railroad is unowned, you may buy it from the Bank.")) {
            if (getPositionOnBoard() < 5) {
                board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(null);
                setPositionOnBoard(5);
                board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(this.color);
            } else if (getPositionOnBoard() < 15) {
                board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(null);
                setPositionOnBoard(15);
                board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(this.color);
            } else if (getPositionOnBoard() < 25) {
                board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(null);
                setPositionOnBoard(25);
                board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(this.color);
            } else if (getPositionOnBoard() < 35) {
                board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(null);
                setPositionOnBoard(35);
                board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(this.color);
            } else if (getPositionOnBoard() <= 39) {
                board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(null);
                setPositionOnBoard(5);
                board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(this.color);
            }
            RailRoad railRoad = (RailRoad) board.getTiles().get(this.positionOnBoard);
            if (railRoad.getOwnedBy() != null) {
                int counter = 0;
                for (int i = 0; i < railRoad.getOwnedBy().ownedTiles.size(); i++) {
                    if (railRoad.getOwnedBy().ownedTiles.get(i).getType() == TileType.RAILROAD) {
                        counter++;
                    }
                }
                switch (counter) {
                    case 1:
                        decreaseMoney(25 * 2);
                        increaseMoneyOfPlayer(railRoad.getOwnedBy(), 25 * 2);
                        break;
                    case 2:
                        decreaseMoney(50 * 2);
                        increaseMoneyOfPlayer(railRoad.getOwnedBy(), 50 * 2);
                        break;
                    case 3:
                        decreaseMoney(100 * 2);
                        increaseMoneyOfPlayer(railRoad.getOwnedBy(), 100 * 2);
                        break;
                    case 4:
                        decreaseMoney(200 * 2);
                        increaseMoneyOfPlayer(railRoad.getOwnedBy(), 200 * 2);
                        break;
                }
            }

        }

        //Advance to nearest Utility
        if (Monopoly_final.DeckOfChanceCards.get(0).getText().equals("Advance token to nearest Utility. If unowned, you may buy it from the Bank. If owned, throw dice and pay owner a total 10 times the amount thrown.")) {
            if (getPositionOnBoard() < 12) {
                board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(null);
                setPositionOnBoard(12);
                board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(this.color);
            } else if (getPositionOnBoard() < 28) {
                board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(null);
                setPositionOnBoard(28);
                board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(this.color);
            } else if (getPositionOnBoard() <= 39) {
                board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(null);
                setPositionOnBoard(12);
                board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(this.color);
            }

            Random firstRandom = new Random();
            Random secondRandom = new Random();
            int firstDice = firstRandom.nextInt((6 - 1) + 1) + 1;
            int secondDice = secondRandom.nextInt((6 - 1) + 1) + 1;

            int sum = firstDice + secondDice;
            sum = sum * 10;

            decreaseMoney(sum);
            increaseMoneyOfPlayer(board.getTiles().get(this.positionOnBoard).getOwnedBy(),sum);

        }

        //Go back 3 spaces.
        if (Monopoly_final.DeckOfChanceCards.get(0).getText().equals("Go back 3 spaces.")) {

            board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(null);
            setPositionOnBoard(this.positionOnBoard - 3);
            board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(this.color);

        }
        //Get out of Jail free
        if (Monopoly_final.DeckOfChanceCards.get(0).getText().equals("Get out of Jail Free. This card may be kept until needed, or traded/sold.")) {

            this.outOfJailCards = this.outOfJailCards + 1;
        }

        //Pay 25 for each house and 100 for each hotel.
        if (Monopoly_final.DeckOfChanceCards.get(0).getText().equals("Make general repairs on all your property: For each house pay $25, For each hotel $100.")) {
            int sumOfHouses = 0;
            int sumOfHotels = 0;
            for (int i = 0; i < Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).getOwnedTiles().size(); i++) {
                if (Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).getOwnedTiles().get(i).getType() == TileType.LOT) {
                    Lot lot = (Lot) Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).getOwnedTiles().get(i);
                    sumOfHouses = sumOfHouses + lot.getHouse();
                    sumOfHotels = sumOfHotels + lot.getHotel();
                }
            }
            Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).decreaseMoney(sumOfHouses * 25);
            Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).decreaseMoney(sumOfHotels * 100);
        }

        //Pay each player $50.
        if (Monopoly_final.DeckOfChanceCards.get(0).getText().equals("You have been elected Chairman of the Board. Pay each player $50.")) {
            for (int i = 0; i < Monopoly_final.players.size(); i++) {
                if (Monopoly_final.players.get(i) != Monopoly_final.getPlayerById(Monopoly_final.currentPlayer)) {
                    Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).decreaseMoney(50);
                    Monopoly_final.players.get(i).increaseMoney(50);
                }
            }
        }

        // BASIC CARD PROCESS
        board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(null);
        if (Monopoly_final.DeckOfChanceCards.get(0).getMoveTo() != -1) {
            if (Monopoly_final.DeckOfChanceCards.get(0).getMoveTo() < getPositionOnBoard()) {
                increaseMoney(200);
            }
            setPositionOnBoard(Monopoly_final.DeckOfChanceCards.get(0).getMoveTo());

        }
        board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(this.color);
        if (Monopoly_final.DeckOfChanceCards.get(0).getAmount() > 0) {
            increaseMoney(Monopoly_final.DeckOfChanceCards.get(0).getAmount());
        } else {
            increaseMoney(Monopoly_final.DeckOfChanceCards.get(0).getAmount());
        }

        Monopoly_final.DeckOfDiscardedChanceCards.add(Monopoly_final.DeckOfChanceCards.get(0));
        Monopoly_final.DeckOfChanceCards.remove(0);

        buttonAllowance(board);

        updateGui();

    }
    /**
     * <p>Ez a függvény felelős a játékos bábújának a pályán való mozgatásáért, illetve ha a játékos áthalad a 'Start' mezőn az ezért járó bónusz kifizetéséért.</p>
     * @param throwResult a dobás eredménye
     * @param board 'Melyik' táblán lévő játékost mozgatjuk
     */

    public void movePlayer(int throwResult, Board board) {
        board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(null);
        if (this.getPositionOnBoard() + throwResult <= 39) {
            this.positionOnBoard += throwResult;
        } else {
            int diff = 39 - (this.getPositionOnBoard() + throwResult) + 1;
            setPositionOnBoard(0);
            if (Math.abs(diff) >= 1) {
                movePlayer(Math.abs(diff), board);
                increaseMoney(200);
            }
        }
        board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(this.color);

        buttonAllowance(board);

        updateGui();

    }
    /**
     * <p>Ezt a függvényt csak a csalófelületen használjuk. Egy játékost egy adott pozicíóba mozgathatunk vele.</p>
     * @param position A pozíció ahova helyezzük a játékos bábúját.
     */

    public void cheatMove(int position) {

        if (position >= 40) {
            JOptionPane.showMessageDialog(Monopoly_final.frame, "Go to must be between 0 and 39!");
        } else {

            Monopoly_final.board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(null);
            if (this.positionOnBoard > position) {
                increaseMoney(200);
            }
            setPositionOnBoard(position);
            Monopoly_final.board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(this.color);
            buttonAllowance(Monopoly_final.board);
            updateGui();
        }
    }

    /**
     * <p>A telekvásárlást megvalósító függvény</p>
     * <p>Leellenőrzi, hogy minden feltétel adott-e ahhoz, hogy a játékos megvásároljon egy telket, és ha igen hozzáadja az 'ownedTiles' tömbhöz, az árát pedig levonja a játékos egyenlegéből.
     * @param board 'Melyik' táblán lévő mezőt vásároljuk meg
     */
    public void buyTile(Board board) {
        int counter = 0;
        System.out.println(board.getInfo().get(this.positionOnBoard).getText());        
        if (board.getTiles().get(this.positionOnBoard).getType() == TileType.LOT) {
            Lot lot = (Lot) board.getTiles().get(this.positionOnBoard);
            if (lot.getOwnedBy() == null) {
                lot.setOwnedBy(this);
                addToOwnedTiles(lot);
                board.getInfo().get(this.positionOnBoard).setText(this.name);
                decreaseMoney(lot.getPrice());
                lot.setBuildedInThisRound(true);
                Lot tempLot;
                for (int i = 0; i < this.ownedTiles.size(); i++) {
                    if (this.ownedTiles.get(i).getType().equals(TileType.LOT)) {
                        tempLot = (Lot) this.ownedTiles.get(i);
                        if (tempLot.getGroup().equals(lot.getGroup())) {
                            counter += 1;
                        }
                    }
                }
                if (counter == 2 && (lot.getGroup().equals(TileGroup.G1) || lot.getGroup().equals(TileGroup.G8))) {
                    for (int i = 0; i < this.ownedTiles.size(); i++) {
                        if (this.ownedTiles.get(i).getType().equals(TileType.LOT)) {
                            tempLot = (Lot) this.ownedTiles.get(i);
                            if (tempLot.getGroup().equals(lot.getGroup())) {
                                tempLot.setCanBuild(true);
                            }
                        }
                    }
                } else if (counter == 3 && (lot.getGroup().equals(TileGroup.G2) || lot.getGroup().equals(TileGroup.G3)
                        || lot.getGroup().equals(TileGroup.G4) || lot.getGroup().equals(TileGroup.G5)
                        || lot.getGroup().equals(TileGroup.G6) || lot.getGroup().equals(TileGroup.G7))) {
                    for (int i = 0; i < this.ownedTiles.size(); i++) {
                        if (this.ownedTiles.get(i).getType().equals(TileType.LOT)) {
                            tempLot = (Lot) this.ownedTiles.get(i);
                            if (tempLot.getGroup().equals(lot.getGroup())) {
                                tempLot.setCanBuild(true);
                            }
                        }
                    }
                }
                JOptionPane.showMessageDialog(Monopoly_final.frame, "You have bought: " + lot.getName() + " for " + lot.getPrice());
            } else {
                JOptionPane.showMessageDialog(Monopoly_final.frame, board.getTiles().get(this.positionOnBoard).getName() + " is already owned by " + board.getTiles().get(this.positionOnBoard).getOwnedBy().getName());
            }
        }
        if (board.getTiles().get(this.positionOnBoard).getType() == TileType.RAILROAD) {
            RailRoad railRoad = (RailRoad) board.getTiles().get(this.positionOnBoard);
            if (railRoad.getOwnedBy() == null) {
                railRoad.setOwnedBy(this);
                addToOwnedTiles(railRoad);
                board.getInfo().get(this.positionOnBoard).setText(this.name);
                decreaseMoney(railRoad.getPrice());
                JOptionPane.showMessageDialog(Monopoly_final.frame, "You have bought: " + railRoad.getName() + " for " + railRoad.getPrice());
            } else {
                JOptionPane.showMessageDialog(Monopoly_final.frame, board.getTiles().get(this.positionOnBoard).getName() + " is already owned by " + board.getTiles().get(this.positionOnBoard).getOwnedBy().getName());
            }
        }
        if (board.getTiles().get(this.positionOnBoard).getType() == TileType.UTILITIES) {
            Utilities utilities = (Utilities) board.getTiles().get(this.positionOnBoard);
            if (utilities.getOwnedBy() == null) {
                utilities.setOwnedBy(this);
                addToOwnedTiles(utilities);
                board.getInfo().get(this.positionOnBoard).setText(this.name);
                decreaseMoney(utilities.getPrice());
                JOptionPane.showMessageDialog(Monopoly_final.frame, "You have bought: " + utilities.getName() + " for " + utilities.getPrice());
            } else {
                JOptionPane.showMessageDialog(Monopoly_final.frame, board.getTiles().get(this.positionOnBoard).getName() + " is already owned by " + board.getTiles().get(this.positionOnBoard).getOwnedBy().getName());
            }
        }
        updateGui();
    }
    /**
     *Ez a függvény valósítja meg a telekre történő házépítést.
     * <p>Ellenőrzi a feltételeket, majd ha azok adottak megépíti a telekre a házat vagy hotelt</p>
     */
    public void buildHouse() {
        if (Monopoly_final.board.getTiles().get(this.positionOnBoard).getType().equals(TileType.LOT)) {
            Lot lot = (Lot) Monopoly_final.board.getTiles().get(this.positionOnBoard);
            if (lot.getOwnedBy() == this) {
                if (!lot.isBuildedInThisRound()) {
                    if (isThrowedYet()) {
                        if (lot.getHotel() == 0) {
                            if (lot.isCanBuild()) {
                                if (lot.getHouse() < 4 && lot.getHotel() == 0) {
                                    decreaseMoney(lot.getBuildPrice());
                                    lot.setHouses(lot.getHouse() + 1);
                                    lot.calculatePrice(lot.getGroup());
                                } else {
                                    lot.setHouses(0);
                                    lot.setHotel(1);
                                    lot.calculatePrice(lot.getGroup());
                                }
                                lot.setBuildedInThisRound(true);
                                Monopoly_final.board.getHouseNumber().get(this.positionOnBoard).setText(Integer.toString(lot.getHouse()) + ", " + Integer.toString(lot.getHotel()));
                                JOptionPane.showMessageDialog(Monopoly_final.frame, "Building complete. Number of houses on Lot: " + lot.getHouse() + " Hotels built: " + lot.getHotel());
                            } else {
                                JOptionPane.showMessageDialog(Monopoly_final.frame, "You cannot build until al the lots from this group are yours!");
                            }
                        } else {
                            JOptionPane.showMessageDialog(Monopoly_final.frame, "You cannot build anything else on this lot!");
                        }
                    } else {
                        JOptionPane.showMessageDialog(Monopoly_final.frame, "You have to throw before building!");
                    }
                } else {
                    JOptionPane.showMessageDialog(Monopoly_final.frame, "You already bulit in this round!");
                }
            } else {
                JOptionPane.showMessageDialog(Monopoly_final.frame, "You do not own this Lot.");
            }
            updateGui();
        }
    }

    public void setBalance(int value) {
        balance += value;
        updateGui();
    }

    public void increaseMoney(int amount) {
        this.balance = this.balance + amount;
    }

    public void increaseMoney(double amount) {
        this.balance = this.balance + (int) amount;
    }

    public void increaseMoneyOfPlayer(Player player, int amount) {
        player.increaseMoney(amount);
    }

    public void increaseMoneyOfPlayer(Player player, double amount) {
        player.increaseMoney((int) amount);
    }

    public void decreaseMoney(int amount) {
        this.balance = this.balance - amount;
    }

    public void decreaseMoney(double amount) {
        this.balance = this.balance - (int) amount;
    }

    public void decreaseMoneyOfPlayer(Player player, int amount) {
        player.decreaseMoney(amount);
    }

    public void decreaseMoneyOfPlayer(Player player, double amount) {
        player.decreaseMoney((int) amount);
    }

    /**
     * <p>Kockadobást szimuláló függvény.</p>
     * <p>Generál két véletlen számot (1-6), ezeket összeadja, majd visszaadja.</p>
     * <p>Abban az esetben, ha a két véletlen szám megegyezik, a játékos újra dobhat.</p>
     * @param board 'Melyik' táblán dobunk.
     * @return throwResult - A kockadobás eredménye
     */
    public int throwDice(Board board) {
        int firstDice, secondDice;
        Random firstRandom = new Random();
        Random secondRandom = new Random();
        firstDice = firstRandom.nextInt((6 - 1) + 1) + 1;
        secondDice = secondRandom.nextInt((6 - 1) + 1) + 1;
        //firstDice = 0;
        //secondDice = 1;

        if (firstDice == secondDice) {
            setReThrowTrue();
        }
        int sum = firstDice + secondDice;
        movePlayer(sum, board);
        this.setThrowedYet(true);

        Monopoly_final.frame.getThrowDiceLabel().setText("<html>Dice #1: " + Integer.toString(firstDice) + "<br>Dice #2: " + Integer.toString(secondDice)
                + "<br>Sum: " + Integer.toString(sum) + "<br>" + Monopoly_final.board.getTiles().get(this.positionOnBoard).getName() + "</html>");
        if (firstDice == secondDice && !this.inJail) {
            Monopoly_final.frame.getThrowDiceLabel().setText(Monopoly_final.frame.getThrowDiceLabel().getText().substring(0, (Monopoly_final.frame.getThrowDiceLabel().getText().length() - 7)) + "<br>Throw Again</html>");
        }
        return firstDice + secondDice;
    }

    public int throwDiceForUtilities() {
        int firstDice, secondDice;
        Random firstRandom = new Random();
        Random secondRandom = new Random();
        firstDice = firstRandom.nextInt((6 - 1) + 1) + 1;
        secondDice = secondRandom.nextInt((6 - 1) + 1) + 1;

        if (firstDice == secondDice) {
            setReThrowTrue();
        }

        int sum = firstDice + secondDice;

        Monopoly_final.frame.getThrowDiceLabel().setText("<html>Dice #1: " + Integer.toString(firstDice) + "<br>Dice #2: " + Integer.toString(secondDice)
                + "<br>Sum: " + Integer.toString(sum) + "</html>");

        return firstDice + secondDice;
    }

    public void updateGui() {
        Monopoly_final.frame.getPlayer1_Name().setText(getName());
        Monopoly_final.frame.getPlayer1_Balance().setText("" + getBalance());
        int turnInJail = getTurnsInJail() + 1;
        if (isInJail()) {
            Monopoly_final.frame.getPlayer1_InJail().setText("Yes" + "(" + turnInJail + ")");
        } else {
            Monopoly_final.frame.getPlayer1_InJail().setText("No");
        }
    }

    public void buttonAllowance(Board board) {
        if (board.getTiles().get(this.positionOnBoard).getType() == TileType.TAX) {
            Monopoly_final.frame.getjButton2().setEnabled(false);
            Monopoly_final.frame.getjButton3().setEnabled(false);
            Monopoly_final.frame.getjButton5().setEnabled(false);
            if (this.positionOnBoard == 4) {
                decreaseMoney(200);
            }
            if (this.positionOnBoard == 38) {
                decreaseMoney(100);
            }
        }
        if (board.getTiles().get(this.positionOnBoard).getType() == TileType.LOT) {
            Lot lot = (Lot) board.getTiles().get(this.positionOnBoard);
            Monopoly_final.frame.getjButton2().setEnabled(false);
            Monopoly_final.frame.getjButton3().setEnabled(true);
            Monopoly_final.frame.getjButton5().setEnabled(true);
            if (lot.getOwnedBy() != null && lot.isMortgaged() == false) {
                decreaseMoney(lot.getRent());
                increaseMoneyOfPlayer(lot.getOwnedBy(), lot.getRent());
            }
        }
        if (board.getTiles().get(this.positionOnBoard).getType() == TileType.CHANCE || board.getTiles().get(this.positionOnBoard).getType() == TileType.COMMUNITYCHEST) {
            Monopoly_final.frame.getjButton2().setEnabled(true);
            Monopoly_final.frame.getjButton3().setEnabled(false);
            Monopoly_final.frame.getjButton5().setEnabled(false);
            Monopoly_final.frame.getjButton6().setEnabled(false);
        }

        if (board.getTiles().get(this.positionOnBoard).getType() == TileType.JAIL) {
            System.out.print("JAILED!");
            Monopoly_final.frame.getjButton1().setEnabled(false);
            Monopoly_final.frame.getjButton2().setEnabled(false);
            Monopoly_final.frame.getjButton3().setEnabled(false);
            Monopoly_final.frame.getjButton5().setEnabled(false);
            setInJail(true);
            setTurnsInJail(2);
            if (getOutOfJailCards() > 0) {
                int input = JOptionPane.showConfirmDialog(Monopoly_final.frame, "You have a Get out of Jail for Free card. Do you want to use it?");
                if (input == 0) {
                    setOutOfJailCards(getOutOfJailCards() - 1);
                    setInJail(false);
                    setTurnsInJail(0);
                }
            }
        }
        if (board.getTiles().get(this.positionOnBoard).getType() == TileType.RAILROAD) {
            RailRoad railroad = (RailRoad) board.getTiles().get(this.positionOnBoard);
            Monopoly_final.frame.getjButton3().setEnabled(true);
            //Monopoly_final.frame.getjButton6().setEnabled(false);
            if (railroad.getOwnedBy() != null && railroad.isMortgaged() == false) {
                int counter = 0;
                for (int i = 0; i < railroad.getOwnedBy().ownedTiles.size(); i++) {
                    if (railroad.getOwnedBy().ownedTiles.get(i).getType() == TileType.RAILROAD) {
                        counter++;
                    }
                }
                switch (counter) {
                    case 1:
                        decreaseMoney(25);
                        increaseMoneyOfPlayer(railroad.getOwnedBy(), 25);
                        break;
                    case 2:
                        decreaseMoney(50);
                        increaseMoneyOfPlayer(railroad.getOwnedBy(), 50);
                        break;
                    case 3:
                        decreaseMoney(100);
                        increaseMoneyOfPlayer(railroad.getOwnedBy(), 100);
                        break;
                    case 4:
                        decreaseMoney(200);
                        increaseMoneyOfPlayer(railroad.getOwnedBy(), 200);
                        break;
                }
            }
        }
        if (board.getTiles().get(this.positionOnBoard).getType() == TileType.UTILITIES) {
            Utilities utilities = (Utilities) board.getTiles().get(this.positionOnBoard);
            Monopoly_final.frame.getjButton2().setEnabled(false);
            Monopoly_final.frame.getjButton3().setEnabled(true);
            Monopoly_final.frame.getjButton5().setEnabled(false);
            //Monopoly_final.frame.getjButton6().setEnabled(false);
            if (utilities.getOwnedBy() != null && utilities.isMortgaged() == false) {
                int counter = 0;
                for (int i = 0; i < utilities.getOwnedBy().ownedTiles.size(); i++) {
                    if (utilities.getOwnedBy().ownedTiles.get(i).getType() == TileType.UTILITIES) {
                        counter++;
                    }
                }
                int fee = 0;
                switch (counter) {
                    case 1:
                        fee = throwDiceForUtilities() * 4;
                        break;
                    case 2:
                        fee = throwDiceForUtilities() * 10;
                        break;
                }
                decreaseMoney(fee);
                increaseMoneyOfPlayer(utilities.getOwnedBy(), fee);
            }
        }
        if (board.getTiles().get(this.positionOnBoard).getType() == TileType.GOTOJAIL) {
            System.out.print("JAILED!");
            Monopoly_final.frame.getjButton1().setEnabled(false);
            Monopoly_final.frame.getjButton2().setEnabled(false);
            Monopoly_final.frame.getjButton3().setEnabled(false);
            Monopoly_final.frame.getjButton5().setEnabled(false);
            setInJail(true);
            setTurnsInJail(2);
            Monopoly_final.board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(null);
            setPositionOnBoard(10);
            Monopoly_final.board.getTiles().get(this.positionOnBoard).getPlayerPanel().get(this.playerID).setBackground(this.color);
            if (getOutOfJailCards() > 0) {
                int input = JOptionPane.showConfirmDialog(Monopoly_final.frame, "You have a Get out of Jail for Free card. Do you want to use it?");
                if (input == 0) {
                    setOutOfJailCards(getOutOfJailCards() - 1);
                    setInJail(false);
                    setTurnsInJail(0);
                }
            }
        }
    }

}
