/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Color;
import java.awt.event.*;
import javax.accessibility.AccessibleContext;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import java.util.ArrayList;
import model.Player;
import monopoly_final.Monopoly_final;
import javax.swing.AbstractAction;
import java.awt.Font;
import java.awt.GridBagLayout;
import javax.swing.JOptionPane;
import model.Lot;
import model.RailRoad;
import model.Utilities;

/**
 *<p>MonopolyFrame.java - létrehozza a grafikus felhassználói felületet.</p>
 * 
 * <p>Fő komponensei, alkotóelemei:</p>
 * <ul class = "Main">
 *  <li>JFrame: a program ablaka</li>
 *  <li>JPanel: van egy főpanel, ami maga a tábla, ezenkívül minden mezőnek van egy saját panelje és a panelokon belül egy-egy játékosnak is van egy saját panelje.</li>
 *  <li>JMenuBar, JMenu and JMenuItem: Tartalmaz egy 'File' és egy 'About menüt.&nbsp;
 *      <ul class = "JMenu">
 *          <li>A 'File' nevű menün belül lehetőségünk van: Új játékot kezdeni, VIP menüt előhívni, illetve Kilépni.</li>
 *          <li>Az 'About' nevű menün belül egy 'Info' menüben találhatunk információkat a fejlesztőről.</li>
 *      </ul>
 *  </li>
 *  <li>JButtons: Minden játékos akcióhoz tartozik egy gomb, ami csak akkor aktiválódik, ha a játékos az adott akciót végre is tudja hajtani. </li> 
 * </ul>
 * 
 * @author Osbáth Gergely
 */
public class MonopolyFrame extends javax.swing.JFrame {

    public MonopolyFrame() {
        
        jScrollPane2 = new javax.swing.JScrollPane();
        MainPanel = new javax.swing.JPanel();
        insied_area = new javax.swing.JPanel();
        Tile_00 = new javax.swing.JPanel();
        Player0001 = new javax.swing.JPanel();
        Player0002 = new javax.swing.JPanel();
        Player0003 = new javax.swing.JPanel();
        Player0004 = new javax.swing.JPanel();
        Player0005 = new javax.swing.JPanel();
        Player0006 = new javax.swing.JPanel();
        jLabel25 = new javax.swing.JLabel();
        Tile_01 = new javax.swing.JPanel();
        Player0101 = new javax.swing.JPanel();
        Player0102 = new javax.swing.JPanel();
        Player0103 = new javax.swing.JPanel();
        Player0104 = new javax.swing.JPanel();
        Player0105 = new javax.swing.JPanel();
        Player0106 = new javax.swing.JPanel();
        NamePanel_01 = new javax.swing.JPanel();
        TileName_01 = new javax.swing.JLabel();
        Tile_02 = new javax.swing.JPanel();
        Player0201 = new javax.swing.JPanel();
        Player0202 = new javax.swing.JPanel();
        Player0203 = new javax.swing.JPanel();
        Player0204 = new javax.swing.JPanel();
        Player0205 = new javax.swing.JPanel();
        Player0206 = new javax.swing.JPanel();
        NamePanel_02 = new javax.swing.JPanel();
        TileName_02 = new javax.swing.JLabel();
        Tile_03 = new javax.swing.JPanel();
        Player0301 = new javax.swing.JPanel();
        Player0302 = new javax.swing.JPanel();
        Player0303 = new javax.swing.JPanel();
        Player0304 = new javax.swing.JPanel();
        Player0305 = new javax.swing.JPanel();
        Player0306 = new javax.swing.JPanel();
        NamePanel_03 = new javax.swing.JPanel();
        TileName_03 = new javax.swing.JLabel();
        Tile_04 = new javax.swing.JPanel();
        Player0401 = new javax.swing.JPanel();
        Player0402 = new javax.swing.JPanel();
        Player0403 = new javax.swing.JPanel();
        Player0404 = new javax.swing.JPanel();
        Player0405 = new javax.swing.JPanel();
        Player0406 = new javax.swing.JPanel();
        NamePanel_04 = new javax.swing.JPanel();
        TileName_04 = new javax.swing.JLabel();
        Tile_05 = new javax.swing.JPanel();
        Player0501 = new javax.swing.JPanel();
        Player0502 = new javax.swing.JPanel();
        Player0503 = new javax.swing.JPanel();
        Player0504 = new javax.swing.JPanel();
        Player0505 = new javax.swing.JPanel();
        Player0506 = new javax.swing.JPanel();
        NamePanel_05 = new javax.swing.JPanel();
        TileName_05 = new javax.swing.JLabel();
        Tile_06 = new javax.swing.JPanel();
        Player0601 = new javax.swing.JPanel();
        Player0602 = new javax.swing.JPanel();
        Player0603 = new javax.swing.JPanel();
        Player0604 = new javax.swing.JPanel();
        Player0605 = new javax.swing.JPanel();
        Player0606 = new javax.swing.JPanel();
        NamePanel_06 = new javax.swing.JPanel();
        TileName_06 = new javax.swing.JLabel();
        Tile_07 = new javax.swing.JPanel();
        Player0701 = new javax.swing.JPanel();
        Player0702 = new javax.swing.JPanel();
        Player0703 = new javax.swing.JPanel();
        Player0704 = new javax.swing.JPanel();
        Player0705 = new javax.swing.JPanel();
        Player0706 = new javax.swing.JPanel();
        NamePanel_07 = new javax.swing.JPanel();
        TileName_07 = new javax.swing.JLabel();
        Tile_08 = new javax.swing.JPanel();
        Player0801 = new javax.swing.JPanel();
        Player0802 = new javax.swing.JPanel();
        Player0803 = new javax.swing.JPanel();
        Player0804 = new javax.swing.JPanel();
        Player0805 = new javax.swing.JPanel();
        Player0806 = new javax.swing.JPanel();
        NamePanel_08 = new javax.swing.JPanel();
        TileName_08 = new javax.swing.JLabel();
        Tile_09 = new javax.swing.JPanel();
        Player0901 = new javax.swing.JPanel();
        Player0902 = new javax.swing.JPanel();
        Player0903 = new javax.swing.JPanel();
        Player0904 = new javax.swing.JPanel();
        Player0905 = new javax.swing.JPanel();
        Player0906 = new javax.swing.JPanel();
        NamePanel_09 = new javax.swing.JPanel();
        TileName_09 = new javax.swing.JLabel();
        Tile_10 = new javax.swing.JPanel();
        Player1001 = new javax.swing.JPanel();
        Player1002 = new javax.swing.JPanel();
        Player1003 = new javax.swing.JPanel();
        Player1004 = new javax.swing.JPanel();
        Player1005 = new javax.swing.JPanel();
        Player1006 = new javax.swing.JPanel();
        Tile_11 = new javax.swing.JPanel();
        Player1101 = new javax.swing.JPanel();
        Player1102 = new javax.swing.JPanel();
        Player1103 = new javax.swing.JPanel();
        Player1104 = new javax.swing.JPanel();
        Player1105 = new javax.swing.JPanel();
        Player1106 = new javax.swing.JPanel();
        NamePanel_11 = new javax.swing.JPanel();
        TileName_11 = new javax.swing.JLabel();
        Tile_12 = new javax.swing.JPanel();
        Player1201 = new javax.swing.JPanel();
        Player1202 = new javax.swing.JPanel();
        Player1203 = new javax.swing.JPanel();
        Player1204 = new javax.swing.JPanel();
        Player1205 = new javax.swing.JPanel();
        Player1206 = new javax.swing.JPanel();
        NamePanel_12 = new javax.swing.JPanel();
        TileName_12 = new javax.swing.JLabel();
        Tile_13 = new javax.swing.JPanel();
        Player1301 = new javax.swing.JPanel();
        Player1302 = new javax.swing.JPanel();
        Player1303 = new javax.swing.JPanel();
        Player1304 = new javax.swing.JPanel();
        Player1305 = new javax.swing.JPanel();
        Player1306 = new javax.swing.JPanel();
        NamePanel_13 = new javax.swing.JPanel();
        TileName_13 = new javax.swing.JLabel();
        Tile_14 = new javax.swing.JPanel();
        Player1401 = new javax.swing.JPanel();
        Player1402 = new javax.swing.JPanel();
        Player1404 = new javax.swing.JPanel();
        Player1403 = new javax.swing.JPanel();
        Player1405 = new javax.swing.JPanel();
        Player1406 = new javax.swing.JPanel();
        NamePanel_14 = new javax.swing.JPanel();
        TileName_14 = new javax.swing.JLabel();
        Tile_15 = new javax.swing.JPanel();
        Player1501 = new javax.swing.JPanel();
        Player1502 = new javax.swing.JPanel();
        Player1504 = new javax.swing.JPanel();
        Player1503 = new javax.swing.JPanel();
        Player1505 = new javax.swing.JPanel();
        Player1506 = new javax.swing.JPanel();
        NamePanel_15 = new javax.swing.JPanel();
        TileName_15 = new javax.swing.JLabel();
        Tile_16 = new javax.swing.JPanel();
        Player1601 = new javax.swing.JPanel();
        Player1602 = new javax.swing.JPanel();
        Player1603 = new javax.swing.JPanel();
        Player1604 = new javax.swing.JPanel();
        Player1605 = new javax.swing.JPanel();
        Player1606 = new javax.swing.JPanel();
        NamePanel_16 = new javax.swing.JPanel();
        TileName_16 = new javax.swing.JLabel();
        Tile_17 = new javax.swing.JPanel();
        Player1701 = new javax.swing.JPanel();
        Player1702 = new javax.swing.JPanel();
        Player1703 = new javax.swing.JPanel();
        Player1704 = new javax.swing.JPanel();
        Player1705 = new javax.swing.JPanel();
        Player1706 = new javax.swing.JPanel();
        NamePanel_17 = new javax.swing.JPanel();
        TileName_17 = new javax.swing.JLabel();
        Tile_18 = new javax.swing.JPanel();
        Player1801 = new javax.swing.JPanel();
        Player1802 = new javax.swing.JPanel();
        Player1803 = new javax.swing.JPanel();
        Player1804 = new javax.swing.JPanel();
        Player1805 = new javax.swing.JPanel();
        Player1806 = new javax.swing.JPanel();
        NamePanel_18 = new javax.swing.JPanel();
        TileName_18 = new javax.swing.JLabel();
        Tile_19 = new javax.swing.JPanel();
        Player1901 = new javax.swing.JPanel();
        Player1902 = new javax.swing.JPanel();
        Player1903 = new javax.swing.JPanel();
        Player1904 = new javax.swing.JPanel();
        Player1905 = new javax.swing.JPanel();
        Player1906 = new javax.swing.JPanel();
        NamePanel_19 = new javax.swing.JPanel();
        TileName_19 = new javax.swing.JLabel();
        Tile_20 = new javax.swing.JPanel();
        Player2001 = new javax.swing.JPanel();
        Player2002 = new javax.swing.JPanel();
        Player2003 = new javax.swing.JPanel();
        Player2004 = new javax.swing.JPanel();
        Player2005 = new javax.swing.JPanel();
        Player2006 = new javax.swing.JPanel();
        Tile_21 = new javax.swing.JPanel();
        Player2101 = new javax.swing.JPanel();
        Player2102 = new javax.swing.JPanel();
        Player2103 = new javax.swing.JPanel();
        Player2104 = new javax.swing.JPanel();
        Player2105 = new javax.swing.JPanel();
        Player2106 = new javax.swing.JPanel();
        NamePanel_21 = new javax.swing.JPanel();
        TileName_21 = new javax.swing.JLabel();
        Tile_22 = new javax.swing.JPanel();
        Player2201 = new javax.swing.JPanel();
        Player2202 = new javax.swing.JPanel();
        Player2203 = new javax.swing.JPanel();
        Player2204 = new javax.swing.JPanel();
        Player2205 = new javax.swing.JPanel();
        Player2206 = new javax.swing.JPanel();
        NamePanel_22 = new javax.swing.JPanel();
        TileName_22 = new javax.swing.JLabel();
        Tile_23 = new javax.swing.JPanel();
        Player2301 = new javax.swing.JPanel();
        Player2302 = new javax.swing.JPanel();
        Player2303 = new javax.swing.JPanel();
        Player2304 = new javax.swing.JPanel();
        Player2305 = new javax.swing.JPanel();
        Player2306 = new javax.swing.JPanel();
        NamePanel_23 = new javax.swing.JPanel();
        TileName_23 = new javax.swing.JLabel();
        Tile_24 = new javax.swing.JPanel();
        Player2401 = new javax.swing.JPanel();
        Player2402 = new javax.swing.JPanel();
        Player2403 = new javax.swing.JPanel();
        Player2404 = new javax.swing.JPanel();
        Player2405 = new javax.swing.JPanel();
        Player2406 = new javax.swing.JPanel();
        NamePanel_24 = new javax.swing.JPanel();
        TileName_24 = new javax.swing.JLabel();
        Tile_25 = new javax.swing.JPanel();
        Player2501 = new javax.swing.JPanel();
        Player2502 = new javax.swing.JPanel();
        Player2503 = new javax.swing.JPanel();
        Player2504 = new javax.swing.JPanel();
        Player2505 = new javax.swing.JPanel();
        Player2506 = new javax.swing.JPanel();
        NamePanel_25 = new javax.swing.JPanel();
        TileName_25 = new javax.swing.JLabel();
        Tile_26 = new javax.swing.JPanel();
        Player2601 = new javax.swing.JPanel();
        Player2602 = new javax.swing.JPanel();
        Player2603 = new javax.swing.JPanel();
        Player2604 = new javax.swing.JPanel();
        Player2605 = new javax.swing.JPanel();
        Player2606 = new javax.swing.JPanel();
        NamePanel_26 = new javax.swing.JPanel();
        TileName_26 = new javax.swing.JLabel();
        Tile_27 = new javax.swing.JPanel();
        Player2701 = new javax.swing.JPanel();
        Player2702 = new javax.swing.JPanel();
        Player2703 = new javax.swing.JPanel();
        Player2704 = new javax.swing.JPanel();
        Player2705 = new javax.swing.JPanel();
        Player2706 = new javax.swing.JPanel();
        NamePanel_27 = new javax.swing.JPanel();
        TileName_27 = new javax.swing.JLabel();
        Tile_28 = new javax.swing.JPanel();
        Player2801 = new javax.swing.JPanel();
        Player2802 = new javax.swing.JPanel();
        Player2803 = new javax.swing.JPanel();
        Player2804 = new javax.swing.JPanel();
        Player2805 = new javax.swing.JPanel();
        Player2806 = new javax.swing.JPanel();
        NamePanel_28 = new javax.swing.JPanel();
        TileName_28 = new javax.swing.JLabel();
        Tile_29 = new javax.swing.JPanel();
        Player2901 = new javax.swing.JPanel();
        Player2902 = new javax.swing.JPanel();
        Player2903 = new javax.swing.JPanel();
        Player2904 = new javax.swing.JPanel();
        Player2905 = new javax.swing.JPanel();
        Player2906 = new javax.swing.JPanel();
        NamePanel_29 = new javax.swing.JPanel();
        TileName_29 = new javax.swing.JLabel();
        Tile_30 = new javax.swing.JPanel();
        Player3001 = new javax.swing.JPanel();
        Player3002 = new javax.swing.JPanel();
        Player3003 = new javax.swing.JPanel();
        Player3004 = new javax.swing.JPanel();
        Player3005 = new javax.swing.JPanel();
        Player3006 = new javax.swing.JPanel();
        Tile_31 = new javax.swing.JPanel();
        Player3101 = new javax.swing.JPanel();
        Player3102 = new javax.swing.JPanel();
        Player3103 = new javax.swing.JPanel();
        Player3104 = new javax.swing.JPanel();
        Player3105 = new javax.swing.JPanel();
        Player3106 = new javax.swing.JPanel();
        NamePanel_31 = new javax.swing.JPanel();
        TileName_31 = new javax.swing.JLabel();
        Tile_32 = new javax.swing.JPanel();
        Player3201 = new javax.swing.JPanel();
        Player3202 = new javax.swing.JPanel();
        Player3203 = new javax.swing.JPanel();
        Player3204 = new javax.swing.JPanel();
        Player3205 = new javax.swing.JPanel();
        Player3206 = new javax.swing.JPanel();
        NamePanel_32 = new javax.swing.JPanel();
        TileName_32 = new javax.swing.JLabel();
        Tile_33 = new javax.swing.JPanel();
        Player3301 = new javax.swing.JPanel();
        Player3302 = new javax.swing.JPanel();
        Player3303 = new javax.swing.JPanel();
        Player3304 = new javax.swing.JPanel();
        Player3305 = new javax.swing.JPanel();
        Player3306 = new javax.swing.JPanel();
        NamePanel_33 = new javax.swing.JPanel();
        TileName_33 = new javax.swing.JLabel();
        Tile_34 = new javax.swing.JPanel();
        Player3401 = new javax.swing.JPanel();
        Player3402 = new javax.swing.JPanel();
        Player3403 = new javax.swing.JPanel();
        Player3404 = new javax.swing.JPanel();
        Player3405 = new javax.swing.JPanel();
        Player3406 = new javax.swing.JPanel();
        NamePanel_34 = new javax.swing.JPanel();
        TileName_34 = new javax.swing.JLabel();
        Tile_35 = new javax.swing.JPanel();
        Player3501 = new javax.swing.JPanel();
        Player3502 = new javax.swing.JPanel();
        Player3503 = new javax.swing.JPanel();
        Player3504 = new javax.swing.JPanel();
        Player3505 = new javax.swing.JPanel();
        Player3506 = new javax.swing.JPanel();
        NamePanel_35 = new javax.swing.JPanel();
        TileName_35 = new javax.swing.JLabel();
        Tile_36 = new javax.swing.JPanel();
        Player3601 = new javax.swing.JPanel();
        Player3602 = new javax.swing.JPanel();
        Player3603 = new javax.swing.JPanel();
        Player3604 = new javax.swing.JPanel();
        Player3605 = new javax.swing.JPanel();
        Player3606 = new javax.swing.JPanel();
        NamePanel_36 = new javax.swing.JPanel();
        TileName_36 = new javax.swing.JLabel();
        Tile_37 = new javax.swing.JPanel();
        Player3701 = new javax.swing.JPanel();
        Player3702 = new javax.swing.JPanel();
        Player3703 = new javax.swing.JPanel();
        Player3704 = new javax.swing.JPanel();
        Player3705 = new javax.swing.JPanel();
        Player3706 = new javax.swing.JPanel();
        NamePanel_37 = new javax.swing.JPanel();
        TileName_37 = new javax.swing.JLabel();
        Tile_38 = new javax.swing.JPanel();
        Player3801 = new javax.swing.JPanel();
        Player3802 = new javax.swing.JPanel();
        Player3803 = new javax.swing.JPanel();
        Player3804 = new javax.swing.JPanel();
        Player3805 = new javax.swing.JPanel();
        Player3806 = new javax.swing.JPanel();
        NamePanel_38 = new javax.swing.JPanel();
        TileName_38 = new javax.swing.JLabel();
        Tile_39 = new javax.swing.JPanel();
        Player3901 = new javax.swing.JPanel();
        Player3902 = new javax.swing.JPanel();
        Player3903 = new javax.swing.JPanel();
        Player3904 = new javax.swing.JPanel();
        Player3905 = new javax.swing.JPanel();
        Player3906 = new javax.swing.JPanel();
        NamePanel_39 = new javax.swing.JPanel();
        TileName_39 = new javax.swing.JLabel();
        SidePanel = new javax.swing.JPanel();
        Player1 = new javax.swing.JPanel();
        Player1_Color = new javax.swing.JPanel();
        jComboBox1 = new javax.swing.JComboBox<>();
        Player1_NameD = new javax.swing.JLabel();
        Player1_Name = new javax.swing.JLabel();
        Player1_Balance = new javax.swing.JLabel();
        Player1_InJailD = new javax.swing.JLabel();
        Player1_InJail = new javax.swing.JLabel();
        Player1_BalanceD = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        DownPanel = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jPanel9_ok = new javax.swing.JPanel();
        jLabel82 = new javax.swing.JLabel();
        jLabel83 = new javax.swing.JLabel();
        jLabel84 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jLabel37 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        jLabel40 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        jPanel16 = new javax.swing.JPanel();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jPanel17 = new javax.swing.JPanel();
        jLabel49 = new javax.swing.JLabel();
        jLabel50 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        jPanel18 = new javax.swing.JPanel();
        jLabel52 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        jLabel54 = new javax.swing.JLabel();
        jPanel19 = new javax.swing.JPanel();
        jLabel55 = new javax.swing.JLabel();
        jLabel56 = new javax.swing.JLabel();
        jLabel57 = new javax.swing.JLabel();
        jPanel20 = new javax.swing.JPanel();
        jLabel58 = new javax.swing.JLabel();
        jLabel59 = new javax.swing.JLabel();
        jLabel60 = new javax.swing.JLabel();
        jPanel21 = new javax.swing.JPanel();
        jLabel61 = new javax.swing.JLabel();
        jLabel62 = new javax.swing.JLabel();
        jLabel63 = new javax.swing.JLabel();
        jPanel22 = new javax.swing.JPanel();
        jLabel64 = new javax.swing.JLabel();
        jLabel65 = new javax.swing.JLabel();
        jLabel66 = new javax.swing.JLabel();
        jPanel23 = new javax.swing.JPanel();
        jLabel85 = new javax.swing.JLabel();
        jLabel86 = new javax.swing.JLabel();
        jLabel87 = new javax.swing.JLabel();
        jPanel24 = new javax.swing.JPanel();
        jLabel67 = new javax.swing.JLabel();
        jLabel68 = new javax.swing.JLabel();
        jLabel69 = new javax.swing.JLabel();
        jPanel25 = new javax.swing.JPanel();
        jLabel70 = new javax.swing.JLabel();
        jLabel71 = new javax.swing.JLabel();
        jLabel72 = new javax.swing.JLabel();
        jPanel26 = new javax.swing.JPanel();
        jLabel73 = new javax.swing.JLabel();
        jLabel74 = new javax.swing.JLabel();
        jLabel75 = new javax.swing.JLabel();
        jPanel27 = new javax.swing.JPanel();
        jLabel76 = new javax.swing.JLabel();
        jLabel77 = new javax.swing.JLabel();
        jLabel78 = new javax.swing.JLabel();
        jPanel28 = new javax.swing.JPanel();
        jLabel79 = new javax.swing.JLabel();
        jLabel80 = new javax.swing.JLabel();
        jLabel81 = new javax.swing.JLabel();
        throwDiceLabel = new javax.swing.JLabel();
        jMenuBar = new javax.swing.JMenuBar();
        file = new javax.swing.JMenu("File");
        infoBar = new javax.swing.JMenu("About");
        info = new javax.swing.JMenuItem("Info");
        new_game = new javax.swing.JMenuItem("New Game");
        
        new_game = new javax.swing.JMenuItem(new AbstractAction("New Game"){
            @Override
            public void actionPerformed(ActionEvent e) {
                Monopoly_final.newGame();
            }
        });
        
        exit = new javax.swing.JMenuItem(new AbstractAction("Exit"){
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        vip = new javax.swing.JMenuItem(new AbstractAction("VIP"){
            @Override
            public void actionPerformed(ActionEvent e) {
                CheatFrame cf = new CheatFrame();
            }
        });
        
        info = new javax.swing.JMenuItem(new AbstractAction("Info"){
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(Monopoly_final.frame,"<html>2019 - Prog. Tech II Monopoly beadandó<br>Készítette: Osbáth Gergely</html>"); 
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        
        file.add(new_game);
        file.add(vip);
        file.addSeparator();
        file.add(exit);
        jMenuBar.add(file);
        
        infoBar.add(info);
        jMenuBar.add(infoBar);
        this.setJMenuBar(jMenuBar);

        jScrollPane2.setToolTipText("");
        jScrollPane2.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        jScrollPane2.setPreferredSize(new java.awt.Dimension(1080, 720));

        MainPanel.setMaximumSize(new java.awt.Dimension(4056, 2560));

        insied_area.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        insied_area.setPreferredSize(new java.awt.Dimension(810, 810));

        javax.swing.GroupLayout insied_areaLayout = new javax.swing.GroupLayout(insied_area);
        insied_area.setLayout(insied_areaLayout);
        insied_areaLayout.setHorizontalGroup(
                insied_areaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 808, Short.MAX_VALUE)
        );
        insied_areaLayout.setVerticalGroup(
                insied_areaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 808, Short.MAX_VALUE)
        );
        
        throwDiceLabel.setFont(new Font("Verdana",1,30));        
        throwDiceLabel.setSize(250, 100);
        insied_area.setLayout(new GridBagLayout());
        insied_area.add(throwDiceLabel);
        
        Tile_00.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_00.setPreferredSize(new java.awt.Dimension(125, 125));

        Player0001.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player0001Layout = new javax.swing.GroupLayout(Player0001);
        Player0001.setLayout(Player0001Layout);
        Player0001Layout.setHorizontalGroup(
                Player0001Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player0001Layout.setVerticalGroup(
                Player0001Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player0002.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player0002Layout = new javax.swing.GroupLayout(Player0002);
        Player0002.setLayout(Player0002Layout);
        Player0002Layout.setHorizontalGroup(
                Player0002Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player0002Layout.setVerticalGroup(
                Player0002Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player0003.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player0003Layout = new javax.swing.GroupLayout(Player0003);
        Player0003.setLayout(Player0003Layout);
        Player0003Layout.setHorizontalGroup(
                Player0003Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player0003Layout.setVerticalGroup(
                Player0003Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player0004.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0004Layout = new javax.swing.GroupLayout(Player0004);
        Player0004.setLayout(Player0004Layout);
        Player0004Layout.setHorizontalGroup(
                Player0004Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0004Layout.setVerticalGroup(
                Player0004Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0005.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0005Layout = new javax.swing.GroupLayout(Player0005);
        Player0005.setLayout(Player0005Layout);
        Player0005Layout.setHorizontalGroup(
                Player0005Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0005Layout.setVerticalGroup(
                Player0005Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0006.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0006Layout = new javax.swing.GroupLayout(Player0006);
        Player0006.setLayout(Player0006Layout);
        Player0006Layout.setHorizontalGroup(
                Player0006Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0006Layout.setVerticalGroup(
                Player0006Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        jLabel25.setText("START");

        javax.swing.GroupLayout Tile_00Layout = new javax.swing.GroupLayout(Tile_00);
        Tile_00.setLayout(Tile_00Layout);
        Tile_00Layout.setHorizontalGroup(
                Tile_00Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_00Layout.createSequentialGroup()
                                .addGroup(Tile_00Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, Tile_00Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(Player0005, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(Player0006, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(Player0004, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                                .addComponent(jLabel25)
                                .addContainerGap())
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Tile_00Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(Player0003, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(Player0002, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(Player0001, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        Tile_00Layout.setVerticalGroup(
                Tile_00Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_00Layout.createSequentialGroup()
                                .addGroup(Tile_00Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(Tile_00Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(Player0002, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(Player0001, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(Player0003, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addComponent(Player0004, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(Tile_00Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(Player0005, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel25))
                                .addGap(0, 0, 0)
                                .addComponent(Player0006, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        Tile_01.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_01.setPreferredSize(new java.awt.Dimension(90, 125));

        Player0101.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0101Layout = new javax.swing.GroupLayout(Player0101);
        Player0101.setLayout(Player0101Layout);
        Player0101Layout.setHorizontalGroup(
                Player0101Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0101Layout.setVerticalGroup(
                Player0101Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0102.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0102Layout = new javax.swing.GroupLayout(Player0102);
        Player0102.setLayout(Player0102Layout);
        Player0102Layout.setHorizontalGroup(
                Player0102Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0102Layout.setVerticalGroup(
                Player0102Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0103.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0103Layout = new javax.swing.GroupLayout(Player0103);
        Player0103.setLayout(Player0103Layout);
        Player0103Layout.setHorizontalGroup(
                Player0103Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0103Layout.setVerticalGroup(
                Player0103Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0104.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0104Layout = new javax.swing.GroupLayout(Player0104);
        Player0104.setLayout(Player0104Layout);
        Player0104Layout.setHorizontalGroup(
                Player0104Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0104Layout.setVerticalGroup(
                Player0104Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0105.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0105Layout = new javax.swing.GroupLayout(Player0105);
        Player0105.setLayout(Player0105Layout);
        Player0105Layout.setHorizontalGroup(
                Player0105Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0105Layout.setVerticalGroup(
                Player0105Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0106.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0106Layout = new javax.swing.GroupLayout(Player0106);
        Player0106.setLayout(Player0106Layout);
        Player0106Layout.setHorizontalGroup(
                Player0106Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0106Layout.setVerticalGroup(
                Player0106Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        NamePanel_01.setBackground(new java.awt.Color(102, 51, 0));
        NamePanel_01.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_01.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        NamePanel_01.setPreferredSize(new java.awt.Dimension(90, 40));
        NamePanel_01.setRequestFocusEnabled(false);

        TileName_01.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_01.setText("<HTML>MEDITERANEIAN<BR>AVENUE</HTML>");
        TileName_01.setToolTipText("");
        TileName_01.setPreferredSize(new java.awt.Dimension(90, 40));

        javax.swing.GroupLayout NamePanel_01Layout = new javax.swing.GroupLayout(NamePanel_01);
        NamePanel_01.setLayout(NamePanel_01Layout);
        NamePanel_01Layout.setHorizontalGroup(
                NamePanel_01Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_01Layout.createSequentialGroup()
                                .addComponent(TileName_01, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
        );
        NamePanel_01Layout.setVerticalGroup(
                NamePanel_01Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_01Layout.createSequentialGroup()
                                .addComponent(TileName_01, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout Tile_01Layout = new javax.swing.GroupLayout(Tile_01);
        Tile_01.setLayout(Tile_01Layout);
        Tile_01Layout.setHorizontalGroup(
                Tile_01Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_01Layout.createSequentialGroup()
                                .addGroup(Tile_01Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player0104, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0106, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0105, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(Tile_01Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player0103, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0101, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0102, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(NamePanel_01, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        Tile_01Layout.setVerticalGroup(
                Tile_01Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_01Layout.createSequentialGroup()
                                .addComponent(NamePanel_01, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(Tile_01Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(Tile_01Layout.createSequentialGroup()
                                                .addComponent(Player0101, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGroup(Tile_01Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(Tile_01Layout.createSequentialGroup()
                                                                .addComponent(Player0102, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, 0)
                                                                .addComponent(Player0103, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(Tile_01Layout.createSequentialGroup()
                                                                .addComponent(Player0105, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, 0)
                                                                .addComponent(Player0106, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                        .addComponent(Player0104, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Tile_02.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_02.setPreferredSize(new java.awt.Dimension(90, 125));

        Player0201.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0201Layout = new javax.swing.GroupLayout(Player0201);
        Player0201.setLayout(Player0201Layout);
        Player0201Layout.setHorizontalGroup(
                Player0201Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0201Layout.setVerticalGroup(
                Player0201Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0202.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0202Layout = new javax.swing.GroupLayout(Player0202);
        Player0202.setLayout(Player0202Layout);
        Player0202Layout.setHorizontalGroup(
                Player0202Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0202Layout.setVerticalGroup(
                Player0202Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0203.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0203Layout = new javax.swing.GroupLayout(Player0203);
        Player0203.setLayout(Player0203Layout);
        Player0203Layout.setHorizontalGroup(
                Player0203Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0203Layout.setVerticalGroup(
                Player0203Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0204.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0204Layout = new javax.swing.GroupLayout(Player0204);
        Player0204.setLayout(Player0204Layout);
        Player0204Layout.setHorizontalGroup(
                Player0204Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0204Layout.setVerticalGroup(
                Player0204Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0205.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0205Layout = new javax.swing.GroupLayout(Player0205);
        Player0205.setLayout(Player0205Layout);
        Player0205Layout.setHorizontalGroup(
                Player0205Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0205Layout.setVerticalGroup(
                Player0205Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0206.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0206Layout = new javax.swing.GroupLayout(Player0206);
        Player0206.setLayout(Player0206Layout);
        Player0206Layout.setHorizontalGroup(
                Player0206Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0206Layout.setVerticalGroup(
                Player0206Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        NamePanel_02.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_02.setPreferredSize(new java.awt.Dimension(90, 40));
        NamePanel_02.setRequestFocusEnabled(false);

        TileName_02.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_02.setText("<HTML>COMMUNITY<BR>CHEST</HTML>");
        TileName_02.setPreferredSize(new java.awt.Dimension(90, 40));

        javax.swing.GroupLayout NamePanel_02Layout = new javax.swing.GroupLayout(NamePanel_02);
        NamePanel_02.setLayout(NamePanel_02Layout);
        NamePanel_02Layout.setHorizontalGroup(
                NamePanel_02Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_02Layout.createSequentialGroup()
                                .addComponent(TileName_02, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        NamePanel_02Layout.setVerticalGroup(
                NamePanel_02Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_02Layout.createSequentialGroup()
                                .addComponent(TileName_02, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout Tile_02Layout = new javax.swing.GroupLayout(Tile_02);
        Tile_02.setLayout(Tile_02Layout);
        Tile_02Layout.setHorizontalGroup(
                Tile_02Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_02Layout.createSequentialGroup()
                                .addGroup(Tile_02Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player0204, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0206, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0205, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(Tile_02Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player0203, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0201, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0202, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(NamePanel_02, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        Tile_02Layout.setVerticalGroup(
                Tile_02Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_02Layout.createSequentialGroup()
                                .addComponent(NamePanel_02, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(Tile_02Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(Tile_02Layout.createSequentialGroup()
                                                .addComponent(Player0201, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGroup(Tile_02Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(Tile_02Layout.createSequentialGroup()
                                                                .addComponent(Player0202, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, 0)
                                                                .addComponent(Player0203, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(Tile_02Layout.createSequentialGroup()
                                                                .addComponent(Player0205, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, 0)
                                                                .addComponent(Player0206, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                        .addComponent(Player0204, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Tile_03.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_03.setPreferredSize(new java.awt.Dimension(90, 125));

        Player0301.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0301Layout = new javax.swing.GroupLayout(Player0301);
        Player0301.setLayout(Player0301Layout);
        Player0301Layout.setHorizontalGroup(
                Player0301Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0301Layout.setVerticalGroup(
                Player0301Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0302.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0302Layout = new javax.swing.GroupLayout(Player0302);
        Player0302.setLayout(Player0302Layout);
        Player0302Layout.setHorizontalGroup(
                Player0302Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0302Layout.setVerticalGroup(
                Player0302Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0303.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0303Layout = new javax.swing.GroupLayout(Player0303);
        Player0303.setLayout(Player0303Layout);
        Player0303Layout.setHorizontalGroup(
                Player0303Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0303Layout.setVerticalGroup(
                Player0303Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0304.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0304Layout = new javax.swing.GroupLayout(Player0304);
        Player0304.setLayout(Player0304Layout);
        Player0304Layout.setHorizontalGroup(
                Player0304Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0304Layout.setVerticalGroup(
                Player0304Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0305.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0305Layout = new javax.swing.GroupLayout(Player0305);
        Player0305.setLayout(Player0305Layout);
        Player0305Layout.setHorizontalGroup(
                Player0305Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0305Layout.setVerticalGroup(
                Player0305Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0306.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0306Layout = new javax.swing.GroupLayout(Player0306);
        Player0306.setLayout(Player0306Layout);
        Player0306Layout.setHorizontalGroup(
                Player0306Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0306Layout.setVerticalGroup(
                Player0306Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        NamePanel_03.setBackground(new java.awt.Color(102, 51, 0));
        NamePanel_03.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_03.setPreferredSize(new java.awt.Dimension(90, 40));
        NamePanel_03.setRequestFocusEnabled(false);

        TileName_03.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_03.setText("BALTIC AVENUE");
        TileName_03.setPreferredSize(new java.awt.Dimension(90, 40));

        javax.swing.GroupLayout NamePanel_03Layout = new javax.swing.GroupLayout(NamePanel_03);
        NamePanel_03.setLayout(NamePanel_03Layout);
        NamePanel_03Layout.setHorizontalGroup(
                NamePanel_03Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_03Layout.createSequentialGroup()
                                .addComponent(TileName_03, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        NamePanel_03Layout.setVerticalGroup(
                NamePanel_03Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_03Layout.createSequentialGroup()
                                .addComponent(TileName_03, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout Tile_03Layout = new javax.swing.GroupLayout(Tile_03);
        Tile_03.setLayout(Tile_03Layout);
        Tile_03Layout.setHorizontalGroup(
                Tile_03Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_03Layout.createSequentialGroup()
                                .addGroup(Tile_03Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player0304, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0306, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0305, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(Tile_03Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player0303, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0301, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0302, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(NamePanel_03, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        Tile_03Layout.setVerticalGroup(
                Tile_03Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_03Layout.createSequentialGroup()
                                .addComponent(NamePanel_03, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(Tile_03Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(Tile_03Layout.createSequentialGroup()
                                                .addComponent(Player0301, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGroup(Tile_03Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(Tile_03Layout.createSequentialGroup()
                                                                .addComponent(Player0302, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, 0)
                                                                .addComponent(Player0303, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(Tile_03Layout.createSequentialGroup()
                                                                .addComponent(Player0305, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, 0)
                                                                .addComponent(Player0306, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                        .addComponent(Player0304, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Tile_04.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_04.setPreferredSize(new java.awt.Dimension(90, 125));

        Player0401.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0401Layout = new javax.swing.GroupLayout(Player0401);
        Player0401.setLayout(Player0401Layout);
        Player0401Layout.setHorizontalGroup(
                Player0401Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0401Layout.setVerticalGroup(
                Player0401Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0402.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0402Layout = new javax.swing.GroupLayout(Player0402);
        Player0402.setLayout(Player0402Layout);
        Player0402Layout.setHorizontalGroup(
                Player0402Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0402Layout.setVerticalGroup(
                Player0402Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0403.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0403Layout = new javax.swing.GroupLayout(Player0403);
        Player0403.setLayout(Player0403Layout);
        Player0403Layout.setHorizontalGroup(
                Player0403Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0403Layout.setVerticalGroup(
                Player0403Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0404.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0404Layout = new javax.swing.GroupLayout(Player0404);
        Player0404.setLayout(Player0404Layout);
        Player0404Layout.setHorizontalGroup(
                Player0404Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0404Layout.setVerticalGroup(
                Player0404Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0405.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0405Layout = new javax.swing.GroupLayout(Player0405);
        Player0405.setLayout(Player0405Layout);
        Player0405Layout.setHorizontalGroup(
                Player0405Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0405Layout.setVerticalGroup(
                Player0405Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0406.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0406Layout = new javax.swing.GroupLayout(Player0406);
        Player0406.setLayout(Player0406Layout);
        Player0406Layout.setHorizontalGroup(
                Player0406Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0406Layout.setVerticalGroup(
                Player0406Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        NamePanel_04.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_04.setPreferredSize(new java.awt.Dimension(90, 40));
        NamePanel_04.setRequestFocusEnabled(false);

        TileName_04.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_04.setText("INCOME TAX");
        TileName_04.setPreferredSize(new java.awt.Dimension(90, 40));

        javax.swing.GroupLayout NamePanel_04Layout = new javax.swing.GroupLayout(NamePanel_04);
        NamePanel_04.setLayout(NamePanel_04Layout);
        NamePanel_04Layout.setHorizontalGroup(
                NamePanel_04Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_04Layout.createSequentialGroup()
                                .addComponent(TileName_04, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        NamePanel_04Layout.setVerticalGroup(
                NamePanel_04Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_04Layout.createSequentialGroup()
                                .addComponent(TileName_04, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout Tile_04Layout = new javax.swing.GroupLayout(Tile_04);
        Tile_04.setLayout(Tile_04Layout);
        Tile_04Layout.setHorizontalGroup(
                Tile_04Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_04Layout.createSequentialGroup()
                                .addGroup(Tile_04Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player0404, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0406, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0405, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(Tile_04Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player0403, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0401, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0402, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(NamePanel_04, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        Tile_04Layout.setVerticalGroup(
                Tile_04Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_04Layout.createSequentialGroup()
                                .addComponent(NamePanel_04, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(Tile_04Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(Tile_04Layout.createSequentialGroup()
                                                .addComponent(Player0401, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGroup(Tile_04Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(Tile_04Layout.createSequentialGroup()
                                                                .addComponent(Player0402, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, 0)
                                                                .addComponent(Player0403, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(Tile_04Layout.createSequentialGroup()
                                                                .addComponent(Player0405, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, 0)
                                                                .addComponent(Player0406, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                        .addComponent(Player0404, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Tile_05.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_05.setPreferredSize(new java.awt.Dimension(90, 125));

        Player0501.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0501Layout = new javax.swing.GroupLayout(Player0501);
        Player0501.setLayout(Player0501Layout);
        Player0501Layout.setHorizontalGroup(
                Player0501Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0501Layout.setVerticalGroup(
                Player0501Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0502.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0502Layout = new javax.swing.GroupLayout(Player0502);
        Player0502.setLayout(Player0502Layout);
        Player0502Layout.setHorizontalGroup(
                Player0502Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0502Layout.setVerticalGroup(
                Player0502Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0503.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0503Layout = new javax.swing.GroupLayout(Player0503);
        Player0503.setLayout(Player0503Layout);
        Player0503Layout.setHorizontalGroup(
                Player0503Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0503Layout.setVerticalGroup(
                Player0503Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0504.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0504Layout = new javax.swing.GroupLayout(Player0504);
        Player0504.setLayout(Player0504Layout);
        Player0504Layout.setHorizontalGroup(
                Player0504Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0504Layout.setVerticalGroup(
                Player0504Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0505.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0505Layout = new javax.swing.GroupLayout(Player0505);
        Player0505.setLayout(Player0505Layout);
        Player0505Layout.setHorizontalGroup(
                Player0505Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0505Layout.setVerticalGroup(
                Player0505Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0506.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0506Layout = new javax.swing.GroupLayout(Player0506);
        Player0506.setLayout(Player0506Layout);
        Player0506Layout.setHorizontalGroup(
                Player0506Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0506Layout.setVerticalGroup(
                Player0506Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        NamePanel_05.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_05.setPreferredSize(new java.awt.Dimension(90, 40));
        NamePanel_05.setRequestFocusEnabled(false);

        TileName_05.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_05.setText("<HTML>READING<BR>RAILROAD</HTML>");
        TileName_05.setPreferredSize(new java.awt.Dimension(90, 40));

        javax.swing.GroupLayout NamePanel_05Layout = new javax.swing.GroupLayout(NamePanel_05);
        NamePanel_05.setLayout(NamePanel_05Layout);
        NamePanel_05Layout.setHorizontalGroup(
                NamePanel_05Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_05Layout.createSequentialGroup()
                                .addComponent(TileName_05, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        NamePanel_05Layout.setVerticalGroup(
                NamePanel_05Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_05Layout.createSequentialGroup()
                                .addComponent(TileName_05, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout Tile_05Layout = new javax.swing.GroupLayout(Tile_05);
        Tile_05.setLayout(Tile_05Layout);
        Tile_05Layout.setHorizontalGroup(
                Tile_05Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_05Layout.createSequentialGroup()
                                .addGroup(Tile_05Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player0504, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0506, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0505, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(Tile_05Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player0503, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0501, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0502, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(NamePanel_05, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        Tile_05Layout.setVerticalGroup(
                Tile_05Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_05Layout.createSequentialGroup()
                                .addComponent(NamePanel_05, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(Tile_05Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(Tile_05Layout.createSequentialGroup()
                                                .addComponent(Player0501, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGroup(Tile_05Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(Tile_05Layout.createSequentialGroup()
                                                                .addComponent(Player0502, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, 0)
                                                                .addComponent(Player0503, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(Tile_05Layout.createSequentialGroup()
                                                                .addComponent(Player0505, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, 0)
                                                                .addComponent(Player0506, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                        .addComponent(Player0504, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Tile_06.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_06.setPreferredSize(new java.awt.Dimension(90, 125));

        Player0601.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0601Layout = new javax.swing.GroupLayout(Player0601);
        Player0601.setLayout(Player0601Layout);
        Player0601Layout.setHorizontalGroup(
                Player0601Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0601Layout.setVerticalGroup(
                Player0601Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0602.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0602Layout = new javax.swing.GroupLayout(Player0602);
        Player0602.setLayout(Player0602Layout);
        Player0602Layout.setHorizontalGroup(
                Player0602Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0602Layout.setVerticalGroup(
                Player0602Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0603.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0603Layout = new javax.swing.GroupLayout(Player0603);
        Player0603.setLayout(Player0603Layout);
        Player0603Layout.setHorizontalGroup(
                Player0603Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0603Layout.setVerticalGroup(
                Player0603Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0604.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0604Layout = new javax.swing.GroupLayout(Player0604);
        Player0604.setLayout(Player0604Layout);
        Player0604Layout.setHorizontalGroup(
                Player0604Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0604Layout.setVerticalGroup(
                Player0604Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0605.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0605Layout = new javax.swing.GroupLayout(Player0605);
        Player0605.setLayout(Player0605Layout);
        Player0605Layout.setHorizontalGroup(
                Player0605Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0605Layout.setVerticalGroup(
                Player0605Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0606.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0606Layout = new javax.swing.GroupLayout(Player0606);
        Player0606.setLayout(Player0606Layout);
        Player0606Layout.setHorizontalGroup(
                Player0606Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0606Layout.setVerticalGroup(
                Player0606Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        NamePanel_06.setBackground(new java.awt.Color(102, 255, 255));
        NamePanel_06.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_06.setPreferredSize(new java.awt.Dimension(90, 40));
        NamePanel_06.setRequestFocusEnabled(false);

        TileName_06.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_06.setText("<HTML>ORIENTAL<BR>AVENUE</HTML>");
        TileName_06.setPreferredSize(new java.awt.Dimension(90, 40));

        javax.swing.GroupLayout NamePanel_06Layout = new javax.swing.GroupLayout(NamePanel_06);
        NamePanel_06.setLayout(NamePanel_06Layout);
        NamePanel_06Layout.setHorizontalGroup(
                NamePanel_06Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_06Layout.createSequentialGroup()
                                .addComponent(TileName_06, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        NamePanel_06Layout.setVerticalGroup(
                NamePanel_06Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_06Layout.createSequentialGroup()
                                .addComponent(TileName_06, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout Tile_06Layout = new javax.swing.GroupLayout(Tile_06);
        Tile_06.setLayout(Tile_06Layout);
        Tile_06Layout.setHorizontalGroup(
                Tile_06Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_06Layout.createSequentialGroup()
                                .addGroup(Tile_06Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player0604, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0606, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0605, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(Tile_06Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player0603, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0601, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0602, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(NamePanel_06, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        Tile_06Layout.setVerticalGroup(
                Tile_06Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_06Layout.createSequentialGroup()
                                .addComponent(NamePanel_06, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(Tile_06Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(Tile_06Layout.createSequentialGroup()
                                                .addComponent(Player0601, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGroup(Tile_06Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(Tile_06Layout.createSequentialGroup()
                                                                .addComponent(Player0602, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, 0)
                                                                .addComponent(Player0603, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(Tile_06Layout.createSequentialGroup()
                                                                .addComponent(Player0605, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, 0)
                                                                .addComponent(Player0606, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                        .addComponent(Player0604, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Tile_07.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_07.setPreferredSize(new java.awt.Dimension(90, 125));

        Player0701.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0701Layout = new javax.swing.GroupLayout(Player0701);
        Player0701.setLayout(Player0701Layout);
        Player0701Layout.setHorizontalGroup(
                Player0701Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0701Layout.setVerticalGroup(
                Player0701Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0702.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0702Layout = new javax.swing.GroupLayout(Player0702);
        Player0702.setLayout(Player0702Layout);
        Player0702Layout.setHorizontalGroup(
                Player0702Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0702Layout.setVerticalGroup(
                Player0702Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0703.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0703Layout = new javax.swing.GroupLayout(Player0703);
        Player0703.setLayout(Player0703Layout);
        Player0703Layout.setHorizontalGroup(
                Player0703Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0703Layout.setVerticalGroup(
                Player0703Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0704.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0704Layout = new javax.swing.GroupLayout(Player0704);
        Player0704.setLayout(Player0704Layout);
        Player0704Layout.setHorizontalGroup(
                Player0704Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0704Layout.setVerticalGroup(
                Player0704Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0705.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0705Layout = new javax.swing.GroupLayout(Player0705);
        Player0705.setLayout(Player0705Layout);
        Player0705Layout.setHorizontalGroup(
                Player0705Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0705Layout.setVerticalGroup(
                Player0705Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0706.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0706Layout = new javax.swing.GroupLayout(Player0706);
        Player0706.setLayout(Player0706Layout);
        Player0706Layout.setHorizontalGroup(
                Player0706Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0706Layout.setVerticalGroup(
                Player0706Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        NamePanel_07.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_07.setPreferredSize(new java.awt.Dimension(90, 40));
        NamePanel_07.setRequestFocusEnabled(false);

        TileName_07.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_07.setText("CHANCE");
        TileName_07.setPreferredSize(new java.awt.Dimension(90, 40));

        javax.swing.GroupLayout NamePanel_07Layout = new javax.swing.GroupLayout(NamePanel_07);
        NamePanel_07.setLayout(NamePanel_07Layout);
        NamePanel_07Layout.setHorizontalGroup(
                NamePanel_07Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_07Layout.createSequentialGroup()
                                .addComponent(TileName_07, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        NamePanel_07Layout.setVerticalGroup(
                NamePanel_07Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_07Layout.createSequentialGroup()
                                .addComponent(TileName_07, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout Tile_07Layout = new javax.swing.GroupLayout(Tile_07);
        Tile_07.setLayout(Tile_07Layout);
        Tile_07Layout.setHorizontalGroup(
                Tile_07Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_07Layout.createSequentialGroup()
                                .addGroup(Tile_07Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player0704, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0706, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0705, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(Tile_07Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player0703, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0701, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0702, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(NamePanel_07, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        Tile_07Layout.setVerticalGroup(
                Tile_07Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_07Layout.createSequentialGroup()
                                .addComponent(NamePanel_07, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(Tile_07Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(Tile_07Layout.createSequentialGroup()
                                                .addComponent(Player0701, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGroup(Tile_07Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(Tile_07Layout.createSequentialGroup()
                                                                .addComponent(Player0702, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, 0)
                                                                .addComponent(Player0703, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(Tile_07Layout.createSequentialGroup()
                                                                .addComponent(Player0705, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, 0)
                                                                .addComponent(Player0706, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                        .addComponent(Player0704, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Tile_08.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_08.setPreferredSize(new java.awt.Dimension(90, 125));

        Player0801.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0801Layout = new javax.swing.GroupLayout(Player0801);
        Player0801.setLayout(Player0801Layout);
        Player0801Layout.setHorizontalGroup(
                Player0801Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0801Layout.setVerticalGroup(
                Player0801Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0802.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0802Layout = new javax.swing.GroupLayout(Player0802);
        Player0802.setLayout(Player0802Layout);
        Player0802Layout.setHorizontalGroup(
                Player0802Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0802Layout.setVerticalGroup(
                Player0802Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0803.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0803Layout = new javax.swing.GroupLayout(Player0803);
        Player0803.setLayout(Player0803Layout);
        Player0803Layout.setHorizontalGroup(
                Player0803Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0803Layout.setVerticalGroup(
                Player0803Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0804.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0804Layout = new javax.swing.GroupLayout(Player0804);
        Player0804.setLayout(Player0804Layout);
        Player0804Layout.setHorizontalGroup(
                Player0804Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0804Layout.setVerticalGroup(
                Player0804Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0805.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0805Layout = new javax.swing.GroupLayout(Player0805);
        Player0805.setLayout(Player0805Layout);
        Player0805Layout.setHorizontalGroup(
                Player0805Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0805Layout.setVerticalGroup(
                Player0805Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0806.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0806Layout = new javax.swing.GroupLayout(Player0806);
        Player0806.setLayout(Player0806Layout);
        Player0806Layout.setHorizontalGroup(
                Player0806Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0806Layout.setVerticalGroup(
                Player0806Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        NamePanel_08.setBackground(new java.awt.Color(102, 255, 255));
        NamePanel_08.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_08.setPreferredSize(new java.awt.Dimension(90, 40));
        NamePanel_08.setRequestFocusEnabled(false);

        TileName_08.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_08.setText("<HTML>VERMONT<BR>AVENUE</HTML>");
        TileName_08.setPreferredSize(new java.awt.Dimension(90, 40));

        javax.swing.GroupLayout NamePanel_08Layout = new javax.swing.GroupLayout(NamePanel_08);
        NamePanel_08.setLayout(NamePanel_08Layout);
        NamePanel_08Layout.setHorizontalGroup(
                NamePanel_08Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_08Layout.createSequentialGroup()
                                .addComponent(TileName_08, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        NamePanel_08Layout.setVerticalGroup(
                NamePanel_08Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_08Layout.createSequentialGroup()
                                .addComponent(TileName_08, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout Tile_08Layout = new javax.swing.GroupLayout(Tile_08);
        Tile_08.setLayout(Tile_08Layout);
        Tile_08Layout.setHorizontalGroup(
                Tile_08Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_08Layout.createSequentialGroup()
                                .addGroup(Tile_08Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player0804, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0806, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0805, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(Tile_08Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player0803, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0801, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0802, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(NamePanel_08, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        Tile_08Layout.setVerticalGroup(
                Tile_08Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_08Layout.createSequentialGroup()
                                .addComponent(NamePanel_08, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(Tile_08Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(Tile_08Layout.createSequentialGroup()
                                                .addComponent(Player0801, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGroup(Tile_08Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(Tile_08Layout.createSequentialGroup()
                                                                .addComponent(Player0802, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, 0)
                                                                .addComponent(Player0803, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(Tile_08Layout.createSequentialGroup()
                                                                .addComponent(Player0805, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, 0)
                                                                .addComponent(Player0806, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                        .addComponent(Player0804, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Tile_09.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_09.setPreferredSize(new java.awt.Dimension(90, 125));

        Player0901.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0901Layout = new javax.swing.GroupLayout(Player0901);
        Player0901.setLayout(Player0901Layout);
        Player0901Layout.setHorizontalGroup(
                Player0901Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0901Layout.setVerticalGroup(
                Player0901Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0902.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0902Layout = new javax.swing.GroupLayout(Player0902);
        Player0902.setLayout(Player0902Layout);
        Player0902Layout.setHorizontalGroup(
                Player0902Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0902Layout.setVerticalGroup(
                Player0902Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0903.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0903Layout = new javax.swing.GroupLayout(Player0903);
        Player0903.setLayout(Player0903Layout);
        Player0903Layout.setHorizontalGroup(
                Player0903Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0903Layout.setVerticalGroup(
                Player0903Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0904.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0904Layout = new javax.swing.GroupLayout(Player0904);
        Player0904.setLayout(Player0904Layout);
        Player0904Layout.setHorizontalGroup(
                Player0904Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0904Layout.setVerticalGroup(
                Player0904Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0905.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0905Layout = new javax.swing.GroupLayout(Player0905);
        Player0905.setLayout(Player0905Layout);
        Player0905Layout.setHorizontalGroup(
                Player0905Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0905Layout.setVerticalGroup(
                Player0905Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player0906.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player0906Layout = new javax.swing.GroupLayout(Player0906);
        Player0906.setLayout(Player0906Layout);
        Player0906Layout.setHorizontalGroup(
                Player0906Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player0906Layout.setVerticalGroup(
                Player0906Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        NamePanel_09.setBackground(new java.awt.Color(102, 255, 255));
        NamePanel_09.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_09.setPreferredSize(new java.awt.Dimension(90, 40));
        NamePanel_09.setRequestFocusEnabled(false);

        TileName_09.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_09.setText("<HTML>CONNECTICUT<BR>AVENUE</HTML>");
        TileName_09.setPreferredSize(new java.awt.Dimension(90, 40));

        javax.swing.GroupLayout NamePanel_09Layout = new javax.swing.GroupLayout(NamePanel_09);
        NamePanel_09.setLayout(NamePanel_09Layout);
        NamePanel_09Layout.setHorizontalGroup(
                NamePanel_09Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_09Layout.createSequentialGroup()
                                .addComponent(TileName_09, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        NamePanel_09Layout.setVerticalGroup(
                NamePanel_09Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_09Layout.createSequentialGroup()
                                .addComponent(TileName_09, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout Tile_09Layout = new javax.swing.GroupLayout(Tile_09);
        Tile_09.setLayout(Tile_09Layout);
        Tile_09Layout.setHorizontalGroup(
                Tile_09Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_09Layout.createSequentialGroup()
                                .addGroup(Tile_09Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player0904, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0906, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0905, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(Tile_09Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player0903, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0901, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player0902, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(NamePanel_09, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        Tile_09Layout.setVerticalGroup(
                Tile_09Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_09Layout.createSequentialGroup()
                                .addComponent(NamePanel_09, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(Tile_09Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(Tile_09Layout.createSequentialGroup()
                                                .addComponent(Player0901, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGroup(Tile_09Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(Tile_09Layout.createSequentialGroup()
                                                                .addComponent(Player0902, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, 0)
                                                                .addComponent(Player0903, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(Tile_09Layout.createSequentialGroup()
                                                                .addComponent(Player0905, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, 0)
                                                                .addComponent(Player0906, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                        .addComponent(Player0904, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Tile_10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_10.setName(""); // NOI18N
        Tile_10.setPreferredSize(new java.awt.Dimension(125, 125));

        Player1001.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player1001Layout = new javax.swing.GroupLayout(Player1001);
        Player1001.setLayout(Player1001Layout);
        Player1001Layout.setHorizontalGroup(
                Player1001Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player1001Layout.setVerticalGroup(
                Player1001Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player1002.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player1002Layout = new javax.swing.GroupLayout(Player1002);
        Player1002.setLayout(Player1002Layout);
        Player1002Layout.setHorizontalGroup(
                Player1002Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player1002Layout.setVerticalGroup(
                Player1002Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player1003.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player1003Layout = new javax.swing.GroupLayout(Player1003);
        Player1003.setLayout(Player1003Layout);
        Player1003Layout.setHorizontalGroup(
                Player1003Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player1003Layout.setVerticalGroup(
                Player1003Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player1004.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1004Layout = new javax.swing.GroupLayout(Player1004);
        Player1004.setLayout(Player1004Layout);
        Player1004Layout.setHorizontalGroup(
                Player1004Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1004Layout.setVerticalGroup(
                Player1004Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1005.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1005Layout = new javax.swing.GroupLayout(Player1005);
        Player1005.setLayout(Player1005Layout);
        Player1005Layout.setHorizontalGroup(
                Player1005Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1005Layout.setVerticalGroup(
                Player1005Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1006.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1006Layout = new javax.swing.GroupLayout(Player1006);
        Player1006.setLayout(Player1006Layout);
        Player1006Layout.setHorizontalGroup(
                Player1006Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1006Layout.setVerticalGroup(
                Player1006Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout Tile_10Layout = new javax.swing.GroupLayout(Tile_10);
        Tile_10.setLayout(Tile_10Layout);
        Tile_10Layout.setHorizontalGroup(
                Tile_10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Tile_10Layout.createSequentialGroup()
                                .addComponent(Player1006, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(Player1005, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(Player1004, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(48, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Tile_10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, Tile_10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player1002, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1001, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(Player1003, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        Tile_10Layout.setVerticalGroup(
                Tile_10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_10Layout.createSequentialGroup()
                                .addGroup(Tile_10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(Tile_10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(Player1005, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(Player1004, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(Player1006, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addComponent(Player1003, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(Player1002, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(Player1001, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        Tile_11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_11.setPreferredSize(new java.awt.Dimension(125, 90));

        Player1101.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1101Layout = new javax.swing.GroupLayout(Player1101);
        Player1101.setLayout(Player1101Layout);
        Player1101Layout.setHorizontalGroup(
                Player1101Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1101Layout.setVerticalGroup(
                Player1101Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1102.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1102Layout = new javax.swing.GroupLayout(Player1102);
        Player1102.setLayout(Player1102Layout);
        Player1102Layout.setHorizontalGroup(
                Player1102Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1102Layout.setVerticalGroup(
                Player1102Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1103.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1103Layout = new javax.swing.GroupLayout(Player1103);
        Player1103.setLayout(Player1103Layout);
        Player1103Layout.setHorizontalGroup(
                Player1103Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1103Layout.setVerticalGroup(
                Player1103Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1104.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1104Layout = new javax.swing.GroupLayout(Player1104);
        Player1104.setLayout(Player1104Layout);
        Player1104Layout.setHorizontalGroup(
                Player1104Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1104Layout.setVerticalGroup(
                Player1104Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1105.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1105Layout = new javax.swing.GroupLayout(Player1105);
        Player1105.setLayout(Player1105Layout);
        Player1105Layout.setHorizontalGroup(
                Player1105Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1105Layout.setVerticalGroup(
                Player1105Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1106.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1106Layout = new javax.swing.GroupLayout(Player1106);
        Player1106.setLayout(Player1106Layout);
        Player1106Layout.setHorizontalGroup(
                Player1106Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1106Layout.setVerticalGroup(
                Player1106Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        NamePanel_11.setBackground(new java.awt.Color(255, 51, 102));
        NamePanel_11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_11.setPreferredSize(new java.awt.Dimension(40, 90));
        NamePanel_11.setRequestFocusEnabled(false);

        TileName_11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_11.setText("<HTML>ST.CHARLES<BR>PLACE</HTML>");
        TileName_11.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        TileName_11.setMaximumSize(new java.awt.Dimension(50000, 50000));
        TileName_11.setPreferredSize(new java.awt.Dimension(40, 90));

        javax.swing.GroupLayout NamePanel_11Layout = new javax.swing.GroupLayout(NamePanel_11);
        NamePanel_11.setLayout(NamePanel_11Layout);
        NamePanel_11Layout.setHorizontalGroup(
                NamePanel_11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_11Layout.createSequentialGroup()
                                .addComponent(TileName_11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
        );
        NamePanel_11Layout.setVerticalGroup(
                NamePanel_11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(TileName_11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout Tile_11Layout = new javax.swing.GroupLayout(Tile_11);
        Tile_11.setLayout(Tile_11Layout);
        Tile_11Layout.setHorizontalGroup(
                Tile_11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_11Layout.createSequentialGroup()
                                .addGroup(Tile_11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_11Layout.createSequentialGroup()
                                                .addComponent(Player1106, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1105, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1104, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_11Layout.createSequentialGroup()
                                                .addComponent(Player1103, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1102, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1101, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(NamePanel_11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0))
        );
        Tile_11Layout.setVerticalGroup(
                Tile_11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(NamePanel_11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(Tile_11Layout.createSequentialGroup()
                                .addGroup(Tile_11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player1106, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1105, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1104, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addGroup(Tile_11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player1103, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1102, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1101, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Tile_12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_12.setPreferredSize(new java.awt.Dimension(125, 90));

        Player1201.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1201Layout = new javax.swing.GroupLayout(Player1201);
        Player1201.setLayout(Player1201Layout);
        Player1201Layout.setHorizontalGroup(
                Player1201Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1201Layout.setVerticalGroup(
                Player1201Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1202.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1202Layout = new javax.swing.GroupLayout(Player1202);
        Player1202.setLayout(Player1202Layout);
        Player1202Layout.setHorizontalGroup(
                Player1202Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1202Layout.setVerticalGroup(
                Player1202Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1203.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1203Layout = new javax.swing.GroupLayout(Player1203);
        Player1203.setLayout(Player1203Layout);
        Player1203Layout.setHorizontalGroup(
                Player1203Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1203Layout.setVerticalGroup(
                Player1203Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1204.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1204Layout = new javax.swing.GroupLayout(Player1204);
        Player1204.setLayout(Player1204Layout);
        Player1204Layout.setHorizontalGroup(
                Player1204Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1204Layout.setVerticalGroup(
                Player1204Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1205.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1205Layout = new javax.swing.GroupLayout(Player1205);
        Player1205.setLayout(Player1205Layout);
        Player1205Layout.setHorizontalGroup(
                Player1205Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1205Layout.setVerticalGroup(
                Player1205Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1206.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1206Layout = new javax.swing.GroupLayout(Player1206);
        Player1206.setLayout(Player1206Layout);
        Player1206Layout.setHorizontalGroup(
                Player1206Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1206Layout.setVerticalGroup(
                Player1206Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        NamePanel_12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_12.setPreferredSize(new java.awt.Dimension(40, 90));
        NamePanel_12.setRequestFocusEnabled(false);

        TileName_12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_12.setText("<HTML>ELECTRIC<BR>COMPANY</HTML>");
        TileName_12.setMaximumSize(new java.awt.Dimension(50000, 50000));
        TileName_12.setName(""); // NOI18N
        TileName_12.setPreferredSize(new java.awt.Dimension(40, 90));

        javax.swing.GroupLayout NamePanel_12Layout = new javax.swing.GroupLayout(NamePanel_12);
        NamePanel_12.setLayout(NamePanel_12Layout);
        NamePanel_12Layout.setHorizontalGroup(
                NamePanel_12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_12Layout.createSequentialGroup()
                                .addComponent(TileName_12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
        );
        NamePanel_12Layout.setVerticalGroup(
                NamePanel_12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(TileName_12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout Tile_12Layout = new javax.swing.GroupLayout(Tile_12);
        Tile_12.setLayout(Tile_12Layout);
        Tile_12Layout.setHorizontalGroup(
                Tile_12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_12Layout.createSequentialGroup()
                                .addGroup(Tile_12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_12Layout.createSequentialGroup()
                                                .addComponent(Player1206, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1205, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1204, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_12Layout.createSequentialGroup()
                                                .addComponent(Player1203, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1202, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1201, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(NamePanel_12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        Tile_12Layout.setVerticalGroup(
                Tile_12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(NamePanel_12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(Tile_12Layout.createSequentialGroup()
                                .addGroup(Tile_12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player1206, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1205, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1204, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addGroup(Tile_12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player1203, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1202, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1201, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Tile_13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_13.setPreferredSize(new java.awt.Dimension(125, 90));

        Player1301.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1301Layout = new javax.swing.GroupLayout(Player1301);
        Player1301.setLayout(Player1301Layout);
        Player1301Layout.setHorizontalGroup(
                Player1301Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1301Layout.setVerticalGroup(
                Player1301Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1302.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1302Layout = new javax.swing.GroupLayout(Player1302);
        Player1302.setLayout(Player1302Layout);
        Player1302Layout.setHorizontalGroup(
                Player1302Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1302Layout.setVerticalGroup(
                Player1302Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1303.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1303Layout = new javax.swing.GroupLayout(Player1303);
        Player1303.setLayout(Player1303Layout);
        Player1303Layout.setHorizontalGroup(
                Player1303Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1303Layout.setVerticalGroup(
                Player1303Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1304.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1304Layout = new javax.swing.GroupLayout(Player1304);
        Player1304.setLayout(Player1304Layout);
        Player1304Layout.setHorizontalGroup(
                Player1304Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1304Layout.setVerticalGroup(
                Player1304Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1305.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1305Layout = new javax.swing.GroupLayout(Player1305);
        Player1305.setLayout(Player1305Layout);
        Player1305Layout.setHorizontalGroup(
                Player1305Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1305Layout.setVerticalGroup(
                Player1305Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1306.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1306Layout = new javax.swing.GroupLayout(Player1306);
        Player1306.setLayout(Player1306Layout);
        Player1306Layout.setHorizontalGroup(
                Player1306Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1306Layout.setVerticalGroup(
                Player1306Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        NamePanel_13.setBackground(new java.awt.Color(255, 51, 102));
        NamePanel_13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_13.setPreferredSize(new java.awt.Dimension(40, 90));
        NamePanel_13.setRequestFocusEnabled(false);

        TileName_13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_13.setText("<HTML>STATES<BR>AVENUE</HTML>");
        TileName_13.setMaximumSize(new java.awt.Dimension(50000, 50000));
        TileName_13.setPreferredSize(new java.awt.Dimension(40, 90));

        javax.swing.GroupLayout NamePanel_13Layout = new javax.swing.GroupLayout(NamePanel_13);
        NamePanel_13.setLayout(NamePanel_13Layout);
        NamePanel_13Layout.setHorizontalGroup(
                NamePanel_13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_13Layout.createSequentialGroup()
                                .addComponent(TileName_13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
        );
        NamePanel_13Layout.setVerticalGroup(
                NamePanel_13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(TileName_13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout Tile_13Layout = new javax.swing.GroupLayout(Tile_13);
        Tile_13.setLayout(Tile_13Layout);
        Tile_13Layout.setHorizontalGroup(
                Tile_13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_13Layout.createSequentialGroup()
                                .addGroup(Tile_13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_13Layout.createSequentialGroup()
                                                .addComponent(Player1306, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1305, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1304, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_13Layout.createSequentialGroup()
                                                .addComponent(Player1303, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1302, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1301, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(NamePanel_13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        Tile_13Layout.setVerticalGroup(
                Tile_13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(NamePanel_13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(Tile_13Layout.createSequentialGroup()
                                .addGroup(Tile_13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player1306, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1305, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1304, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addGroup(Tile_13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player1303, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1302, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1301, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Tile_14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_14.setPreferredSize(new java.awt.Dimension(125, 90));

        Player1401.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1401Layout = new javax.swing.GroupLayout(Player1401);
        Player1401.setLayout(Player1401Layout);
        Player1401Layout.setHorizontalGroup(
                Player1401Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1401Layout.setVerticalGroup(
                Player1401Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1402.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1402Layout = new javax.swing.GroupLayout(Player1402);
        Player1402.setLayout(Player1402Layout);
        Player1402Layout.setHorizontalGroup(
                Player1402Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1402Layout.setVerticalGroup(
                Player1402Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1404.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1404Layout = new javax.swing.GroupLayout(Player1404);
        Player1404.setLayout(Player1404Layout);
        Player1404Layout.setHorizontalGroup(
                Player1404Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1404Layout.setVerticalGroup(
                Player1404Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1403.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1403Layout = new javax.swing.GroupLayout(Player1403);
        Player1403.setLayout(Player1403Layout);
        Player1403Layout.setHorizontalGroup(
                Player1403Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1403Layout.setVerticalGroup(
                Player1403Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1405.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1405Layout = new javax.swing.GroupLayout(Player1405);
        Player1405.setLayout(Player1405Layout);
        Player1405Layout.setHorizontalGroup(
                Player1405Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1405Layout.setVerticalGroup(
                Player1405Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1406.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1406Layout = new javax.swing.GroupLayout(Player1406);
        Player1406.setLayout(Player1406Layout);
        Player1406Layout.setHorizontalGroup(
                Player1406Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1406Layout.setVerticalGroup(
                Player1406Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        NamePanel_14.setBackground(new java.awt.Color(255, 51, 102));
        NamePanel_14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_14.setPreferredSize(new java.awt.Dimension(40, 90));
        NamePanel_14.setRequestFocusEnabled(false);

        TileName_14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_14.setText("<HTML>VIRGINIA<BR>AVENUE</HTML>");
        TileName_14.setMaximumSize(new java.awt.Dimension(50000, 50000));
        TileName_14.setPreferredSize(new java.awt.Dimension(40, 90));

        javax.swing.GroupLayout NamePanel_14Layout = new javax.swing.GroupLayout(NamePanel_14);
        NamePanel_14.setLayout(NamePanel_14Layout);
        NamePanel_14Layout.setHorizontalGroup(
                NamePanel_14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(TileName_14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        NamePanel_14Layout.setVerticalGroup(
                NamePanel_14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(TileName_14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout Tile_14Layout = new javax.swing.GroupLayout(Tile_14);
        Tile_14.setLayout(Tile_14Layout);
        Tile_14Layout.setHorizontalGroup(
                Tile_14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_14Layout.createSequentialGroup()
                                .addGroup(Tile_14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_14Layout.createSequentialGroup()
                                                .addComponent(Player1406, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1405, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1404, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_14Layout.createSequentialGroup()
                                                .addComponent(Player1403, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1402, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1401, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(NamePanel_14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        Tile_14Layout.setVerticalGroup(
                Tile_14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(NamePanel_14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(Tile_14Layout.createSequentialGroup()
                                .addGroup(Tile_14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player1406, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1405, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1404, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addGroup(Tile_14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player1403, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1402, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1401, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Tile_15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_15.setPreferredSize(new java.awt.Dimension(125, 90));

        Player1501.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1501Layout = new javax.swing.GroupLayout(Player1501);
        Player1501.setLayout(Player1501Layout);
        Player1501Layout.setHorizontalGroup(
                Player1501Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1501Layout.setVerticalGroup(
                Player1501Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1502.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1502Layout = new javax.swing.GroupLayout(Player1502);
        Player1502.setLayout(Player1502Layout);
        Player1502Layout.setHorizontalGroup(
                Player1502Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1502Layout.setVerticalGroup(
                Player1502Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1504.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1504Layout = new javax.swing.GroupLayout(Player1504);
        Player1504.setLayout(Player1504Layout);
        Player1504Layout.setHorizontalGroup(
                Player1504Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1504Layout.setVerticalGroup(
                Player1504Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1503.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1503Layout = new javax.swing.GroupLayout(Player1503);
        Player1503.setLayout(Player1503Layout);
        Player1503Layout.setHorizontalGroup(
                Player1503Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1503Layout.setVerticalGroup(
                Player1503Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1505.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1505Layout = new javax.swing.GroupLayout(Player1505);
        Player1505.setLayout(Player1505Layout);
        Player1505Layout.setHorizontalGroup(
                Player1505Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1505Layout.setVerticalGroup(
                Player1505Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1506.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1506Layout = new javax.swing.GroupLayout(Player1506);
        Player1506.setLayout(Player1506Layout);
        Player1506Layout.setHorizontalGroup(
                Player1506Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1506Layout.setVerticalGroup(
                Player1506Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        NamePanel_15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_15.setPreferredSize(new java.awt.Dimension(40, 90));
        NamePanel_15.setRequestFocusEnabled(false);

        TileName_15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_15.setText("<HTML>PENNSYLVANIA<BR>RAILROAD</HTML>");
        TileName_15.setMaximumSize(new java.awt.Dimension(50000, 50000));
        TileName_15.setPreferredSize(new java.awt.Dimension(40, 90));

        javax.swing.GroupLayout NamePanel_15Layout = new javax.swing.GroupLayout(NamePanel_15);
        NamePanel_15.setLayout(NamePanel_15Layout);
        NamePanel_15Layout.setHorizontalGroup(
                NamePanel_15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_15Layout.createSequentialGroup()
                                .addComponent(TileName_15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
        );
        NamePanel_15Layout.setVerticalGroup(
                NamePanel_15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(TileName_15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout Tile_15Layout = new javax.swing.GroupLayout(Tile_15);
        Tile_15.setLayout(Tile_15Layout);
        Tile_15Layout.setHorizontalGroup(
                Tile_15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_15Layout.createSequentialGroup()
                                .addGroup(Tile_15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_15Layout.createSequentialGroup()
                                                .addComponent(Player1506, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1505, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1504, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_15Layout.createSequentialGroup()
                                                .addComponent(Player1503, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1502, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1501, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(NamePanel_15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        Tile_15Layout.setVerticalGroup(
                Tile_15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(NamePanel_15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(Tile_15Layout.createSequentialGroup()
                                .addGroup(Tile_15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player1506, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1505, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1504, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addGroup(Tile_15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player1503, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1502, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1501, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Tile_16.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_16.setPreferredSize(new java.awt.Dimension(125, 90));

        Player1601.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1601Layout = new javax.swing.GroupLayout(Player1601);
        Player1601.setLayout(Player1601Layout);
        Player1601Layout.setHorizontalGroup(
                Player1601Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1601Layout.setVerticalGroup(
                Player1601Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1602.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1602Layout = new javax.swing.GroupLayout(Player1602);
        Player1602.setLayout(Player1602Layout);
        Player1602Layout.setHorizontalGroup(
                Player1602Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1602Layout.setVerticalGroup(
                Player1602Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1603.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1603Layout = new javax.swing.GroupLayout(Player1603);
        Player1603.setLayout(Player1603Layout);
        Player1603Layout.setHorizontalGroup(
                Player1603Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1603Layout.setVerticalGroup(
                Player1603Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1604.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1604Layout = new javax.swing.GroupLayout(Player1604);
        Player1604.setLayout(Player1604Layout);
        Player1604Layout.setHorizontalGroup(
                Player1604Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1604Layout.setVerticalGroup(
                Player1604Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1605.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1605Layout = new javax.swing.GroupLayout(Player1605);
        Player1605.setLayout(Player1605Layout);
        Player1605Layout.setHorizontalGroup(
                Player1605Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1605Layout.setVerticalGroup(
                Player1605Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1606.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1606Layout = new javax.swing.GroupLayout(Player1606);
        Player1606.setLayout(Player1606Layout);
        Player1606Layout.setHorizontalGroup(
                Player1606Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1606Layout.setVerticalGroup(
                Player1606Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        NamePanel_16.setBackground(new java.awt.Color(255, 153, 0));
        NamePanel_16.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_16.setPreferredSize(new java.awt.Dimension(40, 90));
        NamePanel_16.setRequestFocusEnabled(false);

        TileName_16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_16.setText("<HTML>ST. JAMES<BR>PLACE</HTML>");
        TileName_16.setMaximumSize(new java.awt.Dimension(50000, 50000));
        TileName_16.setPreferredSize(new java.awt.Dimension(40, 90));

        javax.swing.GroupLayout NamePanel_16Layout = new javax.swing.GroupLayout(NamePanel_16);
        NamePanel_16.setLayout(NamePanel_16Layout);
        NamePanel_16Layout.setHorizontalGroup(
                NamePanel_16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(TileName_16, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
        );
        NamePanel_16Layout.setVerticalGroup(
                NamePanel_16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(TileName_16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout Tile_16Layout = new javax.swing.GroupLayout(Tile_16);
        Tile_16.setLayout(Tile_16Layout);
        Tile_16Layout.setHorizontalGroup(
                Tile_16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_16Layout.createSequentialGroup()
                                .addGroup(Tile_16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_16Layout.createSequentialGroup()
                                                .addComponent(Player1606, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1605, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1604, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_16Layout.createSequentialGroup()
                                                .addComponent(Player1603, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1602, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1601, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(NamePanel_16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        Tile_16Layout.setVerticalGroup(
                Tile_16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(NamePanel_16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(Tile_16Layout.createSequentialGroup()
                                .addGroup(Tile_16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player1606, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1605, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1604, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addGroup(Tile_16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player1603, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1602, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1601, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Tile_17.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_17.setPreferredSize(new java.awt.Dimension(125, 90));

        Player1701.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1701Layout = new javax.swing.GroupLayout(Player1701);
        Player1701.setLayout(Player1701Layout);
        Player1701Layout.setHorizontalGroup(
                Player1701Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1701Layout.setVerticalGroup(
                Player1701Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1702.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1702Layout = new javax.swing.GroupLayout(Player1702);
        Player1702.setLayout(Player1702Layout);
        Player1702Layout.setHorizontalGroup(
                Player1702Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1702Layout.setVerticalGroup(
                Player1702Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1703.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1703Layout = new javax.swing.GroupLayout(Player1703);
        Player1703.setLayout(Player1703Layout);
        Player1703Layout.setHorizontalGroup(
                Player1703Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1703Layout.setVerticalGroup(
                Player1703Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1704.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1704Layout = new javax.swing.GroupLayout(Player1704);
        Player1704.setLayout(Player1704Layout);
        Player1704Layout.setHorizontalGroup(
                Player1704Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1704Layout.setVerticalGroup(
                Player1704Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1705.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1705Layout = new javax.swing.GroupLayout(Player1705);
        Player1705.setLayout(Player1705Layout);
        Player1705Layout.setHorizontalGroup(
                Player1705Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1705Layout.setVerticalGroup(
                Player1705Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1706.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1706Layout = new javax.swing.GroupLayout(Player1706);
        Player1706.setLayout(Player1706Layout);
        Player1706Layout.setHorizontalGroup(
                Player1706Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1706Layout.setVerticalGroup(
                Player1706Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        NamePanel_17.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_17.setPreferredSize(new java.awt.Dimension(40, 90));
        NamePanel_17.setRequestFocusEnabled(false);

        TileName_17.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_17.setText("<HTML>COMMUNITY<BR>CHEST</HTML>");
        TileName_17.setMaximumSize(new java.awt.Dimension(50000, 50000));
        TileName_17.setPreferredSize(new java.awt.Dimension(40, 90));

        javax.swing.GroupLayout NamePanel_17Layout = new javax.swing.GroupLayout(NamePanel_17);
        NamePanel_17.setLayout(NamePanel_17Layout);
        NamePanel_17Layout.setHorizontalGroup(
                NamePanel_17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, NamePanel_17Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(TileName_17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        NamePanel_17Layout.setVerticalGroup(
                NamePanel_17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(TileName_17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout Tile_17Layout = new javax.swing.GroupLayout(Tile_17);
        Tile_17.setLayout(Tile_17Layout);
        Tile_17Layout.setHorizontalGroup(
                Tile_17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_17Layout.createSequentialGroup()
                                .addGroup(Tile_17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_17Layout.createSequentialGroup()
                                                .addComponent(Player1706, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1705, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1704, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_17Layout.createSequentialGroup()
                                                .addComponent(Player1703, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1702, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1701, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(NamePanel_17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        Tile_17Layout.setVerticalGroup(
                Tile_17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(NamePanel_17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(Tile_17Layout.createSequentialGroup()
                                .addGroup(Tile_17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player1706, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1705, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1704, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addGroup(Tile_17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player1703, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1702, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1701, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Tile_18.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_18.setPreferredSize(new java.awt.Dimension(125, 90));

        Player1801.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1801Layout = new javax.swing.GroupLayout(Player1801);
        Player1801.setLayout(Player1801Layout);
        Player1801Layout.setHorizontalGroup(
                Player1801Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1801Layout.setVerticalGroup(
                Player1801Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1802.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1802Layout = new javax.swing.GroupLayout(Player1802);
        Player1802.setLayout(Player1802Layout);
        Player1802Layout.setHorizontalGroup(
                Player1802Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1802Layout.setVerticalGroup(
                Player1802Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1803.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1803Layout = new javax.swing.GroupLayout(Player1803);
        Player1803.setLayout(Player1803Layout);
        Player1803Layout.setHorizontalGroup(
                Player1803Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1803Layout.setVerticalGroup(
                Player1803Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1804.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1804Layout = new javax.swing.GroupLayout(Player1804);
        Player1804.setLayout(Player1804Layout);
        Player1804Layout.setHorizontalGroup(
                Player1804Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1804Layout.setVerticalGroup(
                Player1804Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1805.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1805Layout = new javax.swing.GroupLayout(Player1805);
        Player1805.setLayout(Player1805Layout);
        Player1805Layout.setHorizontalGroup(
                Player1805Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1805Layout.setVerticalGroup(
                Player1805Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1806.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1806Layout = new javax.swing.GroupLayout(Player1806);
        Player1806.setLayout(Player1806Layout);
        Player1806Layout.setHorizontalGroup(
                Player1806Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1806Layout.setVerticalGroup(
                Player1806Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        NamePanel_18.setBackground(new java.awt.Color(255, 153, 0));
        NamePanel_18.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_18.setPreferredSize(new java.awt.Dimension(40, 90));
        NamePanel_18.setRequestFocusEnabled(false);

        TileName_18.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_18.setText("<HTML>TENESSEE<BR>AVENUE</HTML>");
        TileName_18.setMaximumSize(new java.awt.Dimension(50000, 50000));
        TileName_18.setPreferredSize(new java.awt.Dimension(40, 90));

        javax.swing.GroupLayout NamePanel_18Layout = new javax.swing.GroupLayout(NamePanel_18);
        NamePanel_18.setLayout(NamePanel_18Layout);
        NamePanel_18Layout.setHorizontalGroup(
                NamePanel_18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(TileName_18, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        NamePanel_18Layout.setVerticalGroup(
                NamePanel_18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(TileName_18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout Tile_18Layout = new javax.swing.GroupLayout(Tile_18);
        Tile_18.setLayout(Tile_18Layout);
        Tile_18Layout.setHorizontalGroup(
                Tile_18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_18Layout.createSequentialGroup()
                                .addGroup(Tile_18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_18Layout.createSequentialGroup()
                                                .addComponent(Player1806, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1805, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1804, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_18Layout.createSequentialGroup()
                                                .addComponent(Player1803, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(Player1802, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1801, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(NamePanel_18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0))
        );
        Tile_18Layout.setVerticalGroup(
                Tile_18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(NamePanel_18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(Tile_18Layout.createSequentialGroup()
                                .addGroup(Tile_18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player1806, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1805, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1804, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addGroup(Tile_18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player1803, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1802, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1801, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Tile_19.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_19.setPreferredSize(new java.awt.Dimension(125, 90));

        Player1901.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1901Layout = new javax.swing.GroupLayout(Player1901);
        Player1901.setLayout(Player1901Layout);
        Player1901Layout.setHorizontalGroup(
                Player1901Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1901Layout.setVerticalGroup(
                Player1901Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1902.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1902Layout = new javax.swing.GroupLayout(Player1902);
        Player1902.setLayout(Player1902Layout);
        Player1902Layout.setHorizontalGroup(
                Player1902Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1902Layout.setVerticalGroup(
                Player1902Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1903.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1903Layout = new javax.swing.GroupLayout(Player1903);
        Player1903.setLayout(Player1903Layout);
        Player1903Layout.setHorizontalGroup(
                Player1903Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1903Layout.setVerticalGroup(
                Player1903Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1904.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1904Layout = new javax.swing.GroupLayout(Player1904);
        Player1904.setLayout(Player1904Layout);
        Player1904Layout.setHorizontalGroup(
                Player1904Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1904Layout.setVerticalGroup(
                Player1904Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1905.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1905Layout = new javax.swing.GroupLayout(Player1905);
        Player1905.setLayout(Player1905Layout);
        Player1905Layout.setHorizontalGroup(
                Player1905Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1905Layout.setVerticalGroup(
                Player1905Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player1906.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player1906Layout = new javax.swing.GroupLayout(Player1906);
        Player1906.setLayout(Player1906Layout);
        Player1906Layout.setHorizontalGroup(
                Player1906Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player1906Layout.setVerticalGroup(
                Player1906Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        NamePanel_19.setBackground(new java.awt.Color(255, 153, 0));
        NamePanel_19.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_19.setPreferredSize(new java.awt.Dimension(40, 90));
        NamePanel_19.setRequestFocusEnabled(false);

        TileName_19.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_19.setText("<HTML>NEW YORK<BR>AVENUE</HTML>");
        TileName_19.setMaximumSize(new java.awt.Dimension(50000, 50000));
        TileName_19.setPreferredSize(new java.awt.Dimension(40, 90));

        javax.swing.GroupLayout NamePanel_19Layout = new javax.swing.GroupLayout(NamePanel_19);
        NamePanel_19.setLayout(NamePanel_19Layout);
        NamePanel_19Layout.setHorizontalGroup(
                NamePanel_19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, NamePanel_19Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(TileName_19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        NamePanel_19Layout.setVerticalGroup(
                NamePanel_19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(TileName_19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout Tile_19Layout = new javax.swing.GroupLayout(Tile_19);
        Tile_19.setLayout(Tile_19Layout);
        Tile_19Layout.setHorizontalGroup(
                Tile_19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_19Layout.createSequentialGroup()
                                .addGroup(Tile_19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_19Layout.createSequentialGroup()
                                                .addComponent(Player1906, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1905, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1904, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_19Layout.createSequentialGroup()
                                                .addComponent(Player1903, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1902, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player1901, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(NamePanel_19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        Tile_19Layout.setVerticalGroup(
                Tile_19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(NamePanel_19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(Tile_19Layout.createSequentialGroup()
                                .addGroup(Tile_19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player1906, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1905, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1904, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addGroup(Tile_19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player1903, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1902, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1901, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Tile_20.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_20.setPreferredSize(new java.awt.Dimension(125, 125));

        Player2001.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player2001Layout = new javax.swing.GroupLayout(Player2001);
        Player2001.setLayout(Player2001Layout);
        Player2001Layout.setHorizontalGroup(
                Player2001Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player2001Layout.setVerticalGroup(
                Player2001Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player2002.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player2002Layout = new javax.swing.GroupLayout(Player2002);
        Player2002.setLayout(Player2002Layout);
        Player2002Layout.setHorizontalGroup(
                Player2002Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player2002Layout.setVerticalGroup(
                Player2002Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player2003.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player2003Layout = new javax.swing.GroupLayout(Player2003);
        Player2003.setLayout(Player2003Layout);
        Player2003Layout.setHorizontalGroup(
                Player2003Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player2003Layout.setVerticalGroup(
                Player2003Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player2004.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2004Layout = new javax.swing.GroupLayout(Player2004);
        Player2004.setLayout(Player2004Layout);
        Player2004Layout.setHorizontalGroup(
                Player2004Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2004Layout.setVerticalGroup(
                Player2004Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2005.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2005Layout = new javax.swing.GroupLayout(Player2005);
        Player2005.setLayout(Player2005Layout);
        Player2005Layout.setHorizontalGroup(
                Player2005Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2005Layout.setVerticalGroup(
                Player2005Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2006.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2006Layout = new javax.swing.GroupLayout(Player2006);
        Player2006.setLayout(Player2006Layout);
        Player2006Layout.setHorizontalGroup(
                Player2006Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2006Layout.setVerticalGroup(
                Player2006Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout Tile_20Layout = new javax.swing.GroupLayout(Tile_20);
        Tile_20.setLayout(Tile_20Layout);
        Tile_20Layout.setHorizontalGroup(
                Tile_20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(Tile_20Layout.createSequentialGroup()
                                .addGroup(Tile_20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(Tile_20Layout.createSequentialGroup()
                                                .addComponent(Player2001, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player2002, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player2003, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_20Layout.createSequentialGroup()
                                                .addGap(77, 77, 77)
                                                .addGroup(Tile_20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, Tile_20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addComponent(Player2005, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(Player2004, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addComponent(Player2006, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(78, 78, 78))
        );
        Tile_20Layout.setVerticalGroup(
                Tile_20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, Tile_20Layout.createSequentialGroup()
                                .addComponent(Player2006, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(Player2005, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(Player2004, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(Tile_20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player2002, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player2003, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(Player2001, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        Tile_21.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_21.setPreferredSize(new java.awt.Dimension(90, 125));

        Player2101.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2101Layout = new javax.swing.GroupLayout(Player2101);
        Player2101.setLayout(Player2101Layout);
        Player2101Layout.setHorizontalGroup(
                Player2101Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2101Layout.setVerticalGroup(
                Player2101Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2102.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2102Layout = new javax.swing.GroupLayout(Player2102);
        Player2102.setLayout(Player2102Layout);
        Player2102Layout.setHorizontalGroup(
                Player2102Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2102Layout.setVerticalGroup(
                Player2102Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2103.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2103Layout = new javax.swing.GroupLayout(Player2103);
        Player2103.setLayout(Player2103Layout);
        Player2103Layout.setHorizontalGroup(
                Player2103Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2103Layout.setVerticalGroup(
                Player2103Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2104.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2104Layout = new javax.swing.GroupLayout(Player2104);
        Player2104.setLayout(Player2104Layout);
        Player2104Layout.setHorizontalGroup(
                Player2104Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2104Layout.setVerticalGroup(
                Player2104Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2105.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2105Layout = new javax.swing.GroupLayout(Player2105);
        Player2105.setLayout(Player2105Layout);
        Player2105Layout.setHorizontalGroup(
                Player2105Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2105Layout.setVerticalGroup(
                Player2105Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2106.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2106Layout = new javax.swing.GroupLayout(Player2106);
        Player2106.setLayout(Player2106Layout);
        Player2106Layout.setHorizontalGroup(
                Player2106Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2106Layout.setVerticalGroup(
                Player2106Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        NamePanel_21.setBackground(new java.awt.Color(255, 0, 0));
        NamePanel_21.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_21.setPreferredSize(new java.awt.Dimension(90, 40));
        NamePanel_21.setRequestFocusEnabled(false);

        TileName_21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_21.setText("<HTML>KENTUCKY<BR>AVENUE</HTML>");
        TileName_21.setPreferredSize(new java.awt.Dimension(90, 40));

        javax.swing.GroupLayout NamePanel_21Layout = new javax.swing.GroupLayout(NamePanel_21);
        NamePanel_21.setLayout(NamePanel_21Layout);
        NamePanel_21Layout.setHorizontalGroup(
                NamePanel_21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, NamePanel_21Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(TileName_21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(28, 28, 28))
        );
        NamePanel_21Layout.setVerticalGroup(
                NamePanel_21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(TileName_21, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout Tile_21Layout = new javax.swing.GroupLayout(Tile_21);
        Tile_21.setLayout(Tile_21Layout);
        Tile_21Layout.setHorizontalGroup(
                Tile_21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_21Layout.createSequentialGroup()
                                .addGroup(Tile_21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(NamePanel_21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(Tile_21Layout.createSequentialGroup()
                                                .addGroup(Tile_21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(Player2101, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2103, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2102, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(Tile_21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(Player2104, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2106, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2105, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(0, 0, 0))
        );
        Tile_21Layout.setVerticalGroup(
                Tile_21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_21Layout.createSequentialGroup()
                                .addGroup(Tile_21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player2101, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player2106, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(Tile_21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_21Layout.createSequentialGroup()
                                                .addComponent(Player2105, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player2104, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_21Layout.createSequentialGroup()
                                                .addComponent(Player2102, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player2103, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(NamePanel_21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        Tile_22.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_22.setPreferredSize(new java.awt.Dimension(90, 125));

        Player2201.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2201Layout = new javax.swing.GroupLayout(Player2201);
        Player2201.setLayout(Player2201Layout);
        Player2201Layout.setHorizontalGroup(
                Player2201Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2201Layout.setVerticalGroup(
                Player2201Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2202.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2202Layout = new javax.swing.GroupLayout(Player2202);
        Player2202.setLayout(Player2202Layout);
        Player2202Layout.setHorizontalGroup(
                Player2202Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2202Layout.setVerticalGroup(
                Player2202Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2203.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2203Layout = new javax.swing.GroupLayout(Player2203);
        Player2203.setLayout(Player2203Layout);
        Player2203Layout.setHorizontalGroup(
                Player2203Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2203Layout.setVerticalGroup(
                Player2203Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2204.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2204Layout = new javax.swing.GroupLayout(Player2204);
        Player2204.setLayout(Player2204Layout);
        Player2204Layout.setHorizontalGroup(
                Player2204Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2204Layout.setVerticalGroup(
                Player2204Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2205.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2205Layout = new javax.swing.GroupLayout(Player2205);
        Player2205.setLayout(Player2205Layout);
        Player2205Layout.setHorizontalGroup(
                Player2205Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2205Layout.setVerticalGroup(
                Player2205Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2206.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2206Layout = new javax.swing.GroupLayout(Player2206);
        Player2206.setLayout(Player2206Layout);
        Player2206Layout.setHorizontalGroup(
                Player2206Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2206Layout.setVerticalGroup(
                Player2206Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        NamePanel_22.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_22.setPreferredSize(new java.awt.Dimension(90, 40));
        NamePanel_22.setRequestFocusEnabled(false);

        TileName_22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_22.setText("CHANCE");
        TileName_22.setPreferredSize(new java.awt.Dimension(90, 40));

        javax.swing.GroupLayout NamePanel_22Layout = new javax.swing.GroupLayout(NamePanel_22);
        NamePanel_22.setLayout(NamePanel_22Layout);
        NamePanel_22Layout.setHorizontalGroup(
                NamePanel_22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, NamePanel_22Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(TileName_22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(28, 28, 28))
        );
        NamePanel_22Layout.setVerticalGroup(
                NamePanel_22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_22Layout.createSequentialGroup()
                                .addComponent(TileName_22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout Tile_22Layout = new javax.swing.GroupLayout(Tile_22);
        Tile_22.setLayout(Tile_22Layout);
        Tile_22Layout.setHorizontalGroup(
                Tile_22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_22Layout.createSequentialGroup()
                                .addGroup(Tile_22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(NamePanel_22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(Tile_22Layout.createSequentialGroup()
                                                .addGroup(Tile_22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(Player2201, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2203, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2202, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(Tile_22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(Player2204, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2206, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2205, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(0, 0, 0))
        );
        Tile_22Layout.setVerticalGroup(
                Tile_22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_22Layout.createSequentialGroup()
                                .addGroup(Tile_22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player2201, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player2206, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(Tile_22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_22Layout.createSequentialGroup()
                                                .addComponent(Player2205, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player2204, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_22Layout.createSequentialGroup()
                                                .addComponent(Player2202, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player2203, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                                .addComponent(NamePanel_22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        Tile_23.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_23.setPreferredSize(new java.awt.Dimension(90, 125));

        Player2301.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2301Layout = new javax.swing.GroupLayout(Player2301);
        Player2301.setLayout(Player2301Layout);
        Player2301Layout.setHorizontalGroup(
                Player2301Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2301Layout.setVerticalGroup(
                Player2301Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2302.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2302Layout = new javax.swing.GroupLayout(Player2302);
        Player2302.setLayout(Player2302Layout);
        Player2302Layout.setHorizontalGroup(
                Player2302Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2302Layout.setVerticalGroup(
                Player2302Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2303.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2303Layout = new javax.swing.GroupLayout(Player2303);
        Player2303.setLayout(Player2303Layout);
        Player2303Layout.setHorizontalGroup(
                Player2303Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2303Layout.setVerticalGroup(
                Player2303Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2304.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2304Layout = new javax.swing.GroupLayout(Player2304);
        Player2304.setLayout(Player2304Layout);
        Player2304Layout.setHorizontalGroup(
                Player2304Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2304Layout.setVerticalGroup(
                Player2304Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2305.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2305Layout = new javax.swing.GroupLayout(Player2305);
        Player2305.setLayout(Player2305Layout);
        Player2305Layout.setHorizontalGroup(
                Player2305Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2305Layout.setVerticalGroup(
                Player2305Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2306.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2306Layout = new javax.swing.GroupLayout(Player2306);
        Player2306.setLayout(Player2306Layout);
        Player2306Layout.setHorizontalGroup(
                Player2306Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2306Layout.setVerticalGroup(
                Player2306Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        

        NamePanel_23.setBackground(new java.awt.Color(255, 0, 0));
        NamePanel_23.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_23.setPreferredSize(new java.awt.Dimension(90, 40));
        NamePanel_23.setRequestFocusEnabled(false);

        TileName_23.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_23.setText("<HTML>INDIANA<BR>AVENUE</HTML>");
        TileName_23.setPreferredSize(new java.awt.Dimension(90, 40));

        javax.swing.GroupLayout NamePanel_23Layout = new javax.swing.GroupLayout(NamePanel_23);
        NamePanel_23.setLayout(NamePanel_23Layout);
        NamePanel_23Layout.setHorizontalGroup(
                NamePanel_23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, NamePanel_23Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(TileName_23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(28, 28, 28))
        );
        NamePanel_23Layout.setVerticalGroup(
                NamePanel_23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_23Layout.createSequentialGroup()
                                .addComponent(TileName_23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout Tile_23Layout = new javax.swing.GroupLayout(Tile_23);
        Tile_23.setLayout(Tile_23Layout);
        Tile_23Layout.setHorizontalGroup(
                Tile_23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_23Layout.createSequentialGroup()
                                .addGroup(Tile_23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(NamePanel_23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(Tile_23Layout.createSequentialGroup()
                                                .addGroup(Tile_23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(Player2301, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2303, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2302, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(Tile_23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(Player2304, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2306, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2305, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(0, 0, 0))
        );
        Tile_23Layout.setVerticalGroup(
                Tile_23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_23Layout.createSequentialGroup()
                                .addGroup(Tile_23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player2301, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player2306, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(Tile_23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_23Layout.createSequentialGroup()
                                                .addComponent(Player2305, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player2304, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_23Layout.createSequentialGroup()
                                                .addComponent(Player2302, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player2303, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                                .addComponent(NamePanel_23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        Tile_24.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_24.setPreferredSize(new java.awt.Dimension(90, 125));

        Player2401.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2401Layout = new javax.swing.GroupLayout(Player2401);
        Player2401.setLayout(Player2401Layout);
        Player2401Layout.setHorizontalGroup(
                Player2401Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2401Layout.setVerticalGroup(
                Player2401Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2402.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2402Layout = new javax.swing.GroupLayout(Player2402);
        Player2402.setLayout(Player2402Layout);
        Player2402Layout.setHorizontalGroup(
                Player2402Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2402Layout.setVerticalGroup(
                Player2402Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2403.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2403Layout = new javax.swing.GroupLayout(Player2403);
        Player2403.setLayout(Player2403Layout);
        Player2403Layout.setHorizontalGroup(
                Player2403Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2403Layout.setVerticalGroup(
                Player2403Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2404.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2404Layout = new javax.swing.GroupLayout(Player2404);
        Player2404.setLayout(Player2404Layout);
        Player2404Layout.setHorizontalGroup(
                Player2404Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2404Layout.setVerticalGroup(
                Player2404Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2405.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2405Layout = new javax.swing.GroupLayout(Player2405);
        Player2405.setLayout(Player2405Layout);
        Player2405Layout.setHorizontalGroup(
                Player2405Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2405Layout.setVerticalGroup(
                Player2405Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2406.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2406Layout = new javax.swing.GroupLayout(Player2406);
        Player2406.setLayout(Player2406Layout);
        Player2406Layout.setHorizontalGroup(
                Player2406Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2406Layout.setVerticalGroup(
                Player2406Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        NamePanel_24.setBackground(new java.awt.Color(255, 0, 0));
        NamePanel_24.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_24.setPreferredSize(new java.awt.Dimension(90, 40));
        NamePanel_24.setRequestFocusEnabled(false);

        TileName_24.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_24.setText("<HTML>ILLINOIS<BR>AVENUE</HTML>");
        TileName_24.setPreferredSize(new java.awt.Dimension(90, 40));

        javax.swing.GroupLayout NamePanel_24Layout = new javax.swing.GroupLayout(NamePanel_24);
        NamePanel_24.setLayout(NamePanel_24Layout);
        NamePanel_24Layout.setHorizontalGroup(
                NamePanel_24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, NamePanel_24Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(TileName_24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(28, 28, 28))
        );
        NamePanel_24Layout.setVerticalGroup(
                NamePanel_24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_24Layout.createSequentialGroup()
                                .addComponent(TileName_24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout Tile_24Layout = new javax.swing.GroupLayout(Tile_24);
        Tile_24.setLayout(Tile_24Layout);
        Tile_24Layout.setHorizontalGroup(
                Tile_24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_24Layout.createSequentialGroup()
                                .addGroup(Tile_24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(NamePanel_24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(Tile_24Layout.createSequentialGroup()
                                                .addGroup(Tile_24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(Player2401, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2403, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2402, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(Tile_24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(Player2404, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2406, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2405, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(0, 0, 0))
        );
        Tile_24Layout.setVerticalGroup(
                Tile_24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_24Layout.createSequentialGroup()
                                .addGroup(Tile_24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player2401, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player2406, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(Tile_24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_24Layout.createSequentialGroup()
                                                .addComponent(Player2405, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player2404, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_24Layout.createSequentialGroup()
                                                .addComponent(Player2402, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player2403, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                                .addComponent(NamePanel_24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        Tile_25.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_25.setPreferredSize(new java.awt.Dimension(90, 125));

        Player2501.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2501Layout = new javax.swing.GroupLayout(Player2501);
        Player2501.setLayout(Player2501Layout);
        Player2501Layout.setHorizontalGroup(
                Player2501Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2501Layout.setVerticalGroup(
                Player2501Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2502.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2502Layout = new javax.swing.GroupLayout(Player2502);
        Player2502.setLayout(Player2502Layout);
        Player2502Layout.setHorizontalGroup(
                Player2502Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2502Layout.setVerticalGroup(
                Player2502Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2503.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2503Layout = new javax.swing.GroupLayout(Player2503);
        Player2503.setLayout(Player2503Layout);
        Player2503Layout.setHorizontalGroup(
                Player2503Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2503Layout.setVerticalGroup(
                Player2503Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2504.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2504Layout = new javax.swing.GroupLayout(Player2504);
        Player2504.setLayout(Player2504Layout);
        Player2504Layout.setHorizontalGroup(
                Player2504Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2504Layout.setVerticalGroup(
                Player2504Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2505.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2505Layout = new javax.swing.GroupLayout(Player2505);
        Player2505.setLayout(Player2505Layout);
        Player2505Layout.setHorizontalGroup(
                Player2505Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2505Layout.setVerticalGroup(
                Player2505Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2506.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2506Layout = new javax.swing.GroupLayout(Player2506);
        Player2506.setLayout(Player2506Layout);
        Player2506Layout.setHorizontalGroup(
                Player2506Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2506Layout.setVerticalGroup(
                Player2506Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        NamePanel_25.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_25.setPreferredSize(new java.awt.Dimension(90, 40));
        NamePanel_25.setRequestFocusEnabled(false);

        TileName_25.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_25.setText("<HTML>B.& O.<BR>RAILROAD</HTML>");
        TileName_25.setPreferredSize(new java.awt.Dimension(90, 40));

        javax.swing.GroupLayout NamePanel_25Layout = new javax.swing.GroupLayout(NamePanel_25);
        NamePanel_25.setLayout(NamePanel_25Layout);
        NamePanel_25Layout.setHorizontalGroup(
                NamePanel_25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, NamePanel_25Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(TileName_25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(28, 28, 28))
        );
        NamePanel_25Layout.setVerticalGroup(
                NamePanel_25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_25Layout.createSequentialGroup()
                                .addComponent(TileName_25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout Tile_25Layout = new javax.swing.GroupLayout(Tile_25);
        Tile_25.setLayout(Tile_25Layout);
        Tile_25Layout.setHorizontalGroup(
                Tile_25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_25Layout.createSequentialGroup()
                                .addGroup(Tile_25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(NamePanel_25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(Tile_25Layout.createSequentialGroup()
                                                .addGroup(Tile_25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(Player2501, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2503, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2502, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(Tile_25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(Player2504, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2506, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2505, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(0, 0, 0))
        );
        Tile_25Layout.setVerticalGroup(
                Tile_25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_25Layout.createSequentialGroup()
                                .addGroup(Tile_25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player2501, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player2506, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(Tile_25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_25Layout.createSequentialGroup()
                                                .addComponent(Player2505, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player2504, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_25Layout.createSequentialGroup()
                                                .addComponent(Player2502, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player2503, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                                .addComponent(NamePanel_25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        Tile_26.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_26.setPreferredSize(new java.awt.Dimension(90, 125));

        Player2601.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2601Layout = new javax.swing.GroupLayout(Player2601);
        Player2601.setLayout(Player2601Layout);
        Player2601Layout.setHorizontalGroup(
                Player2601Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2601Layout.setVerticalGroup(
                Player2601Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2602.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2602Layout = new javax.swing.GroupLayout(Player2602);
        Player2602.setLayout(Player2602Layout);
        Player2602Layout.setHorizontalGroup(
                Player2602Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2602Layout.setVerticalGroup(
                Player2602Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2603.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2603Layout = new javax.swing.GroupLayout(Player2603);
        Player2603.setLayout(Player2603Layout);
        Player2603Layout.setHorizontalGroup(
                Player2603Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2603Layout.setVerticalGroup(
                Player2603Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2604.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2604Layout = new javax.swing.GroupLayout(Player2604);
        Player2604.setLayout(Player2604Layout);
        Player2604Layout.setHorizontalGroup(
                Player2604Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2604Layout.setVerticalGroup(
                Player2604Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2605.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2605Layout = new javax.swing.GroupLayout(Player2605);
        Player2605.setLayout(Player2605Layout);
        Player2605Layout.setHorizontalGroup(
                Player2605Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2605Layout.setVerticalGroup(
                Player2605Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2606.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2606Layout = new javax.swing.GroupLayout(Player2606);
        Player2606.setLayout(Player2606Layout);
        Player2606Layout.setHorizontalGroup(
                Player2606Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2606Layout.setVerticalGroup(
                Player2606Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        NamePanel_26.setBackground(new java.awt.Color(255, 255, 0));
        NamePanel_26.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_26.setPreferredSize(new java.awt.Dimension(90, 40));
        NamePanel_26.setRequestFocusEnabled(false);

        TileName_26.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_26.setText("<HTML>ATLANTIC<BR>AVENUE</HTML>");
        TileName_26.setPreferredSize(new java.awt.Dimension(90, 40));

        javax.swing.GroupLayout NamePanel_26Layout = new javax.swing.GroupLayout(NamePanel_26);
        NamePanel_26.setLayout(NamePanel_26Layout);
        NamePanel_26Layout.setHorizontalGroup(
                NamePanel_26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, NamePanel_26Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(TileName_26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(28, 28, 28))
        );
        NamePanel_26Layout.setVerticalGroup(
                NamePanel_26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_26Layout.createSequentialGroup()
                                .addComponent(TileName_26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout Tile_26Layout = new javax.swing.GroupLayout(Tile_26);
        Tile_26.setLayout(Tile_26Layout);
        Tile_26Layout.setHorizontalGroup(
                Tile_26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_26Layout.createSequentialGroup()
                                .addGroup(Tile_26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(NamePanel_26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(Tile_26Layout.createSequentialGroup()
                                                .addGroup(Tile_26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(Player2601, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2603, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2602, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(Tile_26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(Player2604, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2606, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2605, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(0, 0, 0))
        );
        Tile_26Layout.setVerticalGroup(
                Tile_26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_26Layout.createSequentialGroup()
                                .addGroup(Tile_26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player2601, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player2606, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(Tile_26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_26Layout.createSequentialGroup()
                                                .addComponent(Player2605, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player2604, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_26Layout.createSequentialGroup()
                                                .addComponent(Player2602, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player2603, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                                .addComponent(NamePanel_26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        Tile_27.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_27.setPreferredSize(new java.awt.Dimension(90, 125));

        Player2701.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2701Layout = new javax.swing.GroupLayout(Player2701);
        Player2701.setLayout(Player2701Layout);
        Player2701Layout.setHorizontalGroup(
                Player2701Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2701Layout.setVerticalGroup(
                Player2701Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2702.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2702Layout = new javax.swing.GroupLayout(Player2702);
        Player2702.setLayout(Player2702Layout);
        Player2702Layout.setHorizontalGroup(
                Player2702Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2702Layout.setVerticalGroup(
                Player2702Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2703.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2703Layout = new javax.swing.GroupLayout(Player2703);
        Player2703.setLayout(Player2703Layout);
        Player2703Layout.setHorizontalGroup(
                Player2703Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2703Layout.setVerticalGroup(
                Player2703Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2704.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2704Layout = new javax.swing.GroupLayout(Player2704);
        Player2704.setLayout(Player2704Layout);
        Player2704Layout.setHorizontalGroup(
                Player2704Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2704Layout.setVerticalGroup(
                Player2704Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2705.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2705Layout = new javax.swing.GroupLayout(Player2705);
        Player2705.setLayout(Player2705Layout);
        Player2705Layout.setHorizontalGroup(
                Player2705Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2705Layout.setVerticalGroup(
                Player2705Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2706.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2706Layout = new javax.swing.GroupLayout(Player2706);
        Player2706.setLayout(Player2706Layout);
        Player2706Layout.setHorizontalGroup(
                Player2706Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2706Layout.setVerticalGroup(
                Player2706Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        NamePanel_27.setBackground(new java.awt.Color(255, 255, 0));
        NamePanel_27.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_27.setPreferredSize(new java.awt.Dimension(90, 40));
        NamePanel_27.setRequestFocusEnabled(false);

        TileName_27.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_27.setText("<HTML>VENTNOR<BR>AVENUE</HTML>");
        TileName_27.setPreferredSize(new java.awt.Dimension(90, 40));

        javax.swing.GroupLayout NamePanel_27Layout = new javax.swing.GroupLayout(NamePanel_27);
        NamePanel_27.setLayout(NamePanel_27Layout);
        NamePanel_27Layout.setHorizontalGroup(
                NamePanel_27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, NamePanel_27Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(TileName_27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(28, 28, 28))
        );
        NamePanel_27Layout.setVerticalGroup(
                NamePanel_27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_27Layout.createSequentialGroup()
                                .addComponent(TileName_27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout Tile_27Layout = new javax.swing.GroupLayout(Tile_27);
        Tile_27.setLayout(Tile_27Layout);
        Tile_27Layout.setHorizontalGroup(
                Tile_27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_27Layout.createSequentialGroup()
                                .addGroup(Tile_27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(NamePanel_27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(Tile_27Layout.createSequentialGroup()
                                                .addGroup(Tile_27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(Player2701, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2703, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2702, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(Tile_27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(Player2704, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2706, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2705, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(0, 0, 0))
        );
        Tile_27Layout.setVerticalGroup(
                Tile_27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_27Layout.createSequentialGroup()
                                .addGroup(Tile_27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player2701, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player2706, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(Tile_27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_27Layout.createSequentialGroup()
                                                .addComponent(Player2705, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player2704, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_27Layout.createSequentialGroup()
                                                .addComponent(Player2702, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player2703, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                                .addComponent(NamePanel_27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        Tile_28.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_28.setPreferredSize(new java.awt.Dimension(90, 125));

        Player2801.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2801Layout = new javax.swing.GroupLayout(Player2801);
        Player2801.setLayout(Player2801Layout);
        Player2801Layout.setHorizontalGroup(
                Player2801Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2801Layout.setVerticalGroup(
                Player2801Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2802.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2802Layout = new javax.swing.GroupLayout(Player2802);
        Player2802.setLayout(Player2802Layout);
        Player2802Layout.setHorizontalGroup(
                Player2802Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2802Layout.setVerticalGroup(
                Player2802Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2803.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2803Layout = new javax.swing.GroupLayout(Player2803);
        Player2803.setLayout(Player2803Layout);
        Player2803Layout.setHorizontalGroup(
                Player2803Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2803Layout.setVerticalGroup(
                Player2803Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2804.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2804Layout = new javax.swing.GroupLayout(Player2804);
        Player2804.setLayout(Player2804Layout);
        Player2804Layout.setHorizontalGroup(
                Player2804Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2804Layout.setVerticalGroup(
                Player2804Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2805.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2805Layout = new javax.swing.GroupLayout(Player2805);
        Player2805.setLayout(Player2805Layout);
        Player2805Layout.setHorizontalGroup(
                Player2805Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2805Layout.setVerticalGroup(
                Player2805Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2806.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2806Layout = new javax.swing.GroupLayout(Player2806);
        Player2806.setLayout(Player2806Layout);
        Player2806Layout.setHorizontalGroup(
                Player2806Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2806Layout.setVerticalGroup(
                Player2806Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        NamePanel_28.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_28.setPreferredSize(new java.awt.Dimension(90, 40));
        NamePanel_28.setRequestFocusEnabled(false);

        TileName_28.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_28.setText("<HTML>WATER<BR>WORKS</HTML>");
        TileName_28.setPreferredSize(new java.awt.Dimension(90, 40));

        javax.swing.GroupLayout NamePanel_28Layout = new javax.swing.GroupLayout(NamePanel_28);
        NamePanel_28.setLayout(NamePanel_28Layout);
        NamePanel_28Layout.setHorizontalGroup(
                NamePanel_28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, NamePanel_28Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(TileName_28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(28, 28, 28))
        );
        NamePanel_28Layout.setVerticalGroup(
                NamePanel_28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_28Layout.createSequentialGroup()
                                .addComponent(TileName_28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout Tile_28Layout = new javax.swing.GroupLayout(Tile_28);
        Tile_28.setLayout(Tile_28Layout);
        Tile_28Layout.setHorizontalGroup(
                Tile_28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_28Layout.createSequentialGroup()
                                .addGroup(Tile_28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(NamePanel_28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(Tile_28Layout.createSequentialGroup()
                                                .addGroup(Tile_28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(Player2801, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2803, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2802, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(Tile_28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(Player2804, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2806, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2805, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(0, 0, 0))
        );
        Tile_28Layout.setVerticalGroup(
                Tile_28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_28Layout.createSequentialGroup()
                                .addGroup(Tile_28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player2801, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player2806, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(Tile_28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_28Layout.createSequentialGroup()
                                                .addComponent(Player2805, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player2804, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_28Layout.createSequentialGroup()
                                                .addComponent(Player2802, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player2803, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                                .addComponent(NamePanel_28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        Tile_29.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_29.setPreferredSize(new java.awt.Dimension(90, 125));

        Player2901.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2901Layout = new javax.swing.GroupLayout(Player2901);
        Player2901.setLayout(Player2901Layout);
        Player2901Layout.setHorizontalGroup(
                Player2901Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2901Layout.setVerticalGroup(
                Player2901Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2902.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2902Layout = new javax.swing.GroupLayout(Player2902);
        Player2902.setLayout(Player2902Layout);
        Player2902Layout.setHorizontalGroup(
                Player2902Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2902Layout.setVerticalGroup(
                Player2902Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2903.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2903Layout = new javax.swing.GroupLayout(Player2903);
        Player2903.setLayout(Player2903Layout);
        Player2903Layout.setHorizontalGroup(
                Player2903Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2903Layout.setVerticalGroup(
                Player2903Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2904.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2904Layout = new javax.swing.GroupLayout(Player2904);
        Player2904.setLayout(Player2904Layout);
        Player2904Layout.setHorizontalGroup(
                Player2904Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2904Layout.setVerticalGroup(
                Player2904Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2905.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2905Layout = new javax.swing.GroupLayout(Player2905);
        Player2905.setLayout(Player2905Layout);
        Player2905Layout.setHorizontalGroup(
                Player2905Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2905Layout.setVerticalGroup(
                Player2905Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player2906.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player2906Layout = new javax.swing.GroupLayout(Player2906);
        Player2906.setLayout(Player2906Layout);
        Player2906Layout.setHorizontalGroup(
                Player2906Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player2906Layout.setVerticalGroup(
                Player2906Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        NamePanel_29.setBackground(new java.awt.Color(255, 255, 0));
        NamePanel_29.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_29.setPreferredSize(new java.awt.Dimension(90, 40));
        NamePanel_29.setRequestFocusEnabled(false);

        TileName_29.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_29.setText("<HTML>MARVIN<BR>GARDENS</HTML>");
        TileName_29.setPreferredSize(new java.awt.Dimension(90, 40));

        javax.swing.GroupLayout NamePanel_29Layout = new javax.swing.GroupLayout(NamePanel_29);
        NamePanel_29.setLayout(NamePanel_29Layout);
        NamePanel_29Layout.setHorizontalGroup(
                NamePanel_29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, NamePanel_29Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(TileName_29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(28, 28, 28))
        );
        NamePanel_29Layout.setVerticalGroup(
                NamePanel_29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(NamePanel_29Layout.createSequentialGroup()
                                .addComponent(TileName_29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout Tile_29Layout = new javax.swing.GroupLayout(Tile_29);
        Tile_29.setLayout(Tile_29Layout);
        Tile_29Layout.setHorizontalGroup(
                Tile_29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_29Layout.createSequentialGroup()
                                .addGroup(Tile_29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(NamePanel_29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(Tile_29Layout.createSequentialGroup()
                                                .addGroup(Tile_29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(Player2901, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2903, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2902, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(Tile_29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(Player2904, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2906, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Player2905, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(0, 0, 0))
        );
        Tile_29Layout.setVerticalGroup(
                Tile_29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_29Layout.createSequentialGroup()
                                .addGroup(Tile_29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player2901, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player2906, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(Tile_29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_29Layout.createSequentialGroup()
                                                .addComponent(Player2905, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player2904, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_29Layout.createSequentialGroup()
                                                .addComponent(Player2902, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player2903, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                                .addComponent(NamePanel_29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        Tile_30.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_30.setPreferredSize(new java.awt.Dimension(125, 125));

        Player3001.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player3001Layout = new javax.swing.GroupLayout(Player3001);
        Player3001.setLayout(Player3001Layout);
        Player3001Layout.setHorizontalGroup(
                Player3001Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player3001Layout.setVerticalGroup(
                Player3001Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player3002.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player3002Layout = new javax.swing.GroupLayout(Player3002);
        Player3002.setLayout(Player3002Layout);
        Player3002Layout.setHorizontalGroup(
                Player3002Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player3002Layout.setVerticalGroup(
                Player3002Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player3003.setPreferredSize(new java.awt.Dimension(45, 25));

        javax.swing.GroupLayout Player3003Layout = new javax.swing.GroupLayout(Player3003);
        Player3003.setLayout(Player3003Layout);
        Player3003Layout.setHorizontalGroup(
                Player3003Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );
        Player3003Layout.setVerticalGroup(
                Player3003Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );

        Player3004.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3004Layout = new javax.swing.GroupLayout(Player3004);
        Player3004.setLayout(Player3004Layout);
        Player3004Layout.setHorizontalGroup(
                Player3004Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3004Layout.setVerticalGroup(
                Player3004Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3005.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3005Layout = new javax.swing.GroupLayout(Player3005);
        Player3005.setLayout(Player3005Layout);
        Player3005Layout.setHorizontalGroup(
                Player3005Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3005Layout.setVerticalGroup(
                Player3005Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3006.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3006Layout = new javax.swing.GroupLayout(Player3006);
        Player3006.setLayout(Player3006Layout);
        Player3006Layout.setHorizontalGroup(
                Player3006Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3006Layout.setVerticalGroup(
                Player3006Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout Tile_30Layout = new javax.swing.GroupLayout(Tile_30);
        Tile_30.setLayout(Tile_30Layout);
        Tile_30Layout.setHorizontalGroup(
                Tile_30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_30Layout.createSequentialGroup()
                                .addGroup(Tile_30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, Tile_30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(Player3002, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(Player3003, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(Player3001, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addComponent(Player3004, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(Player3005, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(Player3006, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0))
        );
        Tile_30Layout.setVerticalGroup(
                Tile_30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_30Layout.createSequentialGroup()
                                .addGroup(Tile_30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(Tile_30Layout.createSequentialGroup()
                                                .addComponent(Player3001, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3002, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3003, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 48, Short.MAX_VALUE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Tile_30Layout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addGroup(Tile_30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(Tile_30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addComponent(Player3005, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(Player3006, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addComponent(Player3004, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(0, 0, 0))
        );

        Tile_31.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_31.setPreferredSize(new java.awt.Dimension(125, 90));

        Player3101.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3101Layout = new javax.swing.GroupLayout(Player3101);
        Player3101.setLayout(Player3101Layout);
        Player3101Layout.setHorizontalGroup(
                Player3101Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3101Layout.setVerticalGroup(
                Player3101Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3102.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3102Layout = new javax.swing.GroupLayout(Player3102);
        Player3102.setLayout(Player3102Layout);
        Player3102Layout.setHorizontalGroup(
                Player3102Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3102Layout.setVerticalGroup(
                Player3102Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3103.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3103Layout = new javax.swing.GroupLayout(Player3103);
        Player3103.setLayout(Player3103Layout);
        Player3103Layout.setHorizontalGroup(
                Player3103Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3103Layout.setVerticalGroup(
                Player3103Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3104.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3104Layout = new javax.swing.GroupLayout(Player3104);
        Player3104.setLayout(Player3104Layout);
        Player3104Layout.setHorizontalGroup(
                Player3104Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3104Layout.setVerticalGroup(
                Player3104Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3105.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3105Layout = new javax.swing.GroupLayout(Player3105);
        Player3105.setLayout(Player3105Layout);
        Player3105Layout.setHorizontalGroup(
                Player3105Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3105Layout.setVerticalGroup(
                Player3105Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3106.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3106Layout = new javax.swing.GroupLayout(Player3106);
        Player3106.setLayout(Player3106Layout);
        Player3106Layout.setHorizontalGroup(
                Player3106Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3106Layout.setVerticalGroup(
                Player3106Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        NamePanel_31.setBackground(new java.awt.Color(0, 255, 0));
        NamePanel_31.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_31.setPreferredSize(new java.awt.Dimension(40, 90));
        NamePanel_31.setRequestFocusEnabled(false);

        TileName_31.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_31.setText("<HTML>PACIFIC<BR>AVENUE</HTML>");
        TileName_31.setMaximumSize(new java.awt.Dimension(50000, 50000));
        TileName_31.setPreferredSize(new java.awt.Dimension(40, 90));

        javax.swing.GroupLayout NamePanel_31Layout = new javax.swing.GroupLayout(NamePanel_31);
        NamePanel_31.setLayout(NamePanel_31Layout);
        NamePanel_31Layout.setHorizontalGroup(
                NamePanel_31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, NamePanel_31Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(TileName_31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        NamePanel_31Layout.setVerticalGroup(
                NamePanel_31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(TileName_31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout Tile_31Layout = new javax.swing.GroupLayout(Tile_31);
        Tile_31.setLayout(Tile_31Layout);
        Tile_31Layout.setHorizontalGroup(
                Tile_31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Tile_31Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(NamePanel_31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(Tile_31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_31Layout.createSequentialGroup()
                                                .addComponent(Player3103, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3102, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3101, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_31Layout.createSequentialGroup()
                                                .addComponent(Player3104, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3105, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3106, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        Tile_31Layout.setVerticalGroup(
                Tile_31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_31Layout.createSequentialGroup()
                                .addGroup(Tile_31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player3103, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3102, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3101, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addGroup(Tile_31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player3104, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3105, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3106, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(NamePanel_31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        Tile_32.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_32.setPreferredSize(new java.awt.Dimension(125, 90));

        Player3201.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3201Layout = new javax.swing.GroupLayout(Player3201);
        Player3201.setLayout(Player3201Layout);
        Player3201Layout.setHorizontalGroup(
                Player3201Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3201Layout.setVerticalGroup(
                Player3201Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3202.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3202Layout = new javax.swing.GroupLayout(Player3202);
        Player3202.setLayout(Player3202Layout);
        Player3202Layout.setHorizontalGroup(
                Player3202Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3202Layout.setVerticalGroup(
                Player3202Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3203.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3203Layout = new javax.swing.GroupLayout(Player3203);
        Player3203.setLayout(Player3203Layout);
        Player3203Layout.setHorizontalGroup(
                Player3203Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3203Layout.setVerticalGroup(
                Player3203Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3204.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3204Layout = new javax.swing.GroupLayout(Player3204);
        Player3204.setLayout(Player3204Layout);
        Player3204Layout.setHorizontalGroup(
                Player3204Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3204Layout.setVerticalGroup(
                Player3204Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3205.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3205Layout = new javax.swing.GroupLayout(Player3205);
        Player3205.setLayout(Player3205Layout);
        Player3205Layout.setHorizontalGroup(
                Player3205Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3205Layout.setVerticalGroup(
                Player3205Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3206.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3206Layout = new javax.swing.GroupLayout(Player3206);
        Player3206.setLayout(Player3206Layout);
        Player3206Layout.setHorizontalGroup(
                Player3206Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3206Layout.setVerticalGroup(
                Player3206Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        NamePanel_32.setBackground(new java.awt.Color(0, 255, 0));
        NamePanel_32.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_32.setPreferredSize(new java.awt.Dimension(40, 90));
        NamePanel_32.setRequestFocusEnabled(false);

        TileName_32.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_32.setText("<HTML>NORTH<BR>CAROLINA<BR>AVENUE</HTML>");
        TileName_32.setMaximumSize(new java.awt.Dimension(50000, 50000));
        TileName_32.setPreferredSize(new java.awt.Dimension(40, 90));

        javax.swing.GroupLayout NamePanel_32Layout = new javax.swing.GroupLayout(NamePanel_32);
        NamePanel_32.setLayout(NamePanel_32Layout);
        NamePanel_32Layout.setHorizontalGroup(
                NamePanel_32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, NamePanel_32Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(TileName_32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        NamePanel_32Layout.setVerticalGroup(
                NamePanel_32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(TileName_32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout Tile_32Layout = new javax.swing.GroupLayout(Tile_32);
        Tile_32.setLayout(Tile_32Layout);
        Tile_32Layout.setHorizontalGroup(
                Tile_32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Tile_32Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(NamePanel_32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(Tile_32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_32Layout.createSequentialGroup()
                                                .addComponent(Player3203, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3202, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3201, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_32Layout.createSequentialGroup()
                                                .addComponent(Player3204, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3205, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3206, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        Tile_32Layout.setVerticalGroup(
                Tile_32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_32Layout.createSequentialGroup()
                                .addGroup(Tile_32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player3203, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3202, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3201, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addGroup(Tile_32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player3204, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3205, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3206, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(NamePanel_32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        Tile_33.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_33.setPreferredSize(new java.awt.Dimension(125, 90));

        Player3301.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3301Layout = new javax.swing.GroupLayout(Player3301);
        Player3301.setLayout(Player3301Layout);
        Player3301Layout.setHorizontalGroup(
                Player3301Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3301Layout.setVerticalGroup(
                Player3301Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3302.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3302Layout = new javax.swing.GroupLayout(Player3302);
        Player3302.setLayout(Player3302Layout);
        Player3302Layout.setHorizontalGroup(
                Player3302Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3302Layout.setVerticalGroup(
                Player3302Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3303.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3303Layout = new javax.swing.GroupLayout(Player3303);
        Player3303.setLayout(Player3303Layout);
        Player3303Layout.setHorizontalGroup(
                Player3303Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3303Layout.setVerticalGroup(
                Player3303Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3304.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3304Layout = new javax.swing.GroupLayout(Player3304);
        Player3304.setLayout(Player3304Layout);
        Player3304Layout.setHorizontalGroup(
                Player3304Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3304Layout.setVerticalGroup(
                Player3304Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3305.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3305Layout = new javax.swing.GroupLayout(Player3305);
        Player3305.setLayout(Player3305Layout);
        Player3305Layout.setHorizontalGroup(
                Player3305Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3305Layout.setVerticalGroup(
                Player3305Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3306.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3306Layout = new javax.swing.GroupLayout(Player3306);
        Player3306.setLayout(Player3306Layout);
        Player3306Layout.setHorizontalGroup(
                Player3306Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3306Layout.setVerticalGroup(
                Player3306Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        NamePanel_33.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_33.setPreferredSize(new java.awt.Dimension(40, 90));
        NamePanel_33.setRequestFocusEnabled(false);

        TileName_33.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_33.setText("<HTML>COMMUNITY<BR>CHEST</HTML>");
        TileName_33.setMaximumSize(new java.awt.Dimension(50000, 50000));
        TileName_33.setPreferredSize(new java.awt.Dimension(40, 90));

        javax.swing.GroupLayout NamePanel_33Layout = new javax.swing.GroupLayout(NamePanel_33);
        NamePanel_33.setLayout(NamePanel_33Layout);
        NamePanel_33Layout.setHorizontalGroup(
                NamePanel_33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, NamePanel_33Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(TileName_33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        NamePanel_33Layout.setVerticalGroup(
                NamePanel_33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(TileName_33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout Tile_33Layout = new javax.swing.GroupLayout(Tile_33);
        Tile_33.setLayout(Tile_33Layout);
        Tile_33Layout.setHorizontalGroup(
                Tile_33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Tile_33Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(NamePanel_33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(Tile_33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_33Layout.createSequentialGroup()
                                                .addComponent(Player3303, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3302, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3301, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_33Layout.createSequentialGroup()
                                                .addComponent(Player3304, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3305, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3306, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        Tile_33Layout.setVerticalGroup(
                Tile_33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_33Layout.createSequentialGroup()
                                .addGroup(Tile_33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player3303, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3302, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3301, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addGroup(Tile_33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player3304, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3305, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3306, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(NamePanel_33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        Tile_34.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_34.setPreferredSize(new java.awt.Dimension(125, 90));

        Player3401.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3401Layout = new javax.swing.GroupLayout(Player3401);
        Player3401.setLayout(Player3401Layout);
        Player3401Layout.setHorizontalGroup(
                Player3401Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3401Layout.setVerticalGroup(
                Player3401Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3402.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3402Layout = new javax.swing.GroupLayout(Player3402);
        Player3402.setLayout(Player3402Layout);
        Player3402Layout.setHorizontalGroup(
                Player3402Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3402Layout.setVerticalGroup(
                Player3402Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3403.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3403Layout = new javax.swing.GroupLayout(Player3403);
        Player3403.setLayout(Player3403Layout);
        Player3403Layout.setHorizontalGroup(
                Player3403Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3403Layout.setVerticalGroup(
                Player3403Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3404.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3404Layout = new javax.swing.GroupLayout(Player3404);
        Player3404.setLayout(Player3404Layout);
        Player3404Layout.setHorizontalGroup(
                Player3404Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3404Layout.setVerticalGroup(
                Player3404Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3405.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3405Layout = new javax.swing.GroupLayout(Player3405);
        Player3405.setLayout(Player3405Layout);
        Player3405Layout.setHorizontalGroup(
                Player3405Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3405Layout.setVerticalGroup(
                Player3405Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3406.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3406Layout = new javax.swing.GroupLayout(Player3406);
        Player3406.setLayout(Player3406Layout);
        Player3406Layout.setHorizontalGroup(
                Player3406Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3406Layout.setVerticalGroup(
                Player3406Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        NamePanel_34.setBackground(new java.awt.Color(0, 255, 0));
        NamePanel_34.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_34.setPreferredSize(new java.awt.Dimension(40, 90));
        NamePanel_34.setRequestFocusEnabled(false);

        TileName_34.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_34.setText("<HTML>PENNSYLVANIA<BR>AVENUE</HTML>");
        TileName_34.setMaximumSize(new java.awt.Dimension(50000, 50000));
        TileName_34.setPreferredSize(new java.awt.Dimension(40, 90));

        javax.swing.GroupLayout NamePanel_34Layout = new javax.swing.GroupLayout(NamePanel_34);
        NamePanel_34.setLayout(NamePanel_34Layout);
        NamePanel_34Layout.setHorizontalGroup(
                NamePanel_34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, NamePanel_34Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(TileName_34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        NamePanel_34Layout.setVerticalGroup(
                NamePanel_34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(TileName_34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout Tile_34Layout = new javax.swing.GroupLayout(Tile_34);
        Tile_34.setLayout(Tile_34Layout);
        Tile_34Layout.setHorizontalGroup(
                Tile_34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Tile_34Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(NamePanel_34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(Tile_34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_34Layout.createSequentialGroup()
                                                .addComponent(Player3403, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3402, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3401, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_34Layout.createSequentialGroup()
                                                .addComponent(Player3404, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3405, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3406, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        Tile_34Layout.setVerticalGroup(
                Tile_34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_34Layout.createSequentialGroup()
                                .addGroup(Tile_34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player3403, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3402, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3401, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addGroup(Tile_34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player3404, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3405, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3406, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(NamePanel_34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        Tile_35.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_35.setPreferredSize(new java.awt.Dimension(125, 90));

        Player3501.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3501Layout = new javax.swing.GroupLayout(Player3501);
        Player3501.setLayout(Player3501Layout);
        Player3501Layout.setHorizontalGroup(
                Player3501Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3501Layout.setVerticalGroup(
                Player3501Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3502.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3502Layout = new javax.swing.GroupLayout(Player3502);
        Player3502.setLayout(Player3502Layout);
        Player3502Layout.setHorizontalGroup(
                Player3502Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3502Layout.setVerticalGroup(
                Player3502Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3503.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3503Layout = new javax.swing.GroupLayout(Player3503);
        Player3503.setLayout(Player3503Layout);
        Player3503Layout.setHorizontalGroup(
                Player3503Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3503Layout.setVerticalGroup(
                Player3503Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3504.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3504Layout = new javax.swing.GroupLayout(Player3504);
        Player3504.setLayout(Player3504Layout);
        Player3504Layout.setHorizontalGroup(
                Player3504Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3504Layout.setVerticalGroup(
                Player3504Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3505.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3505Layout = new javax.swing.GroupLayout(Player3505);
        Player3505.setLayout(Player3505Layout);
        Player3505Layout.setHorizontalGroup(
                Player3505Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3505Layout.setVerticalGroup(
                Player3505Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3506.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3506Layout = new javax.swing.GroupLayout(Player3506);
        Player3506.setLayout(Player3506Layout);
        Player3506Layout.setHorizontalGroup(
                Player3506Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3506Layout.setVerticalGroup(
                Player3506Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        NamePanel_35.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_35.setPreferredSize(new java.awt.Dimension(40, 90));
        NamePanel_35.setRequestFocusEnabled(false);

        TileName_35.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_35.setText("<HTML>SHORT<BR>LINE</HTML>");
        TileName_35.setMaximumSize(new java.awt.Dimension(50000, 50000));
        TileName_35.setPreferredSize(new java.awt.Dimension(40, 90));

        javax.swing.GroupLayout NamePanel_35Layout = new javax.swing.GroupLayout(NamePanel_35);
        NamePanel_35.setLayout(NamePanel_35Layout);
        NamePanel_35Layout.setHorizontalGroup(
                NamePanel_35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, NamePanel_35Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(TileName_35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        NamePanel_35Layout.setVerticalGroup(
                NamePanel_35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(TileName_35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout Tile_35Layout = new javax.swing.GroupLayout(Tile_35);
        Tile_35.setLayout(Tile_35Layout);
        Tile_35Layout.setHorizontalGroup(
                Tile_35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Tile_35Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(NamePanel_35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(Tile_35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_35Layout.createSequentialGroup()
                                                .addComponent(Player3503, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3502, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3501, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_35Layout.createSequentialGroup()
                                                .addComponent(Player3504, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3505, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3506, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        Tile_35Layout.setVerticalGroup(
                Tile_35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_35Layout.createSequentialGroup()
                                .addGroup(Tile_35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player3503, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3502, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3501, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addGroup(Tile_35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player3504, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3505, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3506, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(NamePanel_35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        Tile_36.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_36.setPreferredSize(new java.awt.Dimension(125, 90));

        Player3601.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3601Layout = new javax.swing.GroupLayout(Player3601);
        Player3601.setLayout(Player3601Layout);
        Player3601Layout.setHorizontalGroup(
                Player3601Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3601Layout.setVerticalGroup(
                Player3601Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3602.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3602Layout = new javax.swing.GroupLayout(Player3602);
        Player3602.setLayout(Player3602Layout);
        Player3602Layout.setHorizontalGroup(
                Player3602Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3602Layout.setVerticalGroup(
                Player3602Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3603.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3603Layout = new javax.swing.GroupLayout(Player3603);
        Player3603.setLayout(Player3603Layout);
        Player3603Layout.setHorizontalGroup(
                Player3603Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3603Layout.setVerticalGroup(
                Player3603Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3604.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3604Layout = new javax.swing.GroupLayout(Player3604);
        Player3604.setLayout(Player3604Layout);
        Player3604Layout.setHorizontalGroup(
                Player3604Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3604Layout.setVerticalGroup(
                Player3604Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3605.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3605Layout = new javax.swing.GroupLayout(Player3605);
        Player3605.setLayout(Player3605Layout);
        Player3605Layout.setHorizontalGroup(
                Player3605Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3605Layout.setVerticalGroup(
                Player3605Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3606.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3606Layout = new javax.swing.GroupLayout(Player3606);
        Player3606.setLayout(Player3606Layout);
        Player3606Layout.setHorizontalGroup(
                Player3606Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3606Layout.setVerticalGroup(
                Player3606Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        NamePanel_36.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_36.setPreferredSize(new java.awt.Dimension(40, 90));
        NamePanel_36.setRequestFocusEnabled(false);

        TileName_36.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_36.setText("CHANCE");
        TileName_36.setMaximumSize(new java.awt.Dimension(50000, 50000));
        TileName_36.setPreferredSize(new java.awt.Dimension(40, 90));

        javax.swing.GroupLayout NamePanel_36Layout = new javax.swing.GroupLayout(NamePanel_36);
        NamePanel_36.setLayout(NamePanel_36Layout);
        NamePanel_36Layout.setHorizontalGroup(
                NamePanel_36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, NamePanel_36Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(TileName_36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        NamePanel_36Layout.setVerticalGroup(
                NamePanel_36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(TileName_36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout Tile_36Layout = new javax.swing.GroupLayout(Tile_36);
        Tile_36.setLayout(Tile_36Layout);
        Tile_36Layout.setHorizontalGroup(
                Tile_36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Tile_36Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(NamePanel_36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(Tile_36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_36Layout.createSequentialGroup()
                                                .addComponent(Player3603, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3602, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3601, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_36Layout.createSequentialGroup()
                                                .addComponent(Player3604, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3605, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3606, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        Tile_36Layout.setVerticalGroup(
                Tile_36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_36Layout.createSequentialGroup()
                                .addGroup(Tile_36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player3603, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3602, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3601, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addGroup(Tile_36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player3604, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3605, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3606, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(NamePanel_36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        Tile_37.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_37.setPreferredSize(new java.awt.Dimension(125, 90));

        Player3701.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3701Layout = new javax.swing.GroupLayout(Player3701);
        Player3701.setLayout(Player3701Layout);
        Player3701Layout.setHorizontalGroup(
                Player3701Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3701Layout.setVerticalGroup(
                Player3701Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3702.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3702Layout = new javax.swing.GroupLayout(Player3702);
        Player3702.setLayout(Player3702Layout);
        Player3702Layout.setHorizontalGroup(
                Player3702Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3702Layout.setVerticalGroup(
                Player3702Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3703.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3703Layout = new javax.swing.GroupLayout(Player3703);
        Player3703.setLayout(Player3703Layout);
        Player3703Layout.setHorizontalGroup(
                Player3703Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3703Layout.setVerticalGroup(
                Player3703Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3704.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3704Layout = new javax.swing.GroupLayout(Player3704);
        Player3704.setLayout(Player3704Layout);
        Player3704Layout.setHorizontalGroup(
                Player3704Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3704Layout.setVerticalGroup(
                Player3704Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3705.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3705Layout = new javax.swing.GroupLayout(Player3705);
        Player3705.setLayout(Player3705Layout);
        Player3705Layout.setHorizontalGroup(
                Player3705Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3705Layout.setVerticalGroup(
                Player3705Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3706.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3706Layout = new javax.swing.GroupLayout(Player3706);
        Player3706.setLayout(Player3706Layout);
        Player3706Layout.setHorizontalGroup(
                Player3706Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3706Layout.setVerticalGroup(
                Player3706Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        NamePanel_37.setBackground(new java.awt.Color(0, 0, 255));
        NamePanel_37.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_37.setPreferredSize(new java.awt.Dimension(40, 90));
        NamePanel_37.setRequestFocusEnabled(false);

        TileName_37.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_37.setText("<HTML>PARK<BR>PLACE</HTML>");
        TileName_37.setMaximumSize(new java.awt.Dimension(50000, 50000));
        TileName_37.setPreferredSize(new java.awt.Dimension(40, 90));

        javax.swing.GroupLayout NamePanel_37Layout = new javax.swing.GroupLayout(NamePanel_37);
        NamePanel_37.setLayout(NamePanel_37Layout);
        NamePanel_37Layout.setHorizontalGroup(
                NamePanel_37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, NamePanel_37Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(TileName_37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        NamePanel_37Layout.setVerticalGroup(
                NamePanel_37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(TileName_37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout Tile_37Layout = new javax.swing.GroupLayout(Tile_37);
        Tile_37.setLayout(Tile_37Layout);
        Tile_37Layout.setHorizontalGroup(
                Tile_37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Tile_37Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(NamePanel_37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(Tile_37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_37Layout.createSequentialGroup()
                                                .addComponent(Player3703, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3702, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3701, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_37Layout.createSequentialGroup()
                                                .addComponent(Player3704, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3705, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3706, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        Tile_37Layout.setVerticalGroup(
                Tile_37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_37Layout.createSequentialGroup()
                                .addGroup(Tile_37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player3703, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3702, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3701, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addGroup(Tile_37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player3704, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3705, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3706, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(NamePanel_37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        Tile_38.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_38.setPreferredSize(new java.awt.Dimension(125, 90));

        Player3801.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3801Layout = new javax.swing.GroupLayout(Player3801);
        Player3801.setLayout(Player3801Layout);
        Player3801Layout.setHorizontalGroup(
                Player3801Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3801Layout.setVerticalGroup(
                Player3801Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3802.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3802Layout = new javax.swing.GroupLayout(Player3802);
        Player3802.setLayout(Player3802Layout);
        Player3802Layout.setHorizontalGroup(
                Player3802Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3802Layout.setVerticalGroup(
                Player3802Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3803.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3803Layout = new javax.swing.GroupLayout(Player3803);
        Player3803.setLayout(Player3803Layout);
        Player3803Layout.setHorizontalGroup(
                Player3803Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3803Layout.setVerticalGroup(
                Player3803Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3804.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3804Layout = new javax.swing.GroupLayout(Player3804);
        Player3804.setLayout(Player3804Layout);
        Player3804Layout.setHorizontalGroup(
                Player3804Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3804Layout.setVerticalGroup(
                Player3804Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3805.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3805Layout = new javax.swing.GroupLayout(Player3805);
        Player3805.setLayout(Player3805Layout);
        Player3805Layout.setHorizontalGroup(
                Player3805Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3805Layout.setVerticalGroup(
                Player3805Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3806.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3806Layout = new javax.swing.GroupLayout(Player3806);
        Player3806.setLayout(Player3806Layout);
        Player3806Layout.setHorizontalGroup(
                Player3806Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3806Layout.setVerticalGroup(
                Player3806Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        NamePanel_38.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_38.setPreferredSize(new java.awt.Dimension(40, 90));
        NamePanel_38.setRequestFocusEnabled(false);

        TileName_38.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_38.setText("<HTML>LUXURY<BR>TAX</HTML>");
        TileName_38.setMaximumSize(new java.awt.Dimension(50000, 50000));
        TileName_38.setPreferredSize(new java.awt.Dimension(40, 90));

        javax.swing.GroupLayout NamePanel_38Layout = new javax.swing.GroupLayout(NamePanel_38);
        NamePanel_38.setLayout(NamePanel_38Layout);
        NamePanel_38Layout.setHorizontalGroup(
                NamePanel_38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, NamePanel_38Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(TileName_38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        NamePanel_38Layout.setVerticalGroup(
                NamePanel_38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(TileName_38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout Tile_38Layout = new javax.swing.GroupLayout(Tile_38);
        Tile_38.setLayout(Tile_38Layout);
        Tile_38Layout.setHorizontalGroup(
                Tile_38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Tile_38Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(NamePanel_38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(Tile_38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_38Layout.createSequentialGroup()
                                                .addComponent(Player3803, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3802, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3801, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_38Layout.createSequentialGroup()
                                                .addComponent(Player3804, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3805, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3806, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        Tile_38Layout.setVerticalGroup(
                Tile_38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_38Layout.createSequentialGroup()
                                .addGroup(Tile_38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player3803, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3802, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3801, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addGroup(Tile_38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player3804, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3805, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3806, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(NamePanel_38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        Tile_39.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Tile_39.setPreferredSize(new java.awt.Dimension(125, 90));

        Player3901.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3901Layout = new javax.swing.GroupLayout(Player3901);
        Player3901.setLayout(Player3901Layout);
        Player3901Layout.setHorizontalGroup(
                Player3901Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3901Layout.setVerticalGroup(
                Player3901Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3902.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3902Layout = new javax.swing.GroupLayout(Player3902);
        Player3902.setLayout(Player3902Layout);
        Player3902Layout.setHorizontalGroup(
                Player3902Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3902Layout.setVerticalGroup(
                Player3902Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3903.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3903Layout = new javax.swing.GroupLayout(Player3903);
        Player3903.setLayout(Player3903Layout);
        Player3903Layout.setHorizontalGroup(
                Player3903Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3903Layout.setVerticalGroup(
                Player3903Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3904.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3904Layout = new javax.swing.GroupLayout(Player3904);
        Player3904.setLayout(Player3904Layout);
        Player3904Layout.setHorizontalGroup(
                Player3904Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3904Layout.setVerticalGroup(
                Player3904Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3905.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3905Layout = new javax.swing.GroupLayout(Player3905);
        Player3905.setLayout(Player3905Layout);
        Player3905Layout.setHorizontalGroup(
                Player3905Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3905Layout.setVerticalGroup(
                Player3905Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        Player3906.setPreferredSize(new java.awt.Dimension(25, 45));

        javax.swing.GroupLayout Player3906Layout = new javax.swing.GroupLayout(Player3906);
        Player3906.setLayout(Player3906Layout);
        Player3906Layout.setHorizontalGroup(
                Player3906Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 25, Short.MAX_VALUE)
        );
        Player3906Layout.setVerticalGroup(
                Player3906Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 45, Short.MAX_VALUE)
        );

        NamePanel_39.setBackground(new java.awt.Color(0, 0, 255));
        NamePanel_39.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NamePanel_39.setPreferredSize(new java.awt.Dimension(40, 90));
        NamePanel_39.setRequestFocusEnabled(false);

        TileName_39.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TileName_39.setText("<HTML>BOARDWALK</HTML>");
        TileName_39.setMaximumSize(new java.awt.Dimension(50000, 50000));
        TileName_39.setPreferredSize(new java.awt.Dimension(40, 90));

        javax.swing.GroupLayout NamePanel_39Layout = new javax.swing.GroupLayout(NamePanel_39);
        NamePanel_39.setLayout(NamePanel_39Layout);
        NamePanel_39Layout.setHorizontalGroup(
                NamePanel_39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, NamePanel_39Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(TileName_39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        NamePanel_39Layout.setVerticalGroup(
                NamePanel_39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(TileName_39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout Tile_39Layout = new javax.swing.GroupLayout(Tile_39);
        Tile_39.setLayout(Tile_39Layout);
        Tile_39Layout.setHorizontalGroup(
                Tile_39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Tile_39Layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(NamePanel_39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(Tile_39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(Tile_39Layout.createSequentialGroup()
                                                .addComponent(Player3903, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3902, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3901, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Tile_39Layout.createSequentialGroup()
                                                .addComponent(Player3904, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3905, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Player3906, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        Tile_39Layout.setVerticalGroup(
                Tile_39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Tile_39Layout.createSequentialGroup()
                                .addGroup(Tile_39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player3903, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3902, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3901, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addGroup(Tile_39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player3904, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3905, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player3906, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(NamePanel_39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout MainPanelLayout = new javax.swing.GroupLayout(MainPanel);
        MainPanel.setLayout(MainPanelLayout);
        MainPanelLayout.setHorizontalGroup(
                MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(MainPanelLayout.createSequentialGroup()
                                .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Tile_18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(MainPanelLayout.createSequentialGroup()
                                                .addComponent(Tile_09, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_08, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_07, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_06, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_05, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_04, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_03, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_02, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_01, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(insied_area, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(MainPanelLayout.createSequentialGroup()
                                                .addComponent(Tile_21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, 0)
                                .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                .addComponent(Tile_31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(Tile_32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addComponent(Tile_33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Tile_34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Tile_35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                .addComponent(Tile_36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(Tile_37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addComponent(Tile_38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(Tile_39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addComponent(Tile_30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(Tile_00, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        MainPanelLayout.setVerticalGroup(
                MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(MainPanelLayout.createSequentialGroup()
                                .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Tile_20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(MainPanelLayout.createSequentialGroup()
                                                .addComponent(Tile_19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(MainPanelLayout.createSequentialGroup()
                                                .addComponent(Tile_31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(Tile_39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(insied_area, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Tile_01, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_02, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_03, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_04, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_05, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_06, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_07, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_08, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_09, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Tile_00, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jScrollPane2.setViewportView(MainPanel);

        SidePanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        SidePanel.setPreferredSize(new java.awt.Dimension(550, 720));

        Player1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Player1.setPreferredSize(new java.awt.Dimension(150, 202));

        Player1_Color.setBackground(new java.awt.Color(255, 0, 0));
        Player1_Color.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Player1_Color.setPreferredSize(new java.awt.Dimension(150, 100));
        ArrayList<String> playerNames = new ArrayList<String>();
        for(int i=1;i<=Monopoly_final.players.size();++i){
            playerNames.add("Player " + i);
        }
        String[] contestants = playerNames.toArray(new String[playerNames.size()]);
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(contestants));

        javax.swing.GroupLayout Player1_ColorLayout = new javax.swing.GroupLayout(Player1_Color);
        Player1_Color.setLayout(Player1_ColorLayout);
        Player1_ColorLayout.setHorizontalGroup(
                Player1_ColorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Player1_ColorLayout.createSequentialGroup()
                                .addGap(186, 186, 186)
                                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(222, Short.MAX_VALUE))
        );
        Player1_ColorLayout.setVerticalGroup(
                Player1_ColorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Player1_ColorLayout.createSequentialGroup()
                                .addGap(35, 35, 35)
                                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(43, Short.MAX_VALUE))
        );

        Player1_NameD.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Player1_NameD.setText("Name:");
        Player1_NameD.setPreferredSize(new java.awt.Dimension(75, 34));

        Player1_Name.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Player1_Name.setPreferredSize(new java.awt.Dimension(75, 34));

        Player1_Balance.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Player1_Balance.setPreferredSize(new java.awt.Dimension(75, 34));

        Player1_InJailD.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Player1_InJailD.setText("In Jail:");
        Player1_InJailD.setPreferredSize(new java.awt.Dimension(75, 34));

        Player1_InJail.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Player1_InJail.setPreferredSize(new java.awt.Dimension(75, 34));

        Player1_BalanceD.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Player1_BalanceD.setText("Balance:");
        Player1_BalanceD.setPreferredSize(new java.awt.Dimension(75, 34));

        javax.swing.GroupLayout Player1Layout = new javax.swing.GroupLayout(Player1);
        Player1.setLayout(Player1Layout);
        Player1Layout.setHorizontalGroup(
                Player1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(Player1_Color, javax.swing.GroupLayout.DEFAULT_SIZE, 466, Short.MAX_VALUE)
                        .addGroup(Player1Layout.createSequentialGroup()
                                .addGroup(Player1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player1_NameD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(Player1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(Player1_InJailD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(Player1_BalanceD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(Player1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player1_InJail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1_Name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1_Balance, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        Player1Layout.setVerticalGroup(
                Player1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Player1Layout.createSequentialGroup()
                                .addComponent(Player1_Color, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(Player1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(Player1_Name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Player1_NameD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(Player1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(Player1Layout.createSequentialGroup()
                                                .addComponent(Player1_Balance, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(Player1_InJail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(Player1Layout.createSequentialGroup()
                                                .addComponent(Player1_BalanceD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(Player1_InJailD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        
        jComboBox1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JComboBox cb = (JComboBox)e.getSource();
                String selectedPlayer = (String)cb.getSelectedItem();
                updateLabel(selectedPlayer);
            }
        });

        jButton1.setText("Throw Dice");

        jButton1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                throwDiceEvent(Monopoly_final.getPlayerById(Monopoly_final.currentPlayer));
            }
        });

        jButton2.setText("Draw card");
        jButton2.setEnabled(false);
        
        jButton2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                drawCardEvent(Monopoly_final.getPlayerById(Monopoly_final.currentPlayer));
            }
        });

        jButton3.setText("Buy Tile");
        
        jButton3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                buyTileEvent(Monopoly_final.getPlayerById(Monopoly_final.currentPlayer));
            }
        });

        jButton4.setText("Buy/Sell");
        
        jButton4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                BuySellFrame bsf = new BuySellFrame();
            }
        });

        jButton5.setText("Build");
        jButton5.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                buildEvent(Monopoly_final.getPlayerById(Monopoly_final.currentPlayer));
            }
        });

        
        
        jButton6.setText("End Turn");
        jButton6.setEnabled(false);
        

        jButton6.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                EndTurnEvent();
            }
        });
        
        
         
        jButton7.setText("<html><h4>Get Mortgage/<br>Buy back</h4></html>");
        jButton7.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MortgageFrame mf = new MortgageFrame();
            }
        });
        

        javax.swing.GroupLayout SidePanelLayout = new javax.swing.GroupLayout(SidePanel);
        SidePanel.setLayout(SidePanelLayout);
        SidePanelLayout.setHorizontalGroup(
                SidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(SidePanelLayout.createSequentialGroup()
                                .addGap(50, 50, 50)
                                .addGroup(SidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(Player1, javax.swing.GroupLayout.PREFERRED_SIZE, 468, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(SidePanelLayout.createSequentialGroup()
                                                .addComponent(jButton1)
                                                .addGap(18, 18, 18)
                                                .addComponent(jButton2)
                                                .addGap(18, 18, 18)
                                                .addGroup(SidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGap(18, 18, 18)
                                                        .addComponent(jButton7 , javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGap(18, 18, 18)
                                                        .addComponent(jButton6)
                                                        .addGroup(SidePanelLayout.createSequentialGroup()
                                                                .addComponent(jButton3)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(jButton4)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(jButton5)))))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        SidePanelLayout.setVerticalGroup(
                SidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(SidePanelLayout.createSequentialGroup()
                                .addGap(29, 29, 29)
                                .addComponent(Player1, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton7)
                                .addGap(18, 18, 18)
                                .addGroup(SidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButton1)
                                        .addComponent(jButton2)
                                        .addComponent(jButton3)
                                        .addComponent(jButton4)
                                        .addComponent(jButton5))
                                .addGap(18, 18, 18)
                                .addComponent(jButton6)
                                .addGap(43, 43, 43))
        );
       
        DownPanel.setPreferredSize(new java.awt.Dimension(1680, 300));

        jPanel1.setBackground(new java.awt.Color(102, 51, 0));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("MEDITERRAN AVENUE");
        jLabel1.setToolTipText("");
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel1.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("0, 0");
        jLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel2.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Bank");
        jLabel3.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel3.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel2.setBackground(new java.awt.Color(102, 51, 0));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel2.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("BALTIC AVENUE");
        jLabel4.setToolTipText("");
        jLabel4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel4.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("0, 0");
        jLabel5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel5.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Bank");
        jLabel6.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel6.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
                jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
                jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel3.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("READING RAILROAD");
        jLabel7.setToolTipText("");
        jLabel7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel7.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Owner");
        jLabel8.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel8.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Bank");
        jLabel9.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel9.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel9.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
                jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel3Layout.setVerticalGroup(
                jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel4.setBackground(new java.awt.Color(102, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel4.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("ORIENTAL AVENUE");
        jLabel10.setToolTipText("");
        jLabel10.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel10.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("0, 0");
        jLabel11.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel11.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("Bank");
        jLabel12.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel12.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel12.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
                jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel4Layout.setVerticalGroup(
                jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel5.setBackground(new java.awt.Color(102, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel5.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("VERMONT AVENUE");
        jLabel13.setToolTipText("");
        jLabel13.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel13.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel14.setText("0, 0");
        jLabel14.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel14.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel15.setText("Bank");
        jLabel15.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel15.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel15.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
                jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel5Layout.setVerticalGroup(
                jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel6.setBackground(new java.awt.Color(102, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel6.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel16.setText("<html>CONNECTICUT<br>AVENUE</html>");
        jLabel16.setToolTipText("");
        jLabel16.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel16.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel17.setText("0, 0");
        jLabel17.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel17.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel18.setText("Bank");
        jLabel18.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel18.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel18.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
                jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel6Layout.setVerticalGroup(
                jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel7.setBackground(new java.awt.Color(255, 51, 102));
        jPanel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel7.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel19.setText("ST. CHARLES PLACE");
        jLabel19.setToolTipText("");
        jLabel19.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel19.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel20.setText("0, 0");
        jLabel20.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel20.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("Bank");
        jLabel21.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel21.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel21.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
                jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel7Layout.createSequentialGroup()
                                .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel7Layout.setVerticalGroup(
                jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel7Layout.createSequentialGroup()
                                .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel8.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel22.setText("ELECTRIC COMPANY");
        jLabel22.setToolTipText("");
        jLabel22.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel22.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel23.setText("Owner");
        jLabel23.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel23.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel24.setText("Bank");
        jLabel24.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel24.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel24.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
                jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel8Layout.createSequentialGroup()
                                .addComponent(jLabel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel8Layout.setVerticalGroup(
                jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel8Layout.createSequentialGroup()
                                .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel9_ok.setBackground(new java.awt.Color(255, 51, 102));
        jPanel9_ok.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel9_ok.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel82.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel82.setText("STATES AVENUE");
        jLabel82.setToolTipText("");
        jLabel82.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel82.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel83.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel83.setText("0, 0");
        jLabel83.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel83.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel84.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel84.setText("Bank");
        jLabel84.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel84.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel84.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel9_okLayout = new javax.swing.GroupLayout(jPanel9_ok);
        jPanel9_ok.setLayout(jPanel9_okLayout);
        jPanel9_okLayout.setHorizontalGroup(
                jPanel9_okLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel82, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel9_okLayout.createSequentialGroup()
                                .addComponent(jLabel83, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel84, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel9_okLayout.setVerticalGroup(
                jPanel9_okLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel9_okLayout.createSequentialGroup()
                                .addComponent(jLabel82, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel9_okLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel83, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel84, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel10.setBackground(new java.awt.Color(255, 51, 102));
        jPanel10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel10.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel28.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel28.setText("VIRGINIA AVENUE");
        jLabel28.setToolTipText("");
        jLabel28.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel28.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel29.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel29.setText("0, 0");
        jLabel29.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel29.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel30.setText("Bank");
        jLabel30.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel30.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel30.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
                jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel10Layout.createSequentialGroup()
                                .addComponent(jLabel29, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel30, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel10Layout.setVerticalGroup(
                jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel10Layout.createSequentialGroup()
                                .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel30, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel11.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel31.setText("<HTML>PENNSYLVANIA<BR>RAILROAD<HTML>");
        jLabel31.setToolTipText("");
        jLabel31.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel31.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel32.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel32.setText("Owner");
        jLabel32.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel32.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel33.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel33.setText("Bank");
        jLabel33.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel33.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel33.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
                jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel11Layout.createSequentialGroup()
                                .addComponent(jLabel32, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel33, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel11Layout.setVerticalGroup(
                jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel11Layout.createSequentialGroup()
                                .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel33, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel12.setBackground(new java.awt.Color(255, 153, 0));
        jPanel12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel12.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel34.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel34.setText("ST. JAMES PLACE");
        jLabel34.setToolTipText("");
        jLabel34.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel34.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel35.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel35.setText("0, 0");
        jLabel35.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel35.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel36.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel36.setText("Bank");
        jLabel36.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel36.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel36.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
                jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel12Layout.createSequentialGroup()
                                .addComponent(jLabel35, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel36, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel12Layout.setVerticalGroup(
                jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel12Layout.createSequentialGroup()
                                .addComponent(jLabel34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel36, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel13.setBackground(new java.awt.Color(255, 153, 0));
        jPanel13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel13.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel37.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel37.setText("TENESSEE AVENUE");
        jLabel37.setToolTipText("");
        jLabel37.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel37.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel38.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel38.setText("0, 0");
        jLabel38.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel38.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel39.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel39.setText("Bank");
        jLabel39.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel39.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel39.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
                jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel13Layout.createSequentialGroup()
                                .addComponent(jLabel38, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel39, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel13Layout.setVerticalGroup(
                jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel13Layout.createSequentialGroup()
                                .addComponent(jLabel37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel39, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel14.setBackground(new java.awt.Color(255, 153, 0));
        jPanel14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel14.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel40.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel40.setText("<html>NEW YORK<br>AVENUE</html>");
        jLabel40.setToolTipText("");
        jLabel40.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel40.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel41.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel41.setText("0, 0");
        jLabel41.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel41.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel42.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel42.setText("Bank");
        jLabel42.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel42.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel42.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
                jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel40, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel14Layout.createSequentialGroup()
                                .addComponent(jLabel41, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel42, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel14Layout.setVerticalGroup(
                jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel14Layout.createSequentialGroup()
                                .addComponent(jLabel40, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel41, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel42, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel15.setBackground(new java.awt.Color(255, 0, 0));
        jPanel15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel15.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel43.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel43.setText("KENTUCKY AVENUE");
        jLabel43.setToolTipText("");
        jLabel43.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel43.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel44.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel44.setText("0, 0");
        jLabel44.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel44.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel45.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel45.setText("Bank");
        jLabel45.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel45.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel45.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
                jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel43, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel15Layout.createSequentialGroup()
                                .addComponent(jLabel44, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel45, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel15Layout.setVerticalGroup(
                jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel15Layout.createSequentialGroup()
                                .addComponent(jLabel43, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel44, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel45, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel16.setBackground(new java.awt.Color(255, 0, 0));
        jPanel16.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel16.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel46.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel46.setText("INDIANA AVENUE");
        jLabel46.setToolTipText("");
        jLabel46.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel46.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel47.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel47.setText("0, 0");
        jLabel47.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel47.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel48.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel48.setText("Bank");
        jLabel48.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel48.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel48.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
                jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel46, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel16Layout.createSequentialGroup()
                                .addComponent(jLabel47, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel48, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel16Layout.setVerticalGroup(
                jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel16Layout.createSequentialGroup()
                                .addComponent(jLabel46, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel47, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel48, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel17.setBackground(new java.awt.Color(255, 0, 0));
        jPanel17.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel17.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel49.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel49.setText("ILLINOIS AVENUE");
        jLabel49.setToolTipText("");
        jLabel49.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel49.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel50.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel50.setText("0, 0");
        jLabel50.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel50.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel51.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel51.setText("Bank");
        jLabel51.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel51.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel51.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
                jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel49, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel17Layout.createSequentialGroup()
                                .addComponent(jLabel50, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel51, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel17Layout.setVerticalGroup(
                jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel17Layout.createSequentialGroup()
                                .addComponent(jLabel49, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel50, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel51, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel18.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel18.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel52.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel52.setText("B.& O. RAILROAD");
        jLabel52.setToolTipText("");
        jLabel52.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel52.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel53.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel53.setText("Owner");
        jLabel53.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel53.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel54.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel54.setText("Bank");
        jLabel54.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel54.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel54.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
                jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel52, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel18Layout.createSequentialGroup()
                                .addComponent(jLabel53, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel54, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel18Layout.setVerticalGroup(
                jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel18Layout.createSequentialGroup()
                                .addComponent(jLabel52, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel53, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel54, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel19.setBackground(new java.awt.Color(255, 255, 0));
        jPanel19.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel19.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel55.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel55.setText("ATLANTIC AVENUE");
        jLabel55.setToolTipText("");
        jLabel55.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel55.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel56.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel56.setText("0, 0");
        jLabel56.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel56.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel57.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel57.setText("Bank");
        jLabel57.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel57.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel57.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
                jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel55, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel19Layout.createSequentialGroup()
                                .addComponent(jLabel56, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel57, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel19Layout.setVerticalGroup(
                jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel19Layout.createSequentialGroup()
                                .addComponent(jLabel55, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel56, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel57, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel20.setBackground(new java.awt.Color(255, 255, 0));
        jPanel20.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel20.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel58.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel58.setText("VENTNOR AVENUE");
        jLabel58.setToolTipText("");
        jLabel58.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel58.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel59.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel59.setText("0, 0");
        jLabel59.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel59.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel60.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel60.setText("Bank");
        jLabel60.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel60.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel60.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
                jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel58, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel20Layout.createSequentialGroup()
                                .addComponent(jLabel59, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel60, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel20Layout.setVerticalGroup(
                jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel20Layout.createSequentialGroup()
                                .addComponent(jLabel58, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel59, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel60, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel21.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel21.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel61.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel61.setText("WATER WORKS");
        jLabel61.setToolTipText("");
        jLabel61.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel61.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel62.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel62.setText("Owner");
        jLabel62.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel62.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel63.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel63.setText("Bank");
        jLabel63.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel63.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel63.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel21Layout = new javax.swing.GroupLayout(jPanel21);
        jPanel21.setLayout(jPanel21Layout);
        jPanel21Layout.setHorizontalGroup(
                jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel61, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel21Layout.createSequentialGroup()
                                .addComponent(jLabel62, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel63, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel21Layout.setVerticalGroup(
                jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel21Layout.createSequentialGroup()
                                .addComponent(jLabel61, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel62, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel63, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel22.setBackground(new java.awt.Color(255, 255, 0));
        jPanel22.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel22.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel64.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel64.setText("MARVIN GARDEN");
        jLabel64.setToolTipText("");
        jLabel64.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel64.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel65.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel65.setText("0, 0");
        jLabel65.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel65.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel66.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel66.setText("Bank");
        jLabel66.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel66.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel66.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
                jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel64, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel22Layout.createSequentialGroup()
                                .addComponent(jLabel65, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel66, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel22Layout.setVerticalGroup(
                jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel22Layout.createSequentialGroup()
                                .addComponent(jLabel64, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel65, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel66, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel23.setBackground(new java.awt.Color(0, 255, 0));
        jPanel23.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel23.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel85.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel85.setText("PACIFIC AVENUE");
        jLabel85.setToolTipText("");
        jLabel85.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel85.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel86.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel86.setText("0, 0");
        jLabel86.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel86.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel87.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel87.setText("Bank");
        jLabel87.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel87.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel87.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel23Layout = new javax.swing.GroupLayout(jPanel23);
        jPanel23.setLayout(jPanel23Layout);
        jPanel23Layout.setHorizontalGroup(
                jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel85, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel23Layout.createSequentialGroup()
                                .addComponent(jLabel86, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel87, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel23Layout.setVerticalGroup(
                jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel23Layout.createSequentialGroup()
                                .addComponent(jLabel85, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel86, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel87, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel24.setBackground(new java.awt.Color(0, 255, 0));
        jPanel24.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel24.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel67.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel67.setText("<HTML>NORTH<BR>CAROLINA<BR>AVENUE</HTML>");
        jLabel67.setToolTipText("");
        jLabel67.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel67.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel68.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel68.setText("0, 0");
        jLabel68.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel68.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel69.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel69.setText("Bank");
        jLabel69.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel69.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel69.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel24Layout = new javax.swing.GroupLayout(jPanel24);
        jPanel24.setLayout(jPanel24Layout);
        jPanel24Layout.setHorizontalGroup(
                jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel67, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel24Layout.createSequentialGroup()
                                .addComponent(jLabel68, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel69, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel24Layout.setVerticalGroup(
                jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel24Layout.createSequentialGroup()
                                .addComponent(jLabel67, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel68, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel69, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel25.setBackground(new java.awt.Color(0, 255, 0));
        jPanel25.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel25.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel70.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel70.setText("<HTML>PENNSYLVANIA<BR>AVENUE<HTML>");
        jLabel70.setToolTipText("");
        jLabel70.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel70.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel71.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel71.setText("0, 0");
        jLabel71.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel71.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel72.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel72.setText("Bank");
        jLabel72.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel72.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel72.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel25Layout = new javax.swing.GroupLayout(jPanel25);
        jPanel25.setLayout(jPanel25Layout);
        jPanel25Layout.setHorizontalGroup(
                jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel70, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel25Layout.createSequentialGroup()
                                .addComponent(jLabel71, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel72, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel25Layout.setVerticalGroup(
                jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel25Layout.createSequentialGroup()
                                .addComponent(jLabel70, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel71, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel72, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel26.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel26.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel73.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel73.setText("SHORT LINE");
        jLabel73.setToolTipText("");
        jLabel73.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel73.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel74.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel74.setText("Owner");
        jLabel74.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel74.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel75.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel75.setText("Bank");
        jLabel75.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel75.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel75.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel26Layout = new javax.swing.GroupLayout(jPanel26);
        jPanel26.setLayout(jPanel26Layout);
        jPanel26Layout.setHorizontalGroup(
                jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel73, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel26Layout.createSequentialGroup()
                                .addComponent(jLabel74, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel75, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel26Layout.setVerticalGroup(
                jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel26Layout.createSequentialGroup()
                                .addComponent(jLabel73, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel74, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel75, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel27.setBackground(new java.awt.Color(0, 0, 255));
        jPanel27.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel27.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel76.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel76.setText("PARK PLACE");
        jLabel76.setToolTipText("");
        jLabel76.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel76.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel77.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel77.setText("0, 0");
        jLabel77.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel77.setPreferredSize(new java.awt.Dimension(60, 50));

        jLabel78.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel78.setText("Bank");
        jLabel78.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel78.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel78.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel27Layout = new javax.swing.GroupLayout(jPanel27);
        jPanel27.setLayout(jPanel27Layout);
        jPanel27Layout.setHorizontalGroup(
                jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel76, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel27Layout.createSequentialGroup()
                                .addComponent(jLabel77, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel78, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel27Layout.setVerticalGroup(
                jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel27Layout.createSequentialGroup()
                                .addComponent(jLabel76, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel77, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel78, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel28.setBackground(new java.awt.Color(0, 0, 255));
        jPanel28.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel28.setPreferredSize(new java.awt.Dimension(120, 140));

        jLabel79.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel79.setText("BOARDWALK");
        jLabel79.setToolTipText("");
        jLabel79.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel79.setPreferredSize(new java.awt.Dimension(120, 90));

        jLabel80.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel80.setText("0, 0");
        jLabel80.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel80.setPreferredSize(new java.awt.Dimension(60, 50));
         
        
        
        jLabel81.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel81.setText("Bank");
        jLabel81.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel81.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel81.setPreferredSize(new java.awt.Dimension(60, 50));

        javax.swing.GroupLayout jPanel28Layout = new javax.swing.GroupLayout(jPanel28);
        jPanel28.setLayout(jPanel28Layout);
        jPanel28Layout.setHorizontalGroup(
                jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel79, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel28Layout.createSequentialGroup()
                                .addComponent(jLabel80, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel81, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel28Layout.setVerticalGroup(
                jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel28Layout.createSequentialGroup()
                                .addComponent(jLabel79, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel80, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel81, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout DownPanelLayout = new javax.swing.GroupLayout(DownPanel);
        DownPanel.setLayout(DownPanelLayout);
        DownPanelLayout.setHorizontalGroup(
                DownPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(DownPanelLayout.createSequentialGroup()
                                .addGroup(DownPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(DownPanelLayout.createSequentialGroup()
                                                .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(jPanel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(jPanel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(jPanel23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(DownPanelLayout.createSequentialGroup()
                                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(jPanel9_ok, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, 0)
                                .addGroup(DownPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, DownPanelLayout.createSequentialGroup()
                                                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, DownPanelLayout.createSequentialGroup()
                                                .addComponent(jPanel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(jPanel25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(jPanel26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(jPanel27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, 0)
                                                .addComponent(jPanel28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        DownPanelLayout.setVerticalGroup(
                DownPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, DownPanelLayout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addGroup(DownPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jPanel9_ok, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addGroup(DownPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jPanel28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(SidePanel, javax.swing.GroupLayout.DEFAULT_SIZE, 574, Short.MAX_VALUE)
                                                .addContainerGap())
                                        .addComponent(DownPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 1670, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(SidePanel, javax.swing.GroupLayout.DEFAULT_SIZE, 4420, Short.MAX_VALUE))
                                .addGap(0, 0, 0)
                                .addComponent(DownPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        
        
        pack();
    }// </editor-fold>                        

    // Variables declaration - do not modify                     
    private javax.swing.JPanel DownPanel;
    private javax.swing.JPanel MainPanel;

    public void setDownPanel(JPanel DownPanel) {
        this.DownPanel = DownPanel;
    }

    public void setMainPanel(JPanel MainPanel) {
        this.MainPanel = MainPanel;
    }

    public void setNamePanel_01(JPanel NamePanel_01) {
        this.NamePanel_01 = NamePanel_01;
    }

    public void setNamePanel_02(JPanel NamePanel_02) {
        this.NamePanel_02 = NamePanel_02;
    }

    public void setNamePanel_03(JPanel NamePanel_03) {
        this.NamePanel_03 = NamePanel_03;
    }

    public void setNamePanel_04(JPanel NamePanel_04) {
        this.NamePanel_04 = NamePanel_04;
    }

    public void setNamePanel_05(JPanel NamePanel_05) {
        this.NamePanel_05 = NamePanel_05;
    }

    public void setNamePanel_06(JPanel NamePanel_06) {
        this.NamePanel_06 = NamePanel_06;
    }

    public void setNamePanel_07(JPanel NamePanel_07) {
        this.NamePanel_07 = NamePanel_07;
    }

    public void setNamePanel_08(JPanel NamePanel_08) {
        this.NamePanel_08 = NamePanel_08;
    }

    public void setNamePanel_09(JPanel NamePanel_09) {
        this.NamePanel_09 = NamePanel_09;
    }

    public void setNamePanel_11(JPanel NamePanel_11) {
        this.NamePanel_11 = NamePanel_11;
    }

    public void setNamePanel_12(JPanel NamePanel_12) {
        this.NamePanel_12 = NamePanel_12;
    }

    public void setNamePanel_13(JPanel NamePanel_13) {
        this.NamePanel_13 = NamePanel_13;
    }

    public void setNamePanel_14(JPanel NamePanel_14) {
        this.NamePanel_14 = NamePanel_14;
    }

    public void setNamePanel_15(JPanel NamePanel_15) {
        this.NamePanel_15 = NamePanel_15;
    }

    public void setNamePanel_16(JPanel NamePanel_16) {
        this.NamePanel_16 = NamePanel_16;
    }

    public void setNamePanel_17(JPanel NamePanel_17) {
        this.NamePanel_17 = NamePanel_17;
    }

    public void setNamePanel_18(JPanel NamePanel_18) {
        this.NamePanel_18 = NamePanel_18;
    }

    public void setNamePanel_19(JPanel NamePanel_19) {
        this.NamePanel_19 = NamePanel_19;
    }

    public void setNamePanel_21(JPanel NamePanel_21) {
        this.NamePanel_21 = NamePanel_21;
    }

    public void setNamePanel_22(JPanel NamePanel_22) {
        this.NamePanel_22 = NamePanel_22;
    }

    public void setNamePanel_23(JPanel NamePanel_23) {
        this.NamePanel_23 = NamePanel_23;
    }

    public void setNamePanel_24(JPanel NamePanel_24) {
        this.NamePanel_24 = NamePanel_24;
    }

    public void setNamePanel_25(JPanel NamePanel_25) {
        this.NamePanel_25 = NamePanel_25;
    }

    public void setNamePanel_26(JPanel NamePanel_26) {
        this.NamePanel_26 = NamePanel_26;
    }

    public void setNamePanel_27(JPanel NamePanel_27) {
        this.NamePanel_27 = NamePanel_27;
    }

    public void setNamePanel_28(JPanel NamePanel_28) {
        this.NamePanel_28 = NamePanel_28;
    }

    public void setNamePanel_29(JPanel NamePanel_29) {
        this.NamePanel_29 = NamePanel_29;
    }

    public void setNamePanel_31(JPanel NamePanel_31) {
        this.NamePanel_31 = NamePanel_31;
    }

    public void setNamePanel_32(JPanel NamePanel_32) {
        this.NamePanel_32 = NamePanel_32;
    }

    public void setNamePanel_33(JPanel NamePanel_33) {
        this.NamePanel_33 = NamePanel_33;
    }

    public void setNamePanel_34(JPanel NamePanel_34) {
        this.NamePanel_34 = NamePanel_34;
    }

    public void setNamePanel_35(JPanel NamePanel_35) {
        this.NamePanel_35 = NamePanel_35;
    }

    public void setNamePanel_36(JPanel NamePanel_36) {
        this.NamePanel_36 = NamePanel_36;
    }

    public void setNamePanel_37(JPanel NamePanel_37) {
        this.NamePanel_37 = NamePanel_37;
    }

    public void setNamePanel_38(JPanel NamePanel_38) {
        this.NamePanel_38 = NamePanel_38;
    }

    public void setNamePanel_39(JPanel NamePanel_39) {
        this.NamePanel_39 = NamePanel_39;
    }

    public void setPlayer0001(JPanel Player0001) {
        this.Player0001 = Player0001;
    }

    public void setPlayer0002(JPanel Player0002) {
        this.Player0002 = Player0002;
    }

    public void setPlayer0003(JPanel Player0003) {
        this.Player0003 = Player0003;
    }

    public void setPlayer0004(JPanel Player0004) {
        this.Player0004 = Player0004;
    }

    public void setPlayer0005(JPanel Player0005) {
        this.Player0005 = Player0005;
    }

    public void setPlayer0006(JPanel Player0006) {
        this.Player0006 = Player0006;
    }

    public void setPlayer0101(JPanel Player0101) {
        this.Player0101 = Player0101;
    }

    public void setPlayer0102(JPanel Player0102) {
        this.Player0102 = Player0102;
    }

    public void setPlayer0103(JPanel Player0103) {
        this.Player0103 = Player0103;
    }

    public void setPlayer0104(JPanel Player0104) {
        this.Player0104 = Player0104;
    }

    public void setPlayer0105(JPanel Player0105) {
        this.Player0105 = Player0105;
    }

    public void setPlayer0106(JPanel Player0106) {
        this.Player0106 = Player0106;
    }

    public void setPlayer0201(JPanel Player0201) {
        this.Player0201 = Player0201;
    }

    public void setPlayer0202(JPanel Player0202) {
        this.Player0202 = Player0202;
    }

    public void setPlayer0203(JPanel Player0203) {
        this.Player0203 = Player0203;
    }

    public void setPlayer0204(JPanel Player0204) {
        this.Player0204 = Player0204;
    }

    public void setPlayer0205(JPanel Player0205) {
        this.Player0205 = Player0205;
    }

    public void setPlayer0206(JPanel Player0206) {
        this.Player0206 = Player0206;
    }

    public void setPlayer0301(JPanel Player0301) {
        this.Player0301 = Player0301;
    }

    public void setPlayer0302(JPanel Player0302) {
        this.Player0302 = Player0302;
    }

    public void setPlayer0303(JPanel Player0303) {
        this.Player0303 = Player0303;
    }

    public void setPlayer0304(JPanel Player0304) {
        this.Player0304 = Player0304;
    }

    public void setPlayer0305(JPanel Player0305) {
        this.Player0305 = Player0305;
    }

    public void setPlayer0306(JPanel Player0306) {
        this.Player0306 = Player0306;
    }

    public void setPlayer0401(JPanel Player0401) {
        this.Player0401 = Player0401;
    }

    public void setPlayer0402(JPanel Player0402) {
        this.Player0402 = Player0402;
    }

    public void setPlayer0403(JPanel Player0403) {
        this.Player0403 = Player0403;
    }

    public void setPlayer0404(JPanel Player0404) {
        this.Player0404 = Player0404;
    }

    public void setPlayer0405(JPanel Player0405) {
        this.Player0405 = Player0405;
    }

    public void setPlayer0406(JPanel Player0406) {
        this.Player0406 = Player0406;
    }

    public void setPlayer0501(JPanel Player0501) {
        this.Player0501 = Player0501;
    }

    public void setPlayer0502(JPanel Player0502) {
        this.Player0502 = Player0502;
    }

    public void setPlayer0503(JPanel Player0503) {
        this.Player0503 = Player0503;
    }

    public void setPlayer0504(JPanel Player0504) {
        this.Player0504 = Player0504;
    }

    public void setPlayer0505(JPanel Player0505) {
        this.Player0505 = Player0505;
    }

    public void setPlayer0506(JPanel Player0506) {
        this.Player0506 = Player0506;
    }

    public void setPlayer0601(JPanel Player0601) {
        this.Player0601 = Player0601;
    }

    public void setPlayer0602(JPanel Player0602) {
        this.Player0602 = Player0602;
    }

    public void setPlayer0603(JPanel Player0603) {
        this.Player0603 = Player0603;
    }

    public void setPlayer0604(JPanel Player0604) {
        this.Player0604 = Player0604;
    }

    public void setPlayer0605(JPanel Player0605) {
        this.Player0605 = Player0605;
    }

    public void setPlayer0606(JPanel Player0606) {
        this.Player0606 = Player0606;
    }

    public void setPlayer0701(JPanel Player0701) {
        this.Player0701 = Player0701;
    }

    public void setPlayer0702(JPanel Player0702) {
        this.Player0702 = Player0702;
    }

    public void setPlayer0703(JPanel Player0703) {
        this.Player0703 = Player0703;
    }

    public void setPlayer0704(JPanel Player0704) {
        this.Player0704 = Player0704;
    }

    public void setPlayer0705(JPanel Player0705) {
        this.Player0705 = Player0705;
    }

    public void setPlayer0706(JPanel Player0706) {
        this.Player0706 = Player0706;
    }

    public void setPlayer0801(JPanel Player0801) {
        this.Player0801 = Player0801;
    }

    public void setPlayer0802(JPanel Player0802) {
        this.Player0802 = Player0802;
    }

    public void setPlayer0803(JPanel Player0803) {
        this.Player0803 = Player0803;
    }

    public void setPlayer0804(JPanel Player0804) {
        this.Player0804 = Player0804;
    }

    public void setPlayer0805(JPanel Player0805) {
        this.Player0805 = Player0805;
    }

    public void setPlayer0806(JPanel Player0806) {
        this.Player0806 = Player0806;
    }

    public void setPlayer0901(JPanel Player0901) {
        this.Player0901 = Player0901;
    }

    public void setPlayer0902(JPanel Player0902) {
        this.Player0902 = Player0902;
    }

    public void setPlayer0903(JPanel Player0903) {
        this.Player0903 = Player0903;
    }

    public void setPlayer0904(JPanel Player0904) {
        this.Player0904 = Player0904;
    }

    public void setPlayer0905(JPanel Player0905) {
        this.Player0905 = Player0905;
    }

    public void setPlayer0906(JPanel Player0906) {
        this.Player0906 = Player0906;
    }

    public void setPlayer1(JPanel Player1) {
        this.Player1 = Player1;
    }

    public void setPlayer1001(JPanel Player1001) {
        this.Player1001 = Player1001;
    }

    public void setPlayer1002(JPanel Player1002) {
        this.Player1002 = Player1002;
    }

    public void setPlayer1003(JPanel Player1003) {
        this.Player1003 = Player1003;
    }

    public void setPlayer1004(JPanel Player1004) {
        this.Player1004 = Player1004;
    }

    public void setPlayer1005(JPanel Player1005) {
        this.Player1005 = Player1005;
    }

    public void setPlayer1006(JPanel Player1006) {
        this.Player1006 = Player1006;
    }

    public void setPlayer1101(JPanel Player1101) {
        this.Player1101 = Player1101;
    }

    public void setPlayer1102(JPanel Player1102) {
        this.Player1102 = Player1102;
    }

    public void setPlayer1103(JPanel Player1103) {
        this.Player1103 = Player1103;
    }

    public void setPlayer1104(JPanel Player1104) {
        this.Player1104 = Player1104;
    }

    public void setPlayer1105(JPanel Player1105) {
        this.Player1105 = Player1105;
    }

    public void setPlayer1106(JPanel Player1106) {
        this.Player1106 = Player1106;
    }

    public void setPlayer1201(JPanel Player1201) {
        this.Player1201 = Player1201;
    }

    public void setPlayer1202(JPanel Player1202) {
        this.Player1202 = Player1202;
    }

    public void setPlayer1203(JPanel Player1203) {
        this.Player1203 = Player1203;
    }

    public void setPlayer1204(JPanel Player1204) {
        this.Player1204 = Player1204;
    }

    public void setPlayer1205(JPanel Player1205) {
        this.Player1205 = Player1205;
    }

    public void setPlayer1206(JPanel Player1206) {
        this.Player1206 = Player1206;
    }

    public void setPlayer1301(JPanel Player1301) {
        this.Player1301 = Player1301;
    }

    public void setPlayer1302(JPanel Player1302) {
        this.Player1302 = Player1302;
    }

    public void setPlayer1303(JPanel Player1303) {
        this.Player1303 = Player1303;
    }

    public void setPlayer1304(JPanel Player1304) {
        this.Player1304 = Player1304;
    }

    public void setPlayer1305(JPanel Player1305) {
        this.Player1305 = Player1305;
    }

    public void setPlayer1306(JPanel Player1306) {
        this.Player1306 = Player1306;
    }

    public void setPlayer1401(JPanel Player1401) {
        this.Player1401 = Player1401;
    }

    public void setPlayer1402(JPanel Player1402) {
        this.Player1402 = Player1402;
    }

    public void setPlayer1403(JPanel Player1403) {
        this.Player1403 = Player1403;
    }

    public void setPlayer1404(JPanel Player1404) {
        this.Player1404 = Player1404;
    }

    public void setPlayer1405(JPanel Player1405) {
        this.Player1405 = Player1405;
    }

    public void setPlayer1406(JPanel Player1406) {
        this.Player1406 = Player1406;
    }

    public void setPlayer1501(JPanel Player1501) {
        this.Player1501 = Player1501;
    }

    public void setPlayer1502(JPanel Player1502) {
        this.Player1502 = Player1502;
    }

    public void setPlayer1503(JPanel Player1503) {
        this.Player1503 = Player1503;
    }

    public void setPlayer1504(JPanel Player1504) {
        this.Player1504 = Player1504;
    }

    public void setPlayer1505(JPanel Player1505) {
        this.Player1505 = Player1505;
    }

    public void setPlayer1506(JPanel Player1506) {
        this.Player1506 = Player1506;
    }

    public void setPlayer1601(JPanel Player1601) {
        this.Player1601 = Player1601;
    }

    public void setPlayer1602(JPanel Player1602) {
        this.Player1602 = Player1602;
    }

    public void setPlayer1603(JPanel Player1603) {
        this.Player1603 = Player1603;
    }

    public void setPlayer1604(JPanel Player1604) {
        this.Player1604 = Player1604;
    }

    public void setPlayer1605(JPanel Player1605) {
        this.Player1605 = Player1605;
    }

    public void setPlayer1606(JPanel Player1606) {
        this.Player1606 = Player1606;
    }

    public void setPlayer1701(JPanel Player1701) {
        this.Player1701 = Player1701;
    }

    public void setPlayer1702(JPanel Player1702) {
        this.Player1702 = Player1702;
    }

    public void setPlayer1703(JPanel Player1703) {
        this.Player1703 = Player1703;
    }

    public void setPlayer1704(JPanel Player1704) {
        this.Player1704 = Player1704;
    }

    public void setPlayer1705(JPanel Player1705) {
        this.Player1705 = Player1705;
    }

    public void setPlayer1706(JPanel Player1706) {
        this.Player1706 = Player1706;
    }

    public void setPlayer1801(JPanel Player1801) {
        this.Player1801 = Player1801;
    }

    public void setPlayer1802(JPanel Player1802) {
        this.Player1802 = Player1802;
    }

    public void setPlayer1803(JPanel Player1803) {
        this.Player1803 = Player1803;
    }

    public void setPlayer1804(JPanel Player1804) {
        this.Player1804 = Player1804;
    }

    public void setPlayer1805(JPanel Player1805) {
        this.Player1805 = Player1805;
    }

    public void setPlayer1806(JPanel Player1806) {
        this.Player1806 = Player1806;
    }

    public void setPlayer1901(JPanel Player1901) {
        this.Player1901 = Player1901;
    }

    public void setPlayer1902(JPanel Player1902) {
        this.Player1902 = Player1902;
    }

    public void setPlayer1903(JPanel Player1903) {
        this.Player1903 = Player1903;
    }

    public void setPlayer1904(JPanel Player1904) {
        this.Player1904 = Player1904;
    }

    public void setPlayer1905(JPanel Player1905) {
        this.Player1905 = Player1905;
    }

    public void setPlayer1906(JPanel Player1906) {
        this.Player1906 = Player1906;
    }

    public void setPlayer1_Balance(JLabel Player1_Balance) {
        this.Player1_Balance = Player1_Balance;
    }

    public void setPlayer1_BalanceD(JLabel Player1_BalanceD) {
        this.Player1_BalanceD = Player1_BalanceD;
    }

    public void setPlayer1_Color(JPanel Player1_Color) {
        this.Player1_Color = Player1_Color;
    }

    public void setPlayer1_InJail(JLabel Player1_InJail) {
        this.Player1_InJail = Player1_InJail;
    }

    public void setPlayer1_InJailD(JLabel Player1_InJailD) {
        this.Player1_InJailD = Player1_InJailD;
    }

    public void setPlayer1_Name(JLabel Player1_Name) {
        this.Player1_Name = Player1_Name;
    }

    public void setPlayer1_NameD(JLabel Player1_NameD) {
        this.Player1_NameD = Player1_NameD;
    }

    public void setPlayer2001(JPanel Player2001) {
        this.Player2001 = Player2001;
    }

    public void setPlayer2002(JPanel Player2002) {
        this.Player2002 = Player2002;
    }

    public void setPlayer2003(JPanel Player2003) {
        this.Player2003 = Player2003;
    }

    public void setPlayer2004(JPanel Player2004) {
        this.Player2004 = Player2004;
    }

    public void setPlayer2005(JPanel Player2005) {
        this.Player2005 = Player2005;
    }

    public void setPlayer2006(JPanel Player2006) {
        this.Player2006 = Player2006;
    }

    public void setPlayer2101(JPanel Player2101) {
        this.Player2101 = Player2101;
    }

    public void setPlayer2102(JPanel Player2102) {
        this.Player2102 = Player2102;
    }

    public void setPlayer2103(JPanel Player2103) {
        this.Player2103 = Player2103;
    }

    public void setPlayer2104(JPanel Player2104) {
        this.Player2104 = Player2104;
    }

    public void setPlayer2105(JPanel Player2105) {
        this.Player2105 = Player2105;
    }

    public void setPlayer2106(JPanel Player2106) {
        this.Player2106 = Player2106;
    }

    public void setPlayer2201(JPanel Player2201) {
        this.Player2201 = Player2201;
    }

    public void setPlayer2202(JPanel Player2202) {
        this.Player2202 = Player2202;
    }

    public void setPlayer2203(JPanel Player2203) {
        this.Player2203 = Player2203;
    }

    public void setPlayer2204(JPanel Player2204) {
        this.Player2204 = Player2204;
    }

    public void setPlayer2205(JPanel Player2205) {
        this.Player2205 = Player2205;
    }

    public void setPlayer2206(JPanel Player2206) {
        this.Player2206 = Player2206;
    }

    public void setPlayer2301(JPanel Player2301) {
        this.Player2301 = Player2301;
    }

    public void setPlayer2302(JPanel Player2302) {
        this.Player2302 = Player2302;
    }

    public void setPlayer2303(JPanel Player2303) {
        this.Player2303 = Player2303;
    }

    public void setPlayer2304(JPanel Player2304) {
        this.Player2304 = Player2304;
    }

    public void setPlayer2305(JPanel Player2305) {
        this.Player2305 = Player2305;
    }

    public void setPlayer2306(JPanel Player2306) {
        this.Player2306 = Player2306;
    }

    public void setPlayer2401(JPanel Player2401) {
        this.Player2401 = Player2401;
    }

    public void setPlayer2402(JPanel Player2402) {
        this.Player2402 = Player2402;
    }

    public void setPlayer2403(JPanel Player2403) {
        this.Player2403 = Player2403;
    }

    public void setPlayer2404(JPanel Player2404) {
        this.Player2404 = Player2404;
    }

    public void setPlayer2405(JPanel Player2405) {
        this.Player2405 = Player2405;
    }

    public void setPlayer2406(JPanel Player2406) {
        this.Player2406 = Player2406;
    }

    public void setPlayer2501(JPanel Player2501) {
        this.Player2501 = Player2501;
    }

    public void setPlayer2502(JPanel Player2502) {
        this.Player2502 = Player2502;
    }

    public void setPlayer2503(JPanel Player2503) {
        this.Player2503 = Player2503;
    }

    public void setPlayer2504(JPanel Player2504) {
        this.Player2504 = Player2504;
    }

    public void setPlayer2505(JPanel Player2505) {
        this.Player2505 = Player2505;
    }

    public void setPlayer2506(JPanel Player2506) {
        this.Player2506 = Player2506;
    }

    public void setPlayer2601(JPanel Player2601) {
        this.Player2601 = Player2601;
    }

    public void setPlayer2602(JPanel Player2602) {
        this.Player2602 = Player2602;
    }

    public void setPlayer2603(JPanel Player2603) {
        this.Player2603 = Player2603;
    }

    public void setPlayer2604(JPanel Player2604) {
        this.Player2604 = Player2604;
    }

    public void setPlayer2605(JPanel Player2605) {
        this.Player2605 = Player2605;
    }

    public void setPlayer2606(JPanel Player2606) {
        this.Player2606 = Player2606;
    }

    public void setPlayer2701(JPanel Player2701) {
        this.Player2701 = Player2701;
    }

    public void setPlayer2702(JPanel Player2702) {
        this.Player2702 = Player2702;
    }

    public void setPlayer2703(JPanel Player2703) {
        this.Player2703 = Player2703;
    }

    public void setPlayer2704(JPanel Player2704) {
        this.Player2704 = Player2704;
    }

    public void setPlayer2705(JPanel Player2705) {
        this.Player2705 = Player2705;
    }

    public void setPlayer2706(JPanel Player2706) {
        this.Player2706 = Player2706;
    }

    public void setPlayer2801(JPanel Player2801) {
        this.Player2801 = Player2801;
    }

    public void setPlayer2802(JPanel Player2802) {
        this.Player2802 = Player2802;
    }

    public void setPlayer2803(JPanel Player2803) {
        this.Player2803 = Player2803;
    }

    public void setPlayer2804(JPanel Player2804) {
        this.Player2804 = Player2804;
    }

    public void setPlayer2805(JPanel Player2805) {
        this.Player2805 = Player2805;
    }

    public void setPlayer2806(JPanel Player2806) {
        this.Player2806 = Player2806;
    }

    public void setPlayer2901(JPanel Player2901) {
        this.Player2901 = Player2901;
    }

    public void setPlayer2902(JPanel Player2902) {
        this.Player2902 = Player2902;
    }

    public void setPlayer2903(JPanel Player2903) {
        this.Player2903 = Player2903;
    }

    public void setPlayer2904(JPanel Player2904) {
        this.Player2904 = Player2904;
    }

    public void setPlayer2905(JPanel Player2905) {
        this.Player2905 = Player2905;
    }

    public void setPlayer2906(JPanel Player2906) {
        this.Player2906 = Player2906;
    }

    public void setPlayer3001(JPanel Player3001) {
        this.Player3001 = Player3001;
    }

    public void setPlayer3002(JPanel Player3002) {
        this.Player3002 = Player3002;
    }

    public void setPlayer3003(JPanel Player3003) {
        this.Player3003 = Player3003;
    }

    public void setPlayer3004(JPanel Player3004) {
        this.Player3004 = Player3004;
    }

    public void setPlayer3005(JPanel Player3005) {
        this.Player3005 = Player3005;
    }

    public void setPlayer3006(JPanel Player3006) {
        this.Player3006 = Player3006;
    }

    public void setPlayer3101(JPanel Player3101) {
        this.Player3101 = Player3101;
    }

    public void setPlayer3102(JPanel Player3102) {
        this.Player3102 = Player3102;
    }

    public void setPlayer3103(JPanel Player3103) {
        this.Player3103 = Player3103;
    }

    public void setPlayer3104(JPanel Player3104) {
        this.Player3104 = Player3104;
    }

    public void setPlayer3105(JPanel Player3105) {
        this.Player3105 = Player3105;
    }

    public void setPlayer3106(JPanel Player3106) {
        this.Player3106 = Player3106;
    }

    public void setPlayer3201(JPanel Player3201) {
        this.Player3201 = Player3201;
    }

    public void setPlayer3202(JPanel Player3202) {
        this.Player3202 = Player3202;
    }

    public void setPlayer3203(JPanel Player3203) {
        this.Player3203 = Player3203;
    }

    public void setPlayer3204(JPanel Player3204) {
        this.Player3204 = Player3204;
    }

    public void setPlayer3205(JPanel Player3205) {
        this.Player3205 = Player3205;
    }

    public void setPlayer3206(JPanel Player3206) {
        this.Player3206 = Player3206;
    }

    public void setPlayer3301(JPanel Player3301) {
        this.Player3301 = Player3301;
    }

    public void setPlayer3302(JPanel Player3302) {
        this.Player3302 = Player3302;
    }

    public void setPlayer3303(JPanel Player3303) {
        this.Player3303 = Player3303;
    }

    public void setPlayer3304(JPanel Player3304) {
        this.Player3304 = Player3304;
    }

    public void setPlayer3305(JPanel Player3305) {
        this.Player3305 = Player3305;
    }

    public void setPlayer3306(JPanel Player3306) {
        this.Player3306 = Player3306;
    }

    public void setPlayer3401(JPanel Player3401) {
        this.Player3401 = Player3401;
    }

    public void setPlayer3402(JPanel Player3402) {
        this.Player3402 = Player3402;
    }

    public void setPlayer3403(JPanel Player3403) {
        this.Player3403 = Player3403;
    }

    public void setPlayer3404(JPanel Player3404) {
        this.Player3404 = Player3404;
    }

    public void setPlayer3405(JPanel Player3405) {
        this.Player3405 = Player3405;
    }

    public void setPlayer3406(JPanel Player3406) {
        this.Player3406 = Player3406;
    }

    public void setPlayer3501(JPanel Player3501) {
        this.Player3501 = Player3501;
    }

    public void setPlayer3502(JPanel Player3502) {
        this.Player3502 = Player3502;
    }

    public void setPlayer3503(JPanel Player3503) {
        this.Player3503 = Player3503;
    }

    public void setPlayer3504(JPanel Player3504) {
        this.Player3504 = Player3504;
    }

    public void setPlayer3505(JPanel Player3505) {
        this.Player3505 = Player3505;
    }

    public void setPlayer3506(JPanel Player3506) {
        this.Player3506 = Player3506;
    }

    public void setPlayer3601(JPanel Player3601) {
        this.Player3601 = Player3601;
    }

    public void setPlayer3602(JPanel Player3602) {
        this.Player3602 = Player3602;
    }

    public void setPlayer3603(JPanel Player3603) {
        this.Player3603 = Player3603;
    }

    public void setPlayer3604(JPanel Player3604) {
        this.Player3604 = Player3604;
    }

    public void setPlayer3605(JPanel Player3605) {
        this.Player3605 = Player3605;
    }

    public void setPlayer3606(JPanel Player3606) {
        this.Player3606 = Player3606;
    }

    public void setPlayer3701(JPanel Player3701) {
        this.Player3701 = Player3701;
    }

    public void setPlayer3702(JPanel Player3702) {
        this.Player3702 = Player3702;
    }

    public void setPlayer3703(JPanel Player3703) {
        this.Player3703 = Player3703;
    }

    public void setPlayer3704(JPanel Player3704) {
        this.Player3704 = Player3704;
    }

    public void setPlayer3705(JPanel Player3705) {
        this.Player3705 = Player3705;
    }

    public void setPlayer3706(JPanel Player3706) {
        this.Player3706 = Player3706;
    }

    public void setPlayer3801(JPanel Player3801) {
        this.Player3801 = Player3801;
    }

    public void setPlayer3802(JPanel Player3802) {
        this.Player3802 = Player3802;
    }

    public void setPlayer3803(JPanel Player3803) {
        this.Player3803 = Player3803;
    }

    public void setPlayer3804(JPanel Player3804) {
        this.Player3804 = Player3804;
    }

    public void setPlayer3805(JPanel Player3805) {
        this.Player3805 = Player3805;
    }

    public void setPlayer3806(JPanel Player3806) {
        this.Player3806 = Player3806;
    }

    public void setPlayer3901(JPanel Player3901) {
        this.Player3901 = Player3901;
    }

    public void setPlayer3902(JPanel Player3902) {
        this.Player3902 = Player3902;
    }

    public void setPlayer3903(JPanel Player3903) {
        this.Player3903 = Player3903;
    }

    public void setPlayer3904(JPanel Player3904) {
        this.Player3904 = Player3904;
    }

    public void setPlayer3905(JPanel Player3905) {
        this.Player3905 = Player3905;
    }

    public void setPlayer3906(JPanel Player3906) {
        this.Player3906 = Player3906;
    }

    public void setSidePanel(JPanel SidePanel) {
        this.SidePanel = SidePanel;
    }

    public void setTileName_01(JLabel TileName_01) {
        this.TileName_01 = TileName_01;
    }

    public void setTileName_02(JLabel TileName_02) {
        this.TileName_02 = TileName_02;
    }

    public void setTileName_03(JLabel TileName_03) {
        this.TileName_03 = TileName_03;
    }

    public void setTileName_04(JLabel TileName_04) {
        this.TileName_04 = TileName_04;
    }

    public void setTileName_05(JLabel TileName_05) {
        this.TileName_05 = TileName_05;
    }

    public void setTileName_06(JLabel TileName_06) {
        this.TileName_06 = TileName_06;
    }

    public void setTileName_07(JLabel TileName_07) {
        this.TileName_07 = TileName_07;
    }

    public void setTileName_08(JLabel TileName_08) {
        this.TileName_08 = TileName_08;
    }

    public void setTileName_09(JLabel TileName_09) {
        this.TileName_09 = TileName_09;
    }

    public void setTileName_11(JLabel TileName_11) {
        this.TileName_11 = TileName_11;
    }

    public void setTileName_12(JLabel TileName_12) {
        this.TileName_12 = TileName_12;
    }

    public void setTileName_13(JLabel TileName_13) {
        this.TileName_13 = TileName_13;
    }

    public void setTileName_14(JLabel TileName_14) {
        this.TileName_14 = TileName_14;
    }

    public void setTileName_15(JLabel TileName_15) {
        this.TileName_15 = TileName_15;
    }

    public void setTileName_16(JLabel TileName_16) {
        this.TileName_16 = TileName_16;
    }

    public void setTileName_17(JLabel TileName_17) {
        this.TileName_17 = TileName_17;
    }

    public void setTileName_18(JLabel TileName_18) {
        this.TileName_18 = TileName_18;
    }

    public void setTileName_19(JLabel TileName_19) {
        this.TileName_19 = TileName_19;
    }

    public void setTileName_21(JLabel TileName_21) {
        this.TileName_21 = TileName_21;
    }

    public void setTileName_22(JLabel TileName_22) {
        this.TileName_22 = TileName_22;
    }

    public void setTileName_23(JLabel TileName_23) {
        this.TileName_23 = TileName_23;
    }

    public void setTileName_24(JLabel TileName_24) {
        this.TileName_24 = TileName_24;
    }

    public void setTileName_25(JLabel TileName_25) {
        this.TileName_25 = TileName_25;
    }

    public void setTileName_26(JLabel TileName_26) {
        this.TileName_26 = TileName_26;
    }

    public void setTileName_27(JLabel TileName_27) {
        this.TileName_27 = TileName_27;
    }

    public void setTileName_28(JLabel TileName_28) {
        this.TileName_28 = TileName_28;
    }

    public void setTileName_29(JLabel TileName_29) {
        this.TileName_29 = TileName_29;
    }

    public void setTileName_31(JLabel TileName_31) {
        this.TileName_31 = TileName_31;
    }

    public void setTileName_32(JLabel TileName_32) {
        this.TileName_32 = TileName_32;
    }

    public void setTileName_33(JLabel TileName_33) {
        this.TileName_33 = TileName_33;
    }

    public void setTileName_34(JLabel TileName_34) {
        this.TileName_34 = TileName_34;
    }

    public void setTileName_35(JLabel TileName_35) {
        this.TileName_35 = TileName_35;
    }

    public void setTileName_36(JLabel TileName_36) {
        this.TileName_36 = TileName_36;
    }

    public void setTileName_37(JLabel TileName_37) {
        this.TileName_37 = TileName_37;
    }

    public void setTileName_38(JLabel TileName_38) {
        this.TileName_38 = TileName_38;
    }

    public void setTileName_39(JLabel TileName_39) {
        this.TileName_39 = TileName_39;
    }

    public void setTile_00(JPanel Tile_00) {
        this.Tile_00 = Tile_00;
    }

    public void setTile_01(JPanel Tile_01) {
        this.Tile_01 = Tile_01;
    }

    public void setTile_02(JPanel Tile_02) {
        this.Tile_02 = Tile_02;
    }

    public void setTile_03(JPanel Tile_03) {
        this.Tile_03 = Tile_03;
    }

    public void setTile_04(JPanel Tile_04) {
        this.Tile_04 = Tile_04;
    }

    public void setTile_05(JPanel Tile_05) {
        this.Tile_05 = Tile_05;
    }

    public void setTile_06(JPanel Tile_06) {
        this.Tile_06 = Tile_06;
    }

    public void setTile_07(JPanel Tile_07) {
        this.Tile_07 = Tile_07;
    }

    public void setTile_08(JPanel Tile_08) {
        this.Tile_08 = Tile_08;
    }

    public void setTile_09(JPanel Tile_09) {
        this.Tile_09 = Tile_09;
    }

    public void setTile_10(JPanel Tile_10) {
        this.Tile_10 = Tile_10;
    }

    public void setTile_11(JPanel Tile_11) {
        this.Tile_11 = Tile_11;
    }

    public void setTile_12(JPanel Tile_12) {
        this.Tile_12 = Tile_12;
    }

    public void setTile_13(JPanel Tile_13) {
        this.Tile_13 = Tile_13;
    }

    public void setTile_14(JPanel Tile_14) {
        this.Tile_14 = Tile_14;
    }

    public void setTile_15(JPanel Tile_15) {
        this.Tile_15 = Tile_15;
    }

    public void setTile_16(JPanel Tile_16) {
        this.Tile_16 = Tile_16;
    }

    public void setTile_17(JPanel Tile_17) {
        this.Tile_17 = Tile_17;
    }

    public void setTile_18(JPanel Tile_18) {
        this.Tile_18 = Tile_18;
    }

    public void setTile_19(JPanel Tile_19) {
        this.Tile_19 = Tile_19;
    }

    public void setTile_20(JPanel Tile_20) {
        this.Tile_20 = Tile_20;
    }

    public void setTile_21(JPanel Tile_21) {
        this.Tile_21 = Tile_21;
    }

    public void setTile_22(JPanel Tile_22) {
        this.Tile_22 = Tile_22;
    }

    public void setTile_23(JPanel Tile_23) {
        this.Tile_23 = Tile_23;
    }

    public void setTile_24(JPanel Tile_24) {
        this.Tile_24 = Tile_24;
    }

    public void setTile_25(JPanel Tile_25) {
        this.Tile_25 = Tile_25;
    }

    public void setTile_26(JPanel Tile_26) {
        this.Tile_26 = Tile_26;
    }

    public void setTile_27(JPanel Tile_27) {
        this.Tile_27 = Tile_27;
    }

    public void setTile_28(JPanel Tile_28) {
        this.Tile_28 = Tile_28;
    }

    public void setTile_29(JPanel Tile_29) {
        this.Tile_29 = Tile_29;
    }

    public void setTile_30(JPanel Tile_30) {
        this.Tile_30 = Tile_30;
    }

    public void setTile_31(JPanel Tile_31) {
        this.Tile_31 = Tile_31;
    }

    public void setTile_32(JPanel Tile_32) {
        this.Tile_32 = Tile_32;
    }

    public void setTile_33(JPanel Tile_33) {
        this.Tile_33 = Tile_33;
    }

    public void setTile_34(JPanel Tile_34) {
        this.Tile_34 = Tile_34;
    }

    public void setTile_35(JPanel Tile_35) {
        this.Tile_35 = Tile_35;
    }

    public void setTile_36(JPanel Tile_36) {
        this.Tile_36 = Tile_36;
    }

    public void setTile_37(JPanel Tile_37) {
        this.Tile_37 = Tile_37;
    }

    public void setTile_38(JPanel Tile_38) {
        this.Tile_38 = Tile_38;
    }

    public void setTile_39(JPanel Tile_39) {
        this.Tile_39 = Tile_39;
    }

    public void setInsied_area(JPanel insied_area) {
        this.insied_area = insied_area;
    }

    public void setjButton1(JButton jButton1) {
        this.jButton1 = jButton1;
    }

    public void setjButton2(JButton jButton2) {
        this.jButton2 = jButton2;
    }

    public void setjButton3(JButton jButton3) {
        this.jButton3 = jButton3;
    }

    public void setjButton4(JButton jButton4) {
        this.jButton4 = jButton4;
    }

    public void setjButton5(JButton jButton5) {
        this.jButton5 = jButton5;
    }

    public void setjButton6(JButton jButton6) {
        this.jButton6 = jButton6;
    }

    public void setjComboBox1(JComboBox<String> jComboBox1) {
        this.jComboBox1 = jComboBox1;
    }

    public void setjLabel1(JLabel jLabel1) {
        this.jLabel1 = jLabel1;
    }

    public void setjLabel10(JLabel jLabel10) {
        this.jLabel10 = jLabel10;
    }

    public void setjLabel11(JLabel jLabel11) {
        this.jLabel11 = jLabel11;
    }

    public void setjLabel12(JLabel jLabel12) {
        this.jLabel12 = jLabel12;
    }

    public void setjLabel13(JLabel jLabel13) {
        this.jLabel13 = jLabel13;
    }

    public void setjLabel14(JLabel jLabel14) {
        this.jLabel14 = jLabel14;
    }

    public void setjLabel15(JLabel jLabel15) {
        this.jLabel15 = jLabel15;
    }

    public void setjLabel16(JLabel jLabel16) {
        this.jLabel16 = jLabel16;
    }

    public void setjLabel17(JLabel jLabel17) {
        this.jLabel17 = jLabel17;
    }

    public void setjLabel18(JLabel jLabel18) {
        this.jLabel18 = jLabel18;
    }

    public void setjLabel19(JLabel jLabel19) {
        this.jLabel19 = jLabel19;
    }

    public void setjLabel2(JLabel jLabel2) {
        this.jLabel2 = jLabel2;
    }

    public void setjLabel20(JLabel jLabel20) {
        this.jLabel20 = jLabel20;
    }

    public void setjLabel21(JLabel jLabel21) {
        this.jLabel21 = jLabel21;
    }

    public void setjLabel22(JLabel jLabel22) {
        this.jLabel22 = jLabel22;
    }

    public void setjLabel23(JLabel jLabel23) {
        this.jLabel23 = jLabel23;
    }

    public void setjLabel24(JLabel jLabel24) {
        this.jLabel24 = jLabel24;
    }

    public void setjLabel25(JLabel jLabel25) {
        this.jLabel25 = jLabel25;
    }

    public void setjLabel28(JLabel jLabel28) {
        this.jLabel28 = jLabel28;
    }

    public void setjLabel29(JLabel jLabel29) {
        this.jLabel29 = jLabel29;
    }

    public void setjLabel3(JLabel jLabel3) {
        this.jLabel3 = jLabel3;
    }

    public void setjLabel30(JLabel jLabel30) {
        this.jLabel30 = jLabel30;
    }

    public void setjLabel31(JLabel jLabel31) {
        this.jLabel31 = jLabel31;
    }

    public void setjLabel32(JLabel jLabel32) {
        this.jLabel32 = jLabel32;
    }

    public void setjLabel33(JLabel jLabel33) {
        this.jLabel33 = jLabel33;
    }

    public void setjLabel34(JLabel jLabel34) {
        this.jLabel34 = jLabel34;
    }

    public void setjLabel35(JLabel jLabel35) {
        this.jLabel35 = jLabel35;
    }

    public void setjLabel36(JLabel jLabel36) {
        this.jLabel36 = jLabel36;
    }

    public void setjLabel37(JLabel jLabel37) {
        this.jLabel37 = jLabel37;
    }

    public void setjLabel38(JLabel jLabel38) {
        this.jLabel38 = jLabel38;
    }

    public void setjLabel39(JLabel jLabel39) {
        this.jLabel39 = jLabel39;
    }

    public void setjLabel4(JLabel jLabel4) {
        this.jLabel4 = jLabel4;
    }

    public void setjLabel40(JLabel jLabel40) {
        this.jLabel40 = jLabel40;
    }

    public void setjLabel41(JLabel jLabel41) {
        this.jLabel41 = jLabel41;
    }

    public void setjLabel42(JLabel jLabel42) {
        this.jLabel42 = jLabel42;
    }

    public void setjLabel43(JLabel jLabel43) {
        this.jLabel43 = jLabel43;
    }

    public void setjLabel44(JLabel jLabel44) {
        this.jLabel44 = jLabel44;
    }

    public void setjLabel45(JLabel jLabel45) {
        this.jLabel45 = jLabel45;
    }

    public void setjLabel46(JLabel jLabel46) {
        this.jLabel46 = jLabel46;
    }

    public void setjLabel47(JLabel jLabel47) {
        this.jLabel47 = jLabel47;
    }

    public void setjLabel48(JLabel jLabel48) {
        this.jLabel48 = jLabel48;
    }

    public void setjLabel49(JLabel jLabel49) {
        this.jLabel49 = jLabel49;
    }

    public void setjLabel5(JLabel jLabel5) {
        this.jLabel5 = jLabel5;
    }

    public void setjLabel50(JLabel jLabel50) {
        this.jLabel50 = jLabel50;
    }

    public void setjLabel51(JLabel jLabel51) {
        this.jLabel51 = jLabel51;
    }

    public void setjLabel52(JLabel jLabel52) {
        this.jLabel52 = jLabel52;
    }

    public void setjLabel53(JLabel jLabel53) {
        this.jLabel53 = jLabel53;
    }

    public void setjLabel54(JLabel jLabel54) {
        this.jLabel54 = jLabel54;
    }

    public void setjLabel55(JLabel jLabel55) {
        this.jLabel55 = jLabel55;
    }

    public void setjLabel56(JLabel jLabel56) {
        this.jLabel56 = jLabel56;
    }

    public void setjLabel57(JLabel jLabel57) {
        this.jLabel57 = jLabel57;
    }

    public void setjLabel58(JLabel jLabel58) {
        this.jLabel58 = jLabel58;
    }

    public void setjLabel59(JLabel jLabel59) {
        this.jLabel59 = jLabel59;
    }

    public void setjLabel6(JLabel jLabel6) {
        this.jLabel6 = jLabel6;
    }

    public void setjLabel60(JLabel jLabel60) {
        this.jLabel60 = jLabel60;
    }

    public void setjLabel61(JLabel jLabel61) {
        this.jLabel61 = jLabel61;
    }

    public void setjLabel62(JLabel jLabel62) {
        this.jLabel62 = jLabel62;
    }

    public void setjLabel63(JLabel jLabel63) {
        this.jLabel63 = jLabel63;
    }

    public void setjLabel64(JLabel jLabel64) {
        this.jLabel64 = jLabel64;
    }

    public void setjLabel65(JLabel jLabel65) {
        this.jLabel65 = jLabel65;
    }

    public void setjLabel66(JLabel jLabel66) {
        this.jLabel66 = jLabel66;
    }

    public void setjLabel67(JLabel jLabel67) {
        this.jLabel67 = jLabel67;
    }

    public void setjLabel68(JLabel jLabel68) {
        this.jLabel68 = jLabel68;
    }

    public void setjLabel69(JLabel jLabel69) {
        this.jLabel69 = jLabel69;
    }

    public void setjLabel7(JLabel jLabel7) {
        this.jLabel7 = jLabel7;
    }

    public void setjLabel70(JLabel jLabel70) {
        this.jLabel70 = jLabel70;
    }

    public void setjLabel71(JLabel jLabel71) {
        this.jLabel71 = jLabel71;
    }

    public void setjLabel72(JLabel jLabel72) {
        this.jLabel72 = jLabel72;
    }

    public void setjLabel73(JLabel jLabel73) {
        this.jLabel73 = jLabel73;
    }

    public void setjLabel74(JLabel jLabel74) {
        this.jLabel74 = jLabel74;
    }

    public void setjLabel75(JLabel jLabel75) {
        this.jLabel75 = jLabel75;
    }

    public void setjLabel76(JLabel jLabel76) {
        this.jLabel76 = jLabel76;
    }

    public void setjLabel77(JLabel jLabel77) {
        this.jLabel77 = jLabel77;
    }

    public void setjLabel78(JLabel jLabel78) {
        this.jLabel78 = jLabel78;
    }

    public void setjLabel79(JLabel jLabel79) {
        this.jLabel79 = jLabel79;
    }

    public void setjLabel8(JLabel jLabel8) {
        this.jLabel8 = jLabel8;
    }

    public void setjLabel80(JLabel jLabel80) {
        this.jLabel80 = jLabel80;
    }

    public void setjLabel81(JLabel jLabel81) {
        this.jLabel81 = jLabel81;
    }

    public void setjLabel82(JLabel jLabel82) {
        this.jLabel82 = jLabel82;
    }

    public void setjLabel83(JLabel jLabel83) {
        this.jLabel83 = jLabel83;
    }

    public void setjLabel84(JLabel jLabel84) {
        this.jLabel84 = jLabel84;
    }

    public void setjLabel85(JLabel jLabel85) {
        this.jLabel85 = jLabel85;
    }

    public void setjLabel86(JLabel jLabel86) {
        this.jLabel86 = jLabel86;
    }

    public void setjLabel87(JLabel jLabel87) {
        this.jLabel87 = jLabel87;
    }

    public void setjLabel9(JLabel jLabel9) {
        this.jLabel9 = jLabel9;
    }

    public void setjPanel1(JPanel jPanel1) {
        this.jPanel1 = jPanel1;
    }

    public void setjPanel10(JPanel jPanel10) {
        this.jPanel10 = jPanel10;
    }

    public void setjPanel11(JPanel jPanel11) {
        this.jPanel11 = jPanel11;
    }

    public void setjPanel12(JPanel jPanel12) {
        this.jPanel12 = jPanel12;
    }

    public void setjPanel13(JPanel jPanel13) {
        this.jPanel13 = jPanel13;
    }

    public void setjPanel14(JPanel jPanel14) {
        this.jPanel14 = jPanel14;
    }

    public void setjPanel15(JPanel jPanel15) {
        this.jPanel15 = jPanel15;
    }

    public void setjPanel16(JPanel jPanel16) {
        this.jPanel16 = jPanel16;
    }

    public void setjPanel17(JPanel jPanel17) {
        this.jPanel17 = jPanel17;
    }

    public void setjPanel18(JPanel jPanel18) {
        this.jPanel18 = jPanel18;
    }

    public void setjPanel19(JPanel jPanel19) {
        this.jPanel19 = jPanel19;
    }

    public void setjPanel2(JPanel jPanel2) {
        this.jPanel2 = jPanel2;
    }

    public void setjPanel20(JPanel jPanel20) {
        this.jPanel20 = jPanel20;
    }

    public void setjPanel21(JPanel jPanel21) {
        this.jPanel21 = jPanel21;
    }

    public void setjPanel22(JPanel jPanel22) {
        this.jPanel22 = jPanel22;
    }

    public void setjPanel23(JPanel jPanel23) {
        this.jPanel23 = jPanel23;
    }

    public void setjPanel24(JPanel jPanel24) {
        this.jPanel24 = jPanel24;
    }

    public void setjPanel25(JPanel jPanel25) {
        this.jPanel25 = jPanel25;
    }

    public void setjPanel26(JPanel jPanel26) {
        this.jPanel26 = jPanel26;
    }

    public void setjPanel27(JPanel jPanel27) {
        this.jPanel27 = jPanel27;
    }

    public void setjPanel28(JPanel jPanel28) {
        this.jPanel28 = jPanel28;
    }

    public void setjPanel3(JPanel jPanel3) {
        this.jPanel3 = jPanel3;
    }

    public void setjPanel4(JPanel jPanel4) {
        this.jPanel4 = jPanel4;
    }

    public void setjPanel5(JPanel jPanel5) {
        this.jPanel5 = jPanel5;
    }

    public void setjPanel6(JPanel jPanel6) {
        this.jPanel6 = jPanel6;
    }

    public void setjPanel7(JPanel jPanel7) {
        this.jPanel7 = jPanel7;
    }

    public void setjPanel8(JPanel jPanel8) {
        this.jPanel8 = jPanel8;
    }

    public void setjPanel9_ok(JPanel jPanel9_ok) {
        this.jPanel9_ok = jPanel9_ok;
    }

    public void setjScrollPane2(JScrollPane jScrollPane2) {
        this.jScrollPane2 = jScrollPane2;
    }

    public JPanel getDownPanel() {
        return DownPanel;
    }

    public JPanel getMainPanel() {
        return MainPanel;
    }

    public JPanel getNamePanel_01() {
        return NamePanel_01;
    }

    public JPanel getNamePanel_02() {
        return NamePanel_02;
    }

    public JPanel getNamePanel_03() {
        return NamePanel_03;
    }

    public JPanel getNamePanel_04() {
        return NamePanel_04;
    }

    public JPanel getNamePanel_05() {
        return NamePanel_05;
    }

    public JPanel getNamePanel_06() {
        return NamePanel_06;
    }

    public JPanel getNamePanel_07() {
        return NamePanel_07;
    }

    public JPanel getNamePanel_08() {
        return NamePanel_08;
    }

    public JPanel getNamePanel_09() {
        return NamePanel_09;
    }

    public JPanel getNamePanel_11() {
        return NamePanel_11;
    }

    public JPanel getNamePanel_12() {
        return NamePanel_12;
    }

    public JPanel getNamePanel_13() {
        return NamePanel_13;
    }

    public JPanel getNamePanel_14() {
        return NamePanel_14;
    }

    public JPanel getNamePanel_15() {
        return NamePanel_15;
    }

    public JPanel getNamePanel_16() {
        return NamePanel_16;
    }

    public JPanel getNamePanel_17() {
        return NamePanel_17;
    }

    public JPanel getNamePanel_18() {
        return NamePanel_18;
    }

    public JPanel getNamePanel_19() {
        return NamePanel_19;
    }

    public JPanel getNamePanel_21() {
        return NamePanel_21;
    }

    public JPanel getNamePanel_22() {
        return NamePanel_22;
    }

    public JPanel getNamePanel_23() {
        return NamePanel_23;
    }

    public JPanel getNamePanel_24() {
        return NamePanel_24;
    }

    public JPanel getNamePanel_25() {
        return NamePanel_25;
    }

    public JPanel getNamePanel_26() {
        return NamePanel_26;
    }

    public JPanel getNamePanel_27() {
        return NamePanel_27;
    }

    public JPanel getNamePanel_28() {
        return NamePanel_28;
    }

    public JPanel getNamePanel_29() {
        return NamePanel_29;
    }

    public JPanel getNamePanel_31() {
        return NamePanel_31;
    }

    public JPanel getNamePanel_32() {
        return NamePanel_32;
    }

    public JPanel getNamePanel_33() {
        return NamePanel_33;
    }

    public JPanel getNamePanel_34() {
        return NamePanel_34;
    }

    public JPanel getNamePanel_35() {
        return NamePanel_35;
    }

    public JPanel getNamePanel_36() {
        return NamePanel_36;
    }

    public JPanel getNamePanel_37() {
        return NamePanel_37;
    }

    public JPanel getNamePanel_38() {
        return NamePanel_38;
    }

    public JPanel getNamePanel_39() {
        return NamePanel_39;
    }

    public JPanel getPlayer0001() {
        return Player0001;
    }

    public JPanel getPlayer0002() {
        return Player0002;
    }

    public JPanel getPlayer0003() {
        return Player0003;
    }

    public JPanel getPlayer0004() {
        return Player0004;
    }

    public JPanel getPlayer0005() {
        return Player0005;
    }

    public JPanel getPlayer0006() {
        return Player0006;
    }

    public JPanel getPlayer0101() {
        return Player0101;
    }

    public JPanel getPlayer0102() {
        return Player0102;
    }

    public JPanel getPlayer0103() {
        return Player0103;
    }

    public JPanel getPlayer0104() {
        return Player0104;
    }

    public JPanel getPlayer0105() {
        return Player0105;
    }

    public JPanel getPlayer0106() {
        return Player0106;
    }

    public JPanel getPlayer0201() {
        return Player0201;
    }

    public JPanel getPlayer0202() {
        return Player0202;
    }

    public JPanel getPlayer0203() {
        return Player0203;
    }

    public JPanel getPlayer0204() {
        return Player0204;
    }

    public JPanel getPlayer0205() {
        return Player0205;
    }

    public JPanel getPlayer0206() {
        return Player0206;
    }

    public JPanel getPlayer0301() {
        return Player0301;
    }

    public JPanel getPlayer0302() {
        return Player0302;
    }

    public JPanel getPlayer0303() {
        return Player0303;
    }

    public JPanel getPlayer0304() {
        return Player0304;
    }

    public JPanel getPlayer0305() {
        return Player0305;
    }

    public JPanel getPlayer0306() {
        return Player0306;
    }

    public JPanel getPlayer0401() {
        return Player0401;
    }

    public JPanel getPlayer0402() {
        return Player0402;
    }

    public JPanel getPlayer0403() {
        return Player0403;
    }

    public JPanel getPlayer0404() {
        return Player0404;
    }

    public JPanel getPlayer0405() {
        return Player0405;
    }

    public JPanel getPlayer0406() {
        return Player0406;
    }

    public JPanel getPlayer0501() {
        return Player0501;
    }

    public JPanel getPlayer0502() {
        return Player0502;
    }

    public JPanel getPlayer0503() {
        return Player0503;
    }

    public JPanel getPlayer0504() {
        return Player0504;
    }

    public JPanel getPlayer0505() {
        return Player0505;
    }

    public JPanel getPlayer0506() {
        return Player0506;
    }

    public JPanel getPlayer0601() {
        return Player0601;
    }

    public JPanel getPlayer0602() {
        return Player0602;
    }

    public JPanel getPlayer0603() {
        return Player0603;
    }

    public JPanel getPlayer0604() {
        return Player0604;
    }

    public JPanel getPlayer0605() {
        return Player0605;
    }

    public JPanel getPlayer0606() {
        return Player0606;
    }

    public JPanel getPlayer0701() {
        return Player0701;
    }

    public JPanel getPlayer0702() {
        return Player0702;
    }

    public JPanel getPlayer0703() {
        return Player0703;
    }

    public JPanel getPlayer0704() {
        return Player0704;
    }

    public JPanel getPlayer0705() {
        return Player0705;
    }

    public JPanel getPlayer0706() {
        return Player0706;
    }

    public JPanel getPlayer0801() {
        return Player0801;
    }

    public JPanel getPlayer0802() {
        return Player0802;
    }

    public JPanel getPlayer0803() {
        return Player0803;
    }

    public JPanel getPlayer0804() {
        return Player0804;
    }

    public JPanel getPlayer0805() {
        return Player0805;
    }

    public JPanel getPlayer0806() {
        return Player0806;
    }

    public JPanel getPlayer0901() {
        return Player0901;
    }

    public JPanel getPlayer0902() {
        return Player0902;
    }

    public JPanel getPlayer0903() {
        return Player0903;
    }

    public JPanel getPlayer0904() {
        return Player0904;
    }

    public JPanel getPlayer0905() {
        return Player0905;
    }

    public JPanel getPlayer0906() {
        return Player0906;
    }

    public JPanel getPlayer1() {
        return Player1;
    }

    public JPanel getPlayer1001() {
        return Player1001;
    }

    public JPanel getPlayer1002() {
        return Player1002;
    }

    public JPanel getPlayer1003() {
        return Player1003;
    }

    public JPanel getPlayer1004() {
        return Player1004;
    }

    public JPanel getPlayer1005() {
        return Player1005;
    }

    public JPanel getPlayer1006() {
        return Player1006;
    }

    public JPanel getPlayer1101() {
        return Player1101;
    }

    public JPanel getPlayer1102() {
        return Player1102;
    }

    public JPanel getPlayer1103() {
        return Player1103;
    }

    public JPanel getPlayer1104() {
        return Player1104;
    }

    public JPanel getPlayer1105() {
        return Player1105;
    }

    public JPanel getPlayer1106() {
        return Player1106;
    }

    public JPanel getPlayer1201() {
        return Player1201;
    }

    public JPanel getPlayer1202() {
        return Player1202;
    }

    public JPanel getPlayer1203() {
        return Player1203;
    }

    public JPanel getPlayer1204() {
        return Player1204;
    }

    public JPanel getPlayer1205() {
        return Player1205;
    }

    public JPanel getPlayer1206() {
        return Player1206;
    }

    public JPanel getPlayer1301() {
        return Player1301;
    }

    public JPanel getPlayer1302() {
        return Player1302;
    }

    public JPanel getPlayer1303() {
        return Player1303;
    }

    public JPanel getPlayer1304() {
        return Player1304;
    }

    public JPanel getPlayer1305() {
        return Player1305;
    }

    public JPanel getPlayer1306() {
        return Player1306;
    }

    public JPanel getPlayer1401() {
        return Player1401;
    }

    public JPanel getPlayer1402() {
        return Player1402;
    }

    public JPanel getPlayer1403() {
        return Player1403;
    }

    public JPanel getPlayer1404() {
        return Player1404;
    }

    public JPanel getPlayer1405() {
        return Player1405;
    }

    public JPanel getPlayer1406() {
        return Player1406;
    }

    public JPanel getPlayer1501() {
        return Player1501;
    }

    public JPanel getPlayer1502() {
        return Player1502;
    }

    public JPanel getPlayer1503() {
        return Player1503;
    }

    public JPanel getPlayer1504() {
        return Player1504;
    }

    public JPanel getPlayer1505() {
        return Player1505;
    }

    public JPanel getPlayer1506() {
        return Player1506;
    }

    public JPanel getPlayer1601() {
        return Player1601;
    }

    public JPanel getPlayer1602() {
        return Player1602;
    }

    public JPanel getPlayer1603() {
        return Player1603;
    }

    public JPanel getPlayer1604() {
        return Player1604;
    }

    public JPanel getPlayer1605() {
        return Player1605;
    }

    public JPanel getPlayer1606() {
        return Player1606;
    }

    public JPanel getPlayer1701() {
        return Player1701;
    }

    public JPanel getPlayer1702() {
        return Player1702;
    }

    public JPanel getPlayer1703() {
        return Player1703;
    }

    public JPanel getPlayer1704() {
        return Player1704;
    }

    public JPanel getPlayer1705() {
        return Player1705;
    }

    public JPanel getPlayer1706() {
        return Player1706;
    }

    public JPanel getPlayer1801() {
        return Player1801;
    }

    public JPanel getPlayer1802() {
        return Player1802;
    }

    public JPanel getPlayer1803() {
        return Player1803;
    }

    public JPanel getPlayer1804() {
        return Player1804;
    }

    public JPanel getPlayer1805() {
        return Player1805;
    }

    public JPanel getPlayer1806() {
        return Player1806;
    }

    public JPanel getPlayer1901() {
        return Player1901;
    }

    public JPanel getPlayer1902() {
        return Player1902;
    }

    public JPanel getPlayer1903() {
        return Player1903;
    }

    public JPanel getPlayer1904() {
        return Player1904;
    }

    public JPanel getPlayer1905() {
        return Player1905;
    }

    public JPanel getPlayer1906() {
        return Player1906;
    }

    public JLabel getPlayer1_Balance() {
        return Player1_Balance;
    }

    public JLabel getPlayer1_BalanceD() {
        return Player1_BalanceD;
    }

    public JPanel getPlayer1_Color() {
        return Player1_Color;
    }

    public JLabel getPlayer1_InJail() {
        return Player1_InJail;
    }

    public JLabel getPlayer1_InJailD() {
        return Player1_InJailD;
    }

    public JLabel getPlayer1_Name() {
        return Player1_Name;
    }

    public JLabel getPlayer1_NameD() {
        return Player1_NameD;
    }

    public JPanel getPlayer2001() {
        return Player2001;
    }

    public JPanel getPlayer2002() {
        return Player2002;
    }

    public JPanel getPlayer2003() {
        return Player2003;
    }

    public JPanel getPlayer2004() {
        return Player2004;
    }

    public JPanel getPlayer2005() {
        return Player2005;
    }

    public JPanel getPlayer2006() {
        return Player2006;
    }

    public JPanel getPlayer2101() {
        return Player2101;
    }

    public JPanel getPlayer2102() {
        return Player2102;
    }

    public JPanel getPlayer2103() {
        return Player2103;
    }

    public JPanel getPlayer2104() {
        return Player2104;
    }

    public JPanel getPlayer2105() {
        return Player2105;
    }

    public JPanel getPlayer2106() {
        return Player2106;
    }

    public JPanel getPlayer2201() {
        return Player2201;
    }

    public JPanel getPlayer2202() {
        return Player2202;
    }

    public JPanel getPlayer2203() {
        return Player2203;
    }

    public JPanel getPlayer2204() {
        return Player2204;
    }

    public JPanel getPlayer2205() {
        return Player2205;
    }

    public JPanel getPlayer2206() {
        return Player2206;
    }

    public JPanel getPlayer2301() {
        return Player2301;
    }

    public JPanel getPlayer2302() {
        return Player2302;
    }

    public JPanel getPlayer2303() {
        return Player2303;
    }

    public JPanel getPlayer2304() {
        return Player2304;
    }

    public JPanel getPlayer2305() {
        return Player2305;
    }

    public JPanel getPlayer2306() {
        return Player2306;
    }

    public JPanel getPlayer2401() {
        return Player2401;
    }

    public JPanel getPlayer2402() {
        return Player2402;
    }

    public JPanel getPlayer2403() {
        return Player2403;
    }

    public JPanel getPlayer2404() {
        return Player2404;
    }

    public JPanel getPlayer2405() {
        return Player2405;
    }

    public JPanel getPlayer2406() {
        return Player2406;
    }

    public JPanel getPlayer2501() {
        return Player2501;
    }

    public JPanel getPlayer2502() {
        return Player2502;
    }

    public JPanel getPlayer2503() {
        return Player2503;
    }

    public JPanel getPlayer2504() {
        return Player2504;
    }

    public JPanel getPlayer2505() {
        return Player2505;
    }

    public JPanel getPlayer2506() {
        return Player2506;
    }

    public JPanel getPlayer2601() {
        return Player2601;
    }

    public JPanel getPlayer2602() {
        return Player2602;
    }

    public JPanel getPlayer2603() {
        return Player2603;
    }

    public JPanel getPlayer2604() {
        return Player2604;
    }

    public JPanel getPlayer2605() {
        return Player2605;
    }

    public JPanel getPlayer2606() {
        return Player2606;
    }

    public JPanel getPlayer2701() {
        return Player2701;
    }

    public JPanel getPlayer2702() {
        return Player2702;
    }

    public JPanel getPlayer2703() {
        return Player2703;
    }

    public JPanel getPlayer2704() {
        return Player2704;
    }

    public JPanel getPlayer2705() {
        return Player2705;
    }

    public JPanel getPlayer2706() {
        return Player2706;
    }

    public JPanel getPlayer2801() {
        return Player2801;
    }

    public JPanel getPlayer2802() {
        return Player2802;
    }

    public JPanel getPlayer2803() {
        return Player2803;
    }

    public JPanel getPlayer2804() {
        return Player2804;
    }

    public JPanel getPlayer2805() {
        return Player2805;
    }

    public JPanel getPlayer2806() {
        return Player2806;
    }

    public JPanel getPlayer2901() {
        return Player2901;
    }

    public JPanel getPlayer2902() {
        return Player2902;
    }

    public JPanel getPlayer2903() {
        return Player2903;
    }

    public JPanel getPlayer2904() {
        return Player2904;
    }

    public JPanel getPlayer2905() {
        return Player2905;
    }

    public JPanel getPlayer2906() {
        return Player2906;
    }

    public JPanel getPlayer3001() {
        return Player3001;
    }

    public JPanel getPlayer3002() {
        return Player3002;
    }

    public JPanel getPlayer3003() {
        return Player3003;
    }

    public JPanel getPlayer3004() {
        return Player3004;
    }

    public JPanel getPlayer3005() {
        return Player3005;
    }

    public JPanel getPlayer3006() {
        return Player3006;
    }

    public JPanel getPlayer3101() {
        return Player3101;
    }

    public JPanel getPlayer3102() {
        return Player3102;
    }

    public JPanel getPlayer3103() {
        return Player3103;
    }

    public JPanel getPlayer3104() {
        return Player3104;
    }

    public JPanel getPlayer3105() {
        return Player3105;
    }

    public JPanel getPlayer3106() {
        return Player3106;
    }

    public JPanel getPlayer3201() {
        return Player3201;
    }

    public JPanel getPlayer3202() {
        return Player3202;
    }

    public JPanel getPlayer3203() {
        return Player3203;
    }

    public JPanel getPlayer3204() {
        return Player3204;
    }

    public JPanel getPlayer3205() {
        return Player3205;
    }

    public JPanel getPlayer3206() {
        return Player3206;
    }

    public JPanel getPlayer3301() {
        return Player3301;
    }

    public JPanel getPlayer3302() {
        return Player3302;
    }

    public JPanel getPlayer3303() {
        return Player3303;
    }

    public JPanel getPlayer3304() {
        return Player3304;
    }

    public JPanel getPlayer3305() {
        return Player3305;
    }

    public JPanel getPlayer3306() {
        return Player3306;
    }

    public JPanel getPlayer3401() {
        return Player3401;
    }

    public JPanel getPlayer3402() {
        return Player3402;
    }

    public JPanel getPlayer3403() {
        return Player3403;
    }

    public JPanel getPlayer3404() {
        return Player3404;
    }

    public JPanel getPlayer3405() {
        return Player3405;
    }

    public JPanel getPlayer3406() {
        return Player3406;
    }

    public JPanel getPlayer3501() {
        return Player3501;
    }

    public JPanel getPlayer3502() {
        return Player3502;
    }

    public JPanel getPlayer3503() {
        return Player3503;
    }

    public JPanel getPlayer3504() {
        return Player3504;
    }

    public JPanel getPlayer3505() {
        return Player3505;
    }

    public JPanel getPlayer3506() {
        return Player3506;
    }

    public JPanel getPlayer3601() {
        return Player3601;
    }

    public JPanel getPlayer3602() {
        return Player3602;
    }

    public JPanel getPlayer3603() {
        return Player3603;
    }

    public JPanel getPlayer3604() {
        return Player3604;
    }

    public JPanel getPlayer3605() {
        return Player3605;
    }

    public JPanel getPlayer3606() {
        return Player3606;
    }

    public JPanel getPlayer3701() {
        return Player3701;
    }

    public JPanel getPlayer3702() {
        return Player3702;
    }

    public JPanel getPlayer3703() {
        return Player3703;
    }

    public JPanel getPlayer3704() {
        return Player3704;
    }

    public JPanel getPlayer3705() {
        return Player3705;
    }

    public JPanel getPlayer3706() {
        return Player3706;
    }

    public JPanel getPlayer3801() {
        return Player3801;
    }

    public JPanel getPlayer3802() {
        return Player3802;
    }

    public JPanel getPlayer3803() {
        return Player3803;
    }

    public JPanel getPlayer3804() {
        return Player3804;
    }

    public JPanel getPlayer3805() {
        return Player3805;
    }

    public JPanel getPlayer3806() {
        return Player3806;
    }

    public JPanel getPlayer3901() {
        return Player3901;
    }

    public JPanel getPlayer3902() {
        return Player3902;
    }

    public JPanel getPlayer3903() {
        return Player3903;
    }

    public JPanel getPlayer3904() {
        return Player3904;
    }

    public JPanel getPlayer3905() {
        return Player3905;
    }

    public JPanel getPlayer3906() {
        return Player3906;
    }

    public JPanel getSidePanel() {
        return SidePanel;
    }

    public JLabel getTileName_01() {
        return TileName_01;
    }

    public JLabel getTileName_02() {
        return TileName_02;
    }

    public JLabel getTileName_03() {
        return TileName_03;
    }

    public JLabel getTileName_04() {
        return TileName_04;
    }

    public JLabel getTileName_05() {
        return TileName_05;
    }

    public JLabel getTileName_06() {
        return TileName_06;
    }

    public JLabel getTileName_07() {
        return TileName_07;
    }

    public JLabel getTileName_08() {
        return TileName_08;
    }

    public JLabel getTileName_09() {
        return TileName_09;
    }

    public JLabel getTileName_11() {
        return TileName_11;
    }

    public JLabel getTileName_12() {
        return TileName_12;
    }

    public JLabel getTileName_13() {
        return TileName_13;
    }

    public JLabel getTileName_14() {
        return TileName_14;
    }

    public JLabel getTileName_15() {
        return TileName_15;
    }

    public JLabel getTileName_16() {
        return TileName_16;
    }

    public JLabel getTileName_17() {
        return TileName_17;
    }

    public JLabel getTileName_18() {
        return TileName_18;
    }

    public JLabel getTileName_19() {
        return TileName_19;
    }

    public JLabel getTileName_21() {
        return TileName_21;
    }

    public JLabel getTileName_22() {
        return TileName_22;
    }

    public JLabel getTileName_23() {
        return TileName_23;
    }

    public JLabel getTileName_24() {
        return TileName_24;
    }

    public JLabel getTileName_25() {
        return TileName_25;
    }

    public JLabel getTileName_26() {
        return TileName_26;
    }

    public JLabel getTileName_27() {
        return TileName_27;
    }

    public JLabel getTileName_28() {
        return TileName_28;
    }

    public JLabel getTileName_29() {
        return TileName_29;
    }

    public JLabel getTileName_31() {
        return TileName_31;
    }

    public JLabel getTileName_32() {
        return TileName_32;
    }

    public JLabel getTileName_33() {
        return TileName_33;
    }

    public JLabel getTileName_34() {
        return TileName_34;
    }

    public JLabel getTileName_35() {
        return TileName_35;
    }

    public JLabel getTileName_36() {
        return TileName_36;
    }

    public JLabel getTileName_37() {
        return TileName_37;
    }

    public JLabel getTileName_38() {
        return TileName_38;
    }

    public JLabel getTileName_39() {
        return TileName_39;
    }

    public JPanel getTile_00() {
        return Tile_00;
    }

    public JPanel getTile_01() {
        return Tile_01;
    }

    public JPanel getTile_02() {
        return Tile_02;
    }

    public JPanel getTile_03() {
        return Tile_03;
    }

    public JPanel getTile_04() {
        return Tile_04;
    }

    public JPanel getTile_05() {
        return Tile_05;
    }

    public JPanel getTile_06() {
        return Tile_06;
    }

    public JPanel getTile_07() {
        return Tile_07;
    }

    public JPanel getTile_08() {
        return Tile_08;
    }

    public JPanel getTile_09() {
        return Tile_09;
    }

    public JPanel getTile_10() {
        return Tile_10;
    }

    public JPanel getTile_11() {
        return Tile_11;
    }

    public JPanel getTile_12() {
        return Tile_12;
    }

    public JPanel getTile_13() {
        return Tile_13;
    }

    public JPanel getTile_14() {
        return Tile_14;
    }

    public JPanel getTile_15() {
        return Tile_15;
    }

    public JPanel getTile_16() {
        return Tile_16;
    }

    public JPanel getTile_17() {
        return Tile_17;
    }

    public JPanel getTile_18() {
        return Tile_18;
    }

    public JPanel getTile_19() {
        return Tile_19;
    }

    public JPanel getTile_20() {
        return Tile_20;
    }

    public JPanel getTile_21() {
        return Tile_21;
    }

    public JPanel getTile_22() {
        return Tile_22;
    }

    public JPanel getTile_23() {
        return Tile_23;
    }

    public JPanel getTile_24() {
        return Tile_24;
    }

    public JPanel getTile_25() {
        return Tile_25;
    }

    public JPanel getTile_26() {
        return Tile_26;
    }

    public JPanel getTile_27() {
        return Tile_27;
    }

    public JPanel getTile_28() {
        return Tile_28;
    }

    public JPanel getTile_29() {
        return Tile_29;
    }

    public JPanel getTile_30() {
        return Tile_30;
    }

    public JPanel getTile_31() {
        return Tile_31;
    }

    public JPanel getTile_32() {
        return Tile_32;
    }

    public JPanel getTile_33() {
        return Tile_33;
    }

    public JPanel getTile_34() {
        return Tile_34;
    }

    public JPanel getTile_35() {
        return Tile_35;
    }

    public JPanel getTile_36() {
        return Tile_36;
    }

    public JPanel getTile_37() {
        return Tile_37;
    }

    public JPanel getTile_38() {
        return Tile_38;
    }

    public JPanel getTile_39() {
        return Tile_39;
    }

    public JPanel getInsied_area() {
        return insied_area;
    }

    public JButton getjButton1() {
        return jButton1;
    }

    public JButton getjButton2() {
        return jButton2;
    }

    public JButton getjButton3() {
        return jButton3;
    }

    public JButton getjButton4() {
        return jButton4;
    }

    public JButton getjButton5() {
        return jButton5;
    }

    public JButton getjButton6() {
        return jButton6;
    }

    public JComboBox<String> getjComboBox1() {
        return jComboBox1;
    }

    public JLabel getjLabel1() {
        return jLabel1;
    }

    public JLabel getjLabel10() {
        return jLabel10;
    }

    public JLabel getjLabel11() {
        return jLabel11;
    }

    public JLabel getjLabel12() {
        return jLabel12;
    }

    public JLabel getjLabel13() {
        return jLabel13;
    }

    public JLabel getjLabel14() {
        return jLabel14;
    }

    public JLabel getjLabel15() {
        return jLabel15;
    }

    public JLabel getjLabel16() {
        return jLabel16;
    }

    public JLabel getjLabel17() {
        return jLabel17;
    }

    public JLabel getjLabel18() {
        return jLabel18;
    }

    public JLabel getjLabel19() {
        return jLabel19;
    }

    public JLabel getjLabel2() {
        return jLabel2;
    }

    public JLabel getjLabel20() {
        return jLabel20;
    }

    public JLabel getjLabel21() {
        return jLabel21;
    }

    public JLabel getjLabel22() {
        return jLabel22;
    }

    public JLabel getjLabel23() {
        return jLabel23;
    }

    public JLabel getjLabel24() {
        return jLabel24;
    }

    public JLabel getjLabel25() {
        return jLabel25;
    }

    public JLabel getjLabel28() {
        return jLabel28;
    }

    public JLabel getjLabel29() {
        return jLabel29;
    }

    public JLabel getjLabel3() {
        return jLabel3;
    }

    public JLabel getjLabel30() {
        return jLabel30;
    }

    public JLabel getjLabel31() {
        return jLabel31;
    }

    public JLabel getjLabel32() {
        return jLabel32;
    }

    public JLabel getjLabel33() {
        return jLabel33;
    }

    public JLabel getjLabel34() {
        return jLabel34;
    }

    public JLabel getjLabel35() {
        return jLabel35;
    }

    public JLabel getjLabel36() {
        return jLabel36;
    }

    public JLabel getjLabel37() {
        return jLabel37;
    }

    public JLabel getjLabel38() {
        return jLabel38;
    }

    public JLabel getjLabel39() {
        return jLabel39;
    }

    public JLabel getjLabel4() {
        return jLabel4;
    }

    public JLabel getjLabel40() {
        return jLabel40;
    }

    public JLabel getjLabel41() {
        return jLabel41;
    }

    public JLabel getjLabel42() {
        return jLabel42;
    }

    public JLabel getjLabel43() {
        return jLabel43;
    }

    public JLabel getjLabel44() {
        return jLabel44;
    }

    public JLabel getjLabel45() {
        return jLabel45;
    }

    public JLabel getjLabel46() {
        return jLabel46;
    }

    public JLabel getjLabel47() {
        return jLabel47;
    }

    public JLabel getjLabel48() {
        return jLabel48;
    }

    public JLabel getjLabel49() {
        return jLabel49;
    }

    public JLabel getjLabel5() {
        return jLabel5;
    }

    public JLabel getjLabel50() {
        return jLabel50;
    }

    public JLabel getjLabel51() {
        return jLabel51;
    }

    public JLabel getjLabel52() {
        return jLabel52;
    }

    public JLabel getjLabel53() {
        return jLabel53;
    }

    public JLabel getjLabel54() {
        return jLabel54;
    }

    public JLabel getjLabel55() {
        return jLabel55;
    }

    public JLabel getjLabel56() {
        return jLabel56;
    }

    public JLabel getjLabel57() {
        return jLabel57;
    }

    public JLabel getjLabel58() {
        return jLabel58;
    }

    public JLabel getjLabel59() {
        return jLabel59;
    }

    public JLabel getjLabel6() {
        return jLabel6;
    }

    public JLabel getjLabel60() {
        return jLabel60;
    }

    public JLabel getjLabel61() {
        return jLabel61;
    }

    public JLabel getjLabel62() {
        return jLabel62;
    }

    public JLabel getjLabel63() {
        return jLabel63;
    }

    public JLabel getjLabel64() {
        return jLabel64;
    }

    public JLabel getjLabel65() {
        return jLabel65;
    }

    public JLabel getjLabel66() {
        return jLabel66;
    }

    public JLabel getjLabel67() {
        return jLabel67;
    }

    public JLabel getjLabel68() {
        return jLabel68;
    }

    public JLabel getjLabel69() {
        return jLabel69;
    }

    public JLabel getjLabel7() {
        return jLabel7;
    }

    public JLabel getjLabel70() {
        return jLabel70;
    }

    public JLabel getjLabel71() {
        return jLabel71;
    }

    public JLabel getjLabel72() {
        return jLabel72;
    }

    public JLabel getjLabel73() {
        return jLabel73;
    }

    public JLabel getjLabel74() {
        return jLabel74;
    }

    public JLabel getjLabel75() {
        return jLabel75;
    }

    public JLabel getjLabel76() {
        return jLabel76;
    }

    public JLabel getjLabel77() {
        return jLabel77;
    }

    public JLabel getjLabel78() {
        return jLabel78;
    }

    public JLabel getjLabel79() {
        return jLabel79;
    }

    public JLabel getjLabel8() {
        return jLabel8;
    }

    public JLabel getjLabel80() {
        return jLabel80;
    }

    public JLabel getjLabel81() {
        return jLabel81;
    }

    public JLabel getjLabel82() {
        return jLabel82;
    }

    public JLabel getjLabel83() {
        return jLabel83;
    }

    public JLabel getjLabel84() {
        return jLabel84;
    }

    public JLabel getjLabel85() {
        return jLabel85;
    }

    public JLabel getjLabel86() {
        return jLabel86;
    }

    public JLabel getjLabel87() {
        return jLabel87;
    }

    public JLabel getjLabel9() {
        return jLabel9;
    }

    public JPanel getjPanel1() {
        return jPanel1;
    }

    public JPanel getjPanel10() {
        return jPanel10;
    }

    public JPanel getjPanel11() {
        return jPanel11;
    }

    public JPanel getjPanel12() {
        return jPanel12;
    }

    public JPanel getjPanel13() {
        return jPanel13;
    }

    public JPanel getjPanel14() {
        return jPanel14;
    }

    public JPanel getjPanel15() {
        return jPanel15;
    }

    public JPanel getjPanel16() {
        return jPanel16;
    }

    public JPanel getjPanel17() {
        return jPanel17;
    }

    public JPanel getjPanel18() {
        return jPanel18;
    }

    public JPanel getjPanel19() {
        return jPanel19;
    }

    public JPanel getjPanel2() {
        return jPanel2;
    }

    public JPanel getjPanel20() {
        return jPanel20;
    }

    public JPanel getjPanel21() {
        return jPanel21;
    }

    public JPanel getjPanel22() {
        return jPanel22;
    }

    public JPanel getjPanel23() {
        return jPanel23;
    }

    public JPanel getjPanel24() {
        return jPanel24;
    }

    public JPanel getjPanel25() {
        return jPanel25;
    }

    public JPanel getjPanel26() {
        return jPanel26;
    }

    public JPanel getjPanel27() {
        return jPanel27;
    }

    public JPanel getjPanel28() {
        return jPanel28;
    }

    public JPanel getjPanel3() {
        return jPanel3;
    }

    public JPanel getjPanel4() {
        return jPanel4;
    }

    public JPanel getjPanel5() {
        return jPanel5;
    }

    public JPanel getjPanel6() {
        return jPanel6;
    }

    public JPanel getjPanel7() {
        return jPanel7;
    }

    public JPanel getjPanel8() {
        return jPanel8;
    }

    public JPanel getjPanel9_ok() {
        return jPanel9_ok;
    }

    public JScrollPane getjScrollPane2() {
        return jScrollPane2;
    }

    public static int getEXIT_ON_CLOSE() {
        return EXIT_ON_CLOSE;
    }

    public JRootPane getRootPane() {
        return rootPane;
    }

    public boolean isRootPaneCheckingEnabled() {
        return rootPaneCheckingEnabled;
    }

    public AccessibleContext getAccessibleContext() {
        return accessibleContext;
    }

    public static int getDEFAULT_CURSOR() {
        return DEFAULT_CURSOR;
    }

    public static int getCROSSHAIR_CURSOR() {
        return CROSSHAIR_CURSOR;
    }

    public static int getTEXT_CURSOR() {
        return TEXT_CURSOR;
    }

    public static int getWAIT_CURSOR() {
        return WAIT_CURSOR;
    }

    public static int getSW_RESIZE_CURSOR() {
        return SW_RESIZE_CURSOR;
    }

    public static int getSE_RESIZE_CURSOR() {
        return SE_RESIZE_CURSOR;
    }

    public static int getNW_RESIZE_CURSOR() {
        return NW_RESIZE_CURSOR;
    }

    public static int getNE_RESIZE_CURSOR() {
        return NE_RESIZE_CURSOR;
    }

    public static int getN_RESIZE_CURSOR() {
        return N_RESIZE_CURSOR;
    }

    public static int getS_RESIZE_CURSOR() {
        return S_RESIZE_CURSOR;
    }

    public static int getW_RESIZE_CURSOR() {
        return W_RESIZE_CURSOR;
    }

    public static int getE_RESIZE_CURSOR() {
        return E_RESIZE_CURSOR;
    }

    public static int getHAND_CURSOR() {
        return HAND_CURSOR;
    }

    public static int getMOVE_CURSOR() {
        return MOVE_CURSOR;
    }

    public static int getNORMAL() {
        return NORMAL;
    }

    public static int getICONIFIED() {
        return ICONIFIED;
    }

    public static int getMAXIMIZED_HORIZ() {
        return MAXIMIZED_HORIZ;
    }

    public static int getMAXIMIZED_VERT() {
        return MAXIMIZED_VERT;
    }

    public static int getMAXIMIZED_BOTH() {
        return MAXIMIZED_BOTH;
    }

    public static float getTOP_ALIGNMENT() {
        return TOP_ALIGNMENT;
    }

    public static float getCENTER_ALIGNMENT() {
        return CENTER_ALIGNMENT;
    }

    public static float getBOTTOM_ALIGNMENT() {
        return BOTTOM_ALIGNMENT;
    }

    public static float getLEFT_ALIGNMENT() {
        return LEFT_ALIGNMENT;
    }

    public static float getRIGHT_ALIGNMENT() {
        return RIGHT_ALIGNMENT;
    }

    public static int getDO_NOTHING_ON_CLOSE() {
        return DO_NOTHING_ON_CLOSE;
    }

    public static int getHIDE_ON_CLOSE() {
        return HIDE_ON_CLOSE;
    }

    public static int getDISPOSE_ON_CLOSE() {
        return DISPOSE_ON_CLOSE;
    }

    public static int getWIDTH() {
        return WIDTH;
    }

    public static int getHEIGHT() {
        return HEIGHT;
    }

    public static int getPROPERTIES() {
        return PROPERTIES;
    }

    public static int getSOMEBITS() {
        return SOMEBITS;
    }

    public static int getFRAMEBITS() {
        return FRAMEBITS;
    }

    public static int getALLBITS() {
        return ALLBITS;
    }

    public static int getERROR() {
        return ERROR;
    }

    public static int getABORT() {
        return ABORT;
    }

    public JLabel getThrowDiceLabel() {
        return throwDiceLabel;
    }

    public void throwDiceEvent(Player player) {

        System.out.println("player #" + player.getPlayerID() + ".");

        player.throwDice(Monopoly_final.board);
        System.out.println(player.getReThrow());
        if (player.getReThrow() == true && player.getTurnsInJail() == 0) {
            player.setReThrowFalse();
        } else {
            jButton1.setEnabled(false);
            jButton6.setEnabled(true);
        }

        System.out.println("Position on Board: " + player.getPositionOnBoard());
    }
    
    public void updateLabel(String name){
        if(name.equals("Player 1")){
            Monopoly_final.frame.getPlayer1_Name().setText(Monopoly_final.players.get(0).getName());
            Monopoly_final.frame.getPlayer1_Balance().setText("" + Monopoly_final.players.get(0).getBalance());
            Monopoly_final.frame.getPlayer1_Color().setBackground(Color.RED);
            if(Monopoly_final.players.get(0).isInJail()){
                Monopoly_final.frame.getPlayer1_InJail().setText("Yes" + "(" + Monopoly_final.players.get(0).getTurnsInJail() + ")");
            }else{
                Monopoly_final.frame.getPlayer1_InJail().setText("No");
            }
        }
        if(name.equals("Player 2")){
            Monopoly_final.frame.getPlayer1_Name().setText(Monopoly_final.players.get(1).getName());
            Monopoly_final.frame.getPlayer1_Balance().setText("" + Monopoly_final.players.get(1).getBalance());
            Monopoly_final.frame.getPlayer1_Color().setBackground(Color.BLUE);
            if(Monopoly_final.players.get(1).isInJail()){
                Monopoly_final.frame.getPlayer1_InJail().setText("Yes" + "(" + Monopoly_final.players.get(1).getTurnsInJail() + ")");
            }else{
                Monopoly_final.frame.getPlayer1_InJail().setText("No");
            }
        }
        if(name.equals("Player 3")){
            Monopoly_final.frame.getPlayer1_Name().setText(Monopoly_final.players.get(2).getName());
            Monopoly_final.frame.getPlayer1_Balance().setText("" + Monopoly_final.players.get(2).getBalance());
            Monopoly_final.frame.getPlayer1_Color().setBackground(Color.GREEN);
            if(Monopoly_final.players.get(2).isInJail()){
                Monopoly_final.frame.getPlayer1_InJail().setText("Yes" + "(" + Monopoly_final.players.get(2).getTurnsInJail() + ")");
            }else{
                Monopoly_final.frame.getPlayer1_InJail().setText("No");
            }
        }
        if(name.equals("Player 4")){
            Monopoly_final.frame.getPlayer1_Name().setText(Monopoly_final.players.get(3).getName());
            Monopoly_final.frame.getPlayer1_Balance().setText("" + Monopoly_final.players.get(3).getBalance());
            Monopoly_final.frame.getPlayer1_Color().setBackground(Color.YELLOW);
            if(Monopoly_final.players.get(3).isInJail()){
                Monopoly_final.frame.getPlayer1_InJail().setText("Yes" + "(" + Monopoly_final.players.get(3).getTurnsInJail() + ")");
            }else{
                Monopoly_final.frame.getPlayer1_InJail().setText("No");
            }
        }
        if(name.equals("Player 5")){
            Monopoly_final.frame.getPlayer1_Name().setText(Monopoly_final.players.get(4).getName());
            Monopoly_final.frame.getPlayer1_Balance().setText("" + Monopoly_final.players.get(4).getBalance());
            Monopoly_final.frame.getPlayer1_Color().setBackground(Color.MAGENTA);
            if(Monopoly_final.players.get(4).isInJail()){
                Monopoly_final.frame.getPlayer1_InJail().setText("Yes" + "(" + Monopoly_final.players.get(4).getTurnsInJail() + ")");
            }else{
                Monopoly_final.frame.getPlayer1_InJail().setText("No");
            }
        }
        if(name.equals("Player 6")){
            Monopoly_final.frame.getPlayer1_Name().setText(Monopoly_final.players.get(5).getName());
            Monopoly_final.frame.getPlayer1_Balance().setText("" + Monopoly_final.players.get(5).getBalance());
            Monopoly_final.frame.getPlayer1_Color().setBackground(Color.ORANGE);
            if(Monopoly_final.players.get(5).isInJail()){
                Monopoly_final.frame.getPlayer1_InJail().setText("Yes" + "(" + Monopoly_final.players.get(5).getTurnsInJail() + ")");
            }else{
                Monopoly_final.frame.getPlayer1_InJail().setText("No");
            }
        }
    }
    
    public void drawCardEvent(Player player) {

        System.out.println("player #" + player.getPlayerID() + ".");
        System.out.println("money before: " + player.getBalance());
        if(player.getPositionOnBoard() == 7 || player.getPositionOnBoard() == 22 || player.getPositionOnBoard() == 36){
            player.processNextCard(Monopoly_final.board);
        }else{
            player.processNextCommunityChestCard(Monopoly_final.board);
        }
        jButton2.setEnabled(false);
        jButton6.setEnabled(true);

        System.out.println("Position on Board: " + player.getPositionOnBoard());
        System.out.println("money after: " + player.getBalance());
    }
    
    public void buyTileEvent(Player player) {

        System.out.println("player #" + player.getPlayerID() + ".");
        System.out.println("money before: " + player.getBalance());

        player.buyTile(Monopoly_final.board);

        System.out.println("Position on Board: " + player.getPositionOnBoard());
        System.out.println("money after: " + player.getBalance());
    }
    
        public void buildEvent(Player player) {

        System.out.println("player #" + player.getPlayerID() + ".");
        System.out.println("money before: " + player.getBalance());

        player.buildHouse();

        System.out.println("Position on Board: " + player.getPositionOnBoard());
        System.out.println("money after: " + player.getBalance());
    }
    

    public void EndTurnEvent() {
        Player currPlayer = Monopoly_final.getPlayerById(Monopoly_final.currentPlayer);
        int counter = 0;
        if(currPlayer.getBalance() < 0){
            for(int i=0;i < currPlayer.getOwnedTiles().size();++i){
                if(currPlayer.getOwnedTiles().get(i).getType() == TileType.LOT){
                    Lot lot = (Lot)currPlayer.getOwnedTiles().get(i);
                    if(lot.isMortgaged() == true){
                        counter++;
                    }
                }
                if(currPlayer.getOwnedTiles().get(i).getType() == TileType.RAILROAD){
                    RailRoad lot = (RailRoad)currPlayer.getOwnedTiles().get(i);
                    if(lot.isMortgaged() == true){
                        counter++;
                    }
                }
                if(currPlayer.getOwnedTiles().get(i).getType() == TileType.UTILITIES){
                    Utilities lot = (Utilities)currPlayer.getOwnedTiles().get(i);
                    if(lot.isMortgaged() == true){
                        counter++;
                    }
                }
            }
            if(counter < currPlayer.getOwnedTiles().size()){
                JOptionPane.showMessageDialog(Monopoly_final.frame, "You still have Properties that you can Mortgage.");
            }else{
                JOptionPane.showMessageDialog(Monopoly_final.frame, "You declare bankruptcy! It's game over for you.");
                for(int j=0;j<Monopoly_final.board.getTiles().size();++j){
                    if(Monopoly_final.board.getTiles().get(j).getOwnedBy() == currPlayer){
                        Monopoly_final.board.getInfo().get(j).setText("Bank");
                    }
                }
                for(int i=0;i < currPlayer.getOwnedTiles().size();++i){
                    if(currPlayer.getOwnedTiles().get(i).getType() == TileType.UTILITIES){
                        Utilities lot = (Utilities)currPlayer.getOwnedTiles().get(i);
                        for(int j = 0;j < currPlayer.getOwnedTiles().size();++j){
                            lot.setOwnedBy(null);
                        }
                    }
                    if(currPlayer.getOwnedTiles().get(i).getType() == TileType.LOT){
                        Lot lot = (Lot)currPlayer.getOwnedTiles().get(i);
                        for(int j = 0;j < currPlayer.getOwnedTiles().size();++j){
                            lot.setOwnedBy(null);
                            lot.setHotel(0);
                            lot.setHouses(0);
                        }
                    }
                    if(currPlayer.getOwnedTiles().get(i).getType() == TileType.RAILROAD){
                        RailRoad lot = (RailRoad)currPlayer.getOwnedTiles().get(i);
                        for(int j = 0;j < currPlayer.getOwnedTiles().size();++j){
                            lot.setOwnedBy(null);
                        }
                    }
                }
                ArrayList<String> playerNames = new ArrayList<String>();
                for(int i=1;i<=Monopoly_final.players.size();++i){
                    if(Monopoly_final.currentPlayer != i - 1){
                        playerNames.add("Player " + i);
                    }else{
                        playerNames.add("Player " + i + " (out)");
                    }
                }
                String[] contestants = playerNames.toArray(new String[playerNames.size()]);
                jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(contestants));
                currPlayer.setBankrupt(true);
                currPlayer.setBalance(0);
                Monopoly_final.board.getTiles().get(currPlayer.getPositionOnBoard()).getPlayerPanel().get(Monopoly_final.currentPlayer).setBackground(null);
                
                
                if (Monopoly_final.currentPlayer + 1 >= Monopoly_final.players.size()) {
                    Monopoly_final.currentPlayer = 0;
                }else{
                    Monopoly_final.currentPlayer = Monopoly_final.currentPlayer + 1;
            }
                Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).updateGui();
                Monopoly_final.frame.getPlayer1_Name().setText(Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).getName());
                Monopoly_final.frame.getjComboBox1().setSelectedIndex(Monopoly_final.currentPlayer);
                Monopoly_final.frame.getPlayer1_Balance().setText("" + Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).getBalance());
                Monopoly_final.frame.getPlayer1_Color().setBackground(Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).getColor());
                int tmp = 0;
                for(int i=0;i<Monopoly_final.players.size();i++){
                    if(Monopoly_final.players.get(i).isBankrupt()){
                        tmp++;
                    }
                }
                if(tmp == Monopoly_final.players.size()-1){
                    JOptionPane.showMessageDialog(Monopoly_final.frame, "Congratulations " + Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).getName() + "! You have won!");
                    System.exit(0);
                }
            }
        }else{
            if(Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).isInJail()){
                if(Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).getTurnsInJail() > 0){
                    Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).setTurnsInJail(Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).getTurnsInJail() - 1);
                }else{
                    Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).setInJail(false);
                }
            }else{
                Monopoly_final.frame.getPlayer1_InJail().setText("No");
            }

            if (Monopoly_final.currentPlayer + 1 >= Monopoly_final.players.size()) {
                Monopoly_final.currentPlayer = 0;
            } else {
                Monopoly_final.currentPlayer = Monopoly_final.currentPlayer + 1;
            }
            if(Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).isBankrupt()){
                if (Monopoly_final.currentPlayer + 1 >= Monopoly_final.players.size()) {
                Monopoly_final.currentPlayer = 0;
                } else {
                    Monopoly_final.currentPlayer = Monopoly_final.currentPlayer + 1;
                }
            }

            if (Monopoly_final.board.getTiles().get((Monopoly_final.players).get(Monopoly_final.currentPlayer).getPositionOnBoard()).getType().equals(TileType.LOT)){
                Lot lot = (Lot) Monopoly_final.board.getTiles().get((Monopoly_final.players).get(Monopoly_final.currentPlayer).getPositionOnBoard());
                lot.setBuildedInThisRound(false);
            }
            Monopoly_final.players.get(Monopoly_final.currentPlayer).setThrowedYet(false);
            jButton6.setEnabled(false);

            Monopoly_final.frame.getPlayer1_Name().setText(Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).getName());
            Monopoly_final.frame.getjComboBox1().setSelectedIndex(Monopoly_final.currentPlayer);
            Monopoly_final.frame.getPlayer1_Balance().setText("" + Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).getBalance());
            Monopoly_final.frame.getPlayer1_Color().setBackground(Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).getColor());
            System.out.println("Jailed: " + Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).isInJail());
            if(Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).isInJail()){
                Monopoly_final.frame.getPlayer1_InJail().setText("Yes" + "(" + Monopoly_final.getPlayerById(Monopoly_final.currentPlayer).getTurnsInJail() + ")");
                Monopoly_final.frame.getjButton1().setEnabled(false);
                Monopoly_final.frame.getjButton2().setEnabled(false);
                Monopoly_final.frame.getjButton3().setEnabled(false);
                Monopoly_final.frame.getjButton6().setEnabled(true);
            }else{
                Monopoly_final.frame.getPlayer1_InJail().setText("No");
                Monopoly_final.frame.getjButton1().setEnabled(true);
            }
        }
    }
    private javax.swing.JPanel NamePanel_01;
    private javax.swing.JPanel NamePanel_02;
    private javax.swing.JPanel NamePanel_03;
    private javax.swing.JPanel NamePanel_04;
    private javax.swing.JPanel NamePanel_05;
    private javax.swing.JPanel NamePanel_06;
    private javax.swing.JPanel NamePanel_07;
    private javax.swing.JPanel NamePanel_08;
    private javax.swing.JPanel NamePanel_09;
    private javax.swing.JPanel NamePanel_11;
    private javax.swing.JPanel NamePanel_12;
    private javax.swing.JPanel NamePanel_13;
    private javax.swing.JPanel NamePanel_14;
    private javax.swing.JPanel NamePanel_15;
    private javax.swing.JPanel NamePanel_16;
    private javax.swing.JPanel NamePanel_17;
    private javax.swing.JPanel NamePanel_18;
    private javax.swing.JPanel NamePanel_19;
    private javax.swing.JPanel NamePanel_21;
    private javax.swing.JPanel NamePanel_22;
    private javax.swing.JPanel NamePanel_23;
    private javax.swing.JPanel NamePanel_24;
    private javax.swing.JPanel NamePanel_25;
    private javax.swing.JPanel NamePanel_26;
    private javax.swing.JPanel NamePanel_27;
    private javax.swing.JPanel NamePanel_28;
    private javax.swing.JPanel NamePanel_29;
    private javax.swing.JPanel NamePanel_31;
    private javax.swing.JPanel NamePanel_32;
    private javax.swing.JPanel NamePanel_33;
    private javax.swing.JPanel NamePanel_34;
    private javax.swing.JPanel NamePanel_35;
    private javax.swing.JPanel NamePanel_36;
    private javax.swing.JPanel NamePanel_37;
    private javax.swing.JPanel NamePanel_38;
    private javax.swing.JPanel NamePanel_39;
    private javax.swing.JPanel Player0001;
    private javax.swing.JPanel Player0002;
    private javax.swing.JPanel Player0003;
    private javax.swing.JPanel Player0004;
    private javax.swing.JPanel Player0005;
    private javax.swing.JPanel Player0006;
    private javax.swing.JPanel Player0101;
    private javax.swing.JPanel Player0102;
    private javax.swing.JPanel Player0103;
    private javax.swing.JPanel Player0104;
    private javax.swing.JPanel Player0105;
    private javax.swing.JPanel Player0106;
    private javax.swing.JPanel Player0201;
    private javax.swing.JPanel Player0202;
    private javax.swing.JPanel Player0203;
    private javax.swing.JPanel Player0204;
    private javax.swing.JPanel Player0205;
    private javax.swing.JPanel Player0206;
    private javax.swing.JPanel Player0301;
    private javax.swing.JPanel Player0302;
    private javax.swing.JPanel Player0303;
    private javax.swing.JPanel Player0304;
    private javax.swing.JPanel Player0305;
    private javax.swing.JPanel Player0306;
    private javax.swing.JPanel Player0401;
    private javax.swing.JPanel Player0402;
    private javax.swing.JPanel Player0403;
    private javax.swing.JPanel Player0404;
    private javax.swing.JPanel Player0405;
    private javax.swing.JPanel Player0406;
    private javax.swing.JPanel Player0501;
    private javax.swing.JPanel Player0502;
    private javax.swing.JPanel Player0503;
    private javax.swing.JPanel Player0504;
    private javax.swing.JPanel Player0505;
    private javax.swing.JPanel Player0506;
    private javax.swing.JPanel Player0601;
    private javax.swing.JPanel Player0602;
    private javax.swing.JPanel Player0603;
    private javax.swing.JPanel Player0604;
    private javax.swing.JPanel Player0605;
    private javax.swing.JPanel Player0606;
    private javax.swing.JPanel Player0701;
    private javax.swing.JPanel Player0702;
    private javax.swing.JPanel Player0703;
    private javax.swing.JPanel Player0704;
    private javax.swing.JPanel Player0705;
    private javax.swing.JPanel Player0706;
    private javax.swing.JPanel Player0801;
    private javax.swing.JPanel Player0802;
    private javax.swing.JPanel Player0803;
    private javax.swing.JPanel Player0804;
    private javax.swing.JPanel Player0805;
    private javax.swing.JPanel Player0806;
    private javax.swing.JPanel Player0901;
    private javax.swing.JPanel Player0902;
    private javax.swing.JPanel Player0903;
    private javax.swing.JPanel Player0904;
    private javax.swing.JPanel Player0905;
    private javax.swing.JPanel Player0906;
    private javax.swing.JPanel Player1;
    private javax.swing.JPanel Player1001;
    private javax.swing.JPanel Player1002;
    private javax.swing.JPanel Player1003;
    private javax.swing.JPanel Player1004;
    private javax.swing.JPanel Player1005;
    private javax.swing.JPanel Player1006;
    private javax.swing.JPanel Player1101;
    private javax.swing.JPanel Player1102;
    private javax.swing.JPanel Player1103;
    private javax.swing.JPanel Player1104;
    private javax.swing.JPanel Player1105;
    private javax.swing.JPanel Player1106;
    private javax.swing.JPanel Player1201;
    private javax.swing.JPanel Player1202;
    private javax.swing.JPanel Player1203;
    private javax.swing.JPanel Player1204;
    private javax.swing.JPanel Player1205;
    private javax.swing.JPanel Player1206;
    private javax.swing.JPanel Player1301;
    private javax.swing.JPanel Player1302;
    private javax.swing.JPanel Player1303;
    private javax.swing.JPanel Player1304;
    private javax.swing.JPanel Player1305;
    private javax.swing.JPanel Player1306;
    private javax.swing.JPanel Player1401;
    private javax.swing.JPanel Player1402;
    private javax.swing.JPanel Player1403;
    private javax.swing.JPanel Player1404;
    private javax.swing.JPanel Player1405;
    private javax.swing.JPanel Player1406;
    private javax.swing.JPanel Player1501;
    private javax.swing.JPanel Player1502;
    private javax.swing.JPanel Player1503;
    private javax.swing.JPanel Player1504;
    private javax.swing.JPanel Player1505;
    private javax.swing.JPanel Player1506;
    private javax.swing.JPanel Player1601;
    private javax.swing.JPanel Player1602;
    private javax.swing.JPanel Player1603;
    private javax.swing.JPanel Player1604;
    private javax.swing.JPanel Player1605;
    private javax.swing.JPanel Player1606;
    private javax.swing.JPanel Player1701;
    private javax.swing.JPanel Player1702;
    private javax.swing.JPanel Player1703;
    private javax.swing.JPanel Player1704;
    private javax.swing.JPanel Player1705;
    private javax.swing.JPanel Player1706;
    private javax.swing.JPanel Player1801;
    private javax.swing.JPanel Player1802;
    private javax.swing.JPanel Player1803;
    private javax.swing.JPanel Player1804;
    private javax.swing.JPanel Player1805;
    private javax.swing.JPanel Player1806;
    private javax.swing.JPanel Player1901;
    private javax.swing.JPanel Player1902;
    private javax.swing.JPanel Player1903;
    private javax.swing.JPanel Player1904;
    private javax.swing.JPanel Player1905;
    private javax.swing.JPanel Player1906;
    private javax.swing.JLabel Player1_Balance;
    private javax.swing.JLabel Player1_BalanceD;
    private javax.swing.JPanel Player1_Color;
    private javax.swing.JLabel Player1_InJail;
    private javax.swing.JLabel Player1_InJailD;
    private javax.swing.JLabel Player1_Name;
    private javax.swing.JLabel Player1_NameD;
    private javax.swing.JPanel Player2001;
    private javax.swing.JPanel Player2002;
    private javax.swing.JPanel Player2003;
    private javax.swing.JPanel Player2004;
    private javax.swing.JPanel Player2005;
    private javax.swing.JPanel Player2006;
    private javax.swing.JPanel Player2101;
    private javax.swing.JPanel Player2102;
    private javax.swing.JPanel Player2103;
    private javax.swing.JPanel Player2104;
    private javax.swing.JPanel Player2105;
    private javax.swing.JPanel Player2106;
    private javax.swing.JPanel Player2201;
    private javax.swing.JPanel Player2202;
    private javax.swing.JPanel Player2203;
    private javax.swing.JPanel Player2204;
    private javax.swing.JPanel Player2205;
    private javax.swing.JPanel Player2206;
    private javax.swing.JPanel Player2301;
    private javax.swing.JPanel Player2302;
    private javax.swing.JPanel Player2303;
    private javax.swing.JPanel Player2304;
    private javax.swing.JPanel Player2305;
    private javax.swing.JPanel Player2306;
    private javax.swing.JPanel Player2401;
    private javax.swing.JPanel Player2402;
    private javax.swing.JPanel Player2403;
    private javax.swing.JPanel Player2404;
    private javax.swing.JPanel Player2405;
    private javax.swing.JPanel Player2406;
    private javax.swing.JPanel Player2501;
    private javax.swing.JPanel Player2502;
    private javax.swing.JPanel Player2503;
    private javax.swing.JPanel Player2504;
    private javax.swing.JPanel Player2505;
    private javax.swing.JPanel Player2506;
    private javax.swing.JPanel Player2601;
    private javax.swing.JPanel Player2602;
    private javax.swing.JPanel Player2603;
    private javax.swing.JPanel Player2604;
    private javax.swing.JPanel Player2605;
    private javax.swing.JPanel Player2606;
    private javax.swing.JPanel Player2701;
    private javax.swing.JPanel Player2702;
    private javax.swing.JPanel Player2703;
    private javax.swing.JPanel Player2704;
    private javax.swing.JPanel Player2705;
    private javax.swing.JPanel Player2706;
    private javax.swing.JPanel Player2801;
    private javax.swing.JPanel Player2802;
    private javax.swing.JPanel Player2803;
    private javax.swing.JPanel Player2804;
    private javax.swing.JPanel Player2805;
    private javax.swing.JPanel Player2806;
    private javax.swing.JPanel Player2901;
    private javax.swing.JPanel Player2902;
    private javax.swing.JPanel Player2903;
    private javax.swing.JPanel Player2904;
    private javax.swing.JPanel Player2905;
    private javax.swing.JPanel Player2906;
    private javax.swing.JPanel Player3001;
    private javax.swing.JPanel Player3002;
    private javax.swing.JPanel Player3003;
    private javax.swing.JPanel Player3004;
    private javax.swing.JPanel Player3005;
    private javax.swing.JPanel Player3006;
    private javax.swing.JPanel Player3101;
    private javax.swing.JPanel Player3102;
    private javax.swing.JPanel Player3103;
    private javax.swing.JPanel Player3104;
    private javax.swing.JPanel Player3105;
    private javax.swing.JPanel Player3106;
    private javax.swing.JPanel Player3201;
    private javax.swing.JPanel Player3202;
    private javax.swing.JPanel Player3203;
    private javax.swing.JPanel Player3204;
    private javax.swing.JPanel Player3205;
    private javax.swing.JPanel Player3206;
    private javax.swing.JPanel Player3301;
    private javax.swing.JPanel Player3302;
    private javax.swing.JPanel Player3303;
    private javax.swing.JPanel Player3304;
    private javax.swing.JPanel Player3305;
    private javax.swing.JPanel Player3306;
    private javax.swing.JPanel Player3401;
    private javax.swing.JPanel Player3402;
    private javax.swing.JPanel Player3403;
    private javax.swing.JPanel Player3404;
    private javax.swing.JPanel Player3405;
    private javax.swing.JPanel Player3406;
    private javax.swing.JPanel Player3501;
    private javax.swing.JPanel Player3502;
    private javax.swing.JPanel Player3503;
    private javax.swing.JPanel Player3504;
    private javax.swing.JPanel Player3505;
    private javax.swing.JPanel Player3506;
    private javax.swing.JPanel Player3601;
    private javax.swing.JPanel Player3602;
    private javax.swing.JPanel Player3603;
    private javax.swing.JPanel Player3604;
    private javax.swing.JPanel Player3605;
    private javax.swing.JPanel Player3606;
    private javax.swing.JPanel Player3701;
    private javax.swing.JPanel Player3702;
    private javax.swing.JPanel Player3703;
    private javax.swing.JPanel Player3704;
    private javax.swing.JPanel Player3705;
    private javax.swing.JPanel Player3706;
    private javax.swing.JPanel Player3801;
    private javax.swing.JPanel Player3802;
    private javax.swing.JPanel Player3803;
    private javax.swing.JPanel Player3804;
    private javax.swing.JPanel Player3805;
    private javax.swing.JPanel Player3806;
    private javax.swing.JPanel Player3901;
    private javax.swing.JPanel Player3902;
    private javax.swing.JPanel Player3903;
    private javax.swing.JPanel Player3904;
    private javax.swing.JPanel Player3905;
    private javax.swing.JPanel Player3906;
    private javax.swing.JPanel SidePanel;
    private javax.swing.JLabel TileName_01;
    private javax.swing.JLabel TileName_02;
    private javax.swing.JLabel TileName_03;
    private javax.swing.JLabel TileName_04;
    private javax.swing.JLabel TileName_05;
    private javax.swing.JLabel TileName_06;
    private javax.swing.JLabel TileName_07;
    private javax.swing.JLabel TileName_08;
    private javax.swing.JLabel TileName_09;
    private javax.swing.JLabel TileName_11;
    private javax.swing.JLabel TileName_12;
    private javax.swing.JLabel TileName_13;
    private javax.swing.JLabel TileName_14;
    private javax.swing.JLabel TileName_15;
    private javax.swing.JLabel TileName_16;
    private javax.swing.JLabel TileName_17;
    private javax.swing.JLabel TileName_18;
    private javax.swing.JLabel TileName_19;
    private javax.swing.JLabel TileName_21;
    private javax.swing.JLabel TileName_22;
    private javax.swing.JLabel TileName_23;
    private javax.swing.JLabel TileName_24;
    private javax.swing.JLabel TileName_25;
    private javax.swing.JLabel TileName_26;
    private javax.swing.JLabel TileName_27;
    private javax.swing.JLabel TileName_28;
    private javax.swing.JLabel TileName_29;
    private javax.swing.JLabel TileName_31;
    private javax.swing.JLabel TileName_32;
    private javax.swing.JLabel TileName_33;
    private javax.swing.JLabel TileName_34;
    private javax.swing.JLabel TileName_35;
    private javax.swing.JLabel TileName_36;
    private javax.swing.JLabel TileName_37;
    private javax.swing.JLabel TileName_38;
    private javax.swing.JLabel TileName_39;
    private javax.swing.JPanel Tile_00;
    private javax.swing.JPanel Tile_01;
    private javax.swing.JPanel Tile_02;
    private javax.swing.JPanel Tile_03;
    private javax.swing.JPanel Tile_04;
    private javax.swing.JPanel Tile_05;
    private javax.swing.JPanel Tile_06;
    private javax.swing.JPanel Tile_07;
    private javax.swing.JPanel Tile_08;
    private javax.swing.JPanel Tile_09;
    private javax.swing.JPanel Tile_10;
    private javax.swing.JPanel Tile_11;
    private javax.swing.JPanel Tile_12;
    private javax.swing.JPanel Tile_13;
    private javax.swing.JPanel Tile_14;
    private javax.swing.JPanel Tile_15;
    private javax.swing.JPanel Tile_16;
    private javax.swing.JPanel Tile_17;
    private javax.swing.JPanel Tile_18;
    private javax.swing.JPanel Tile_19;
    private javax.swing.JPanel Tile_20;
    private javax.swing.JPanel Tile_21;
    private javax.swing.JPanel Tile_22;
    private javax.swing.JPanel Tile_23;
    private javax.swing.JPanel Tile_24;
    private javax.swing.JPanel Tile_25;
    private javax.swing.JPanel Tile_26;
    private javax.swing.JPanel Tile_27;
    private javax.swing.JPanel Tile_28;
    private javax.swing.JPanel Tile_29;
    private javax.swing.JPanel Tile_30;
    private javax.swing.JPanel Tile_31;
    private javax.swing.JPanel Tile_32;
    private javax.swing.JPanel Tile_33;
    private javax.swing.JPanel Tile_34;
    private javax.swing.JPanel Tile_35;
    private javax.swing.JPanel Tile_36;
    private javax.swing.JPanel Tile_37;
    private javax.swing.JPanel Tile_38;
    private javax.swing.JPanel Tile_39;
    private javax.swing.JPanel insied_area;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel72;
    private javax.swing.JLabel jLabel73;
    private javax.swing.JLabel jLabel74;
    private javax.swing.JLabel jLabel75;
    private javax.swing.JLabel jLabel76;
    private javax.swing.JLabel jLabel77;
    private javax.swing.JLabel jLabel78;
    private javax.swing.JLabel jLabel79;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel80;
    private javax.swing.JLabel jLabel81;
    private javax.swing.JLabel jLabel82;
    private javax.swing.JLabel jLabel83;
    private javax.swing.JLabel jLabel84;
    private javax.swing.JLabel jLabel85;
    private javax.swing.JLabel jLabel86;
    private javax.swing.JLabel jLabel87;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel27;
    private javax.swing.JPanel jPanel28;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9_ok;
    private javax.swing.JLabel throwDiceLabel;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JMenuBar jMenuBar;
    private javax.swing.JMenu file, infoBar;
    private javax.swing.JMenuItem new_game, exit, vip, info;
    // End of variables declaration                   
}
