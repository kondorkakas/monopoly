package view;

/**
 * <p>TileType.java - Enumerátor egy mező típusának meghatározására.</p>
 * 
 * <p>Lehetséges mező típusok:</p>
 * <ul>
*       <li>Utility</li>
 *      <li>Lot</li>
 *      <li>Chance</li>
 *      <li>CommunityChest</li>
 *      <li>Jail</li>
 *      <li>Go to jail</li>
 *      <li>Free park</li>
 *      <li>Start</li>
 *      <li>Rail road</li>
 *      <li>Tax</li>
 * </ul>
 *
 * @author Osbáth Gergely
 */
public enum TileType {
    UTILITIES,
    LOT,
    CHANCE,
    COMMUNITYCHEST,
    JAIL,
    GOTOJAIL,
    FREEPARK,
    START,
    RAILROAD,
    TAX
}
