/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import model.Lot;
import model.Player;
import monopoly_final.Monopoly_final;

/**
 * <p>CheatFrame.java - "Csaláshoz" szükséges ablakot jeleníti meg.</p>
 *
 * <p>Fő komponensei, alkotóelemei:</p>
 * <ul>
 *  <li>JComboBox: Három combobox található ezen a framen:
 *      <ul>
 *          <li>Melyik játékos?</li>
 *          <li>Melyik telkét?</li>
 *          <li>Kinek szeretné eladni?</li>
 *      </ul>
 * </li>
 *  <li>JTextField: Ide írható be az összeg, amiben a két játékos megegyezett</li>
 * </ul>
 *
 * @author Osbáth Gergely
 */
public class BuySellFrame extends JFrame {

    private javax.swing.JComboBox<String> buyerComboBox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField price;
    private javax.swing.JComboBox<String> sellerComboBox;
    private javax.swing.JButton submitButton;
    private javax.swing.JComboBox<String> tileComboBox;

    public BuySellFrame() {

        jPanel1 = new javax.swing.JPanel();
        sellerComboBox = new javax.swing.JComboBox<>();
        buyerComboBox = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        tileComboBox = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        price = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        submitButton = new javax.swing.JButton();

        setPreferredSize(new java.awt.Dimension(330, 250));

        jPanel1.setPreferredSize(new java.awt.Dimension(330, 250));

        sellerComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]{Monopoly_final.players.get(Monopoly_final.currentPlayer).getName()}));

        String[] playerNames;
        playerNames = new String[Monopoly_final.players.size()];
        for (int i = 0; i < Monopoly_final.players.size(); i++) {
            if (!(Monopoly_final.players.get(Monopoly_final.currentPlayer).getName()).equals((Monopoly_final.players.get(i).getName()))) {
                playerNames[i] = Monopoly_final.players.get(i).getName();
            }
        }

        buyerComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(playerNames));
        sellerComboBox.setPreferredSize(new java.awt.Dimension(105, 20));
        sellerComboBox.setEnabled(false);
        buyerComboBox.setPreferredSize(new java.awt.Dimension(105, 20));

        jLabel1.setText("Seller:");

        jLabel2.setText("Buyer:");

        refreshOwnedTiles();
        tileComboBox.setPreferredSize(new java.awt.Dimension(105, 20));
        tileComboBox.setMaximumRowCount(40);
        jLabel3.setText("Tile:");

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);

        price.setPreferredSize(new java.awt.Dimension(105, 20));

        jLabel4.setText("Price");

        submitButton.setText("Submit");
        submitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                int pBuyer = (int) buyerComboBox.getSelectedIndex();
                int pTileIndex = (int) tileComboBox.getSelectedIndex();
                if (!price.getText().equals("")) {
                    if (pTileIndex != -1) {
                        if (pBuyer != -1) {
                            int a = Monopoly_final.players.get(Monopoly_final.currentPlayer).getOwnedTiles().indexOf(Monopoly_final.board.getTiles().get(pTileIndex));
                            Monopoly_final.players.get(pBuyer).decreaseMoney(Integer.parseInt(price.getText()));
                            Monopoly_final.players.get(pBuyer).getOwnedTiles().add(Monopoly_final.board.getTiles().get(pTileIndex));
                            Monopoly_final.players.get(Monopoly_final.currentPlayer).getOwnedTiles().remove(a);
                            Monopoly_final.players.get(Monopoly_final.currentPlayer).increaseMoney(Integer.parseInt(price.getText()));
                            refreshOwnedTiles();
                            Monopoly_final.players.get(Monopoly_final.currentPlayer).updateGui();
                            Monopoly_final.board.getTiles().get(pTileIndex).setOwnedBy(Monopoly_final.players.get(pBuyer));
                            Monopoly_final.board.getInfo().get(pTileIndex).setText(Monopoly_final.players.get(pBuyer).getName());
                            tileGroupCheck(Monopoly_final.players.get(pBuyer), Monopoly_final.board.getTiles().get(pTileIndex));
                        } else {
                            JOptionPane.showMessageDialog(Monopoly_final.frame, "You have to select a buyer to sell");
                        }
                    } else {
                        JOptionPane.showMessageDialog(Monopoly_final.frame, "You have to select a tile to sell");
                    }
                } else {
                    JOptionPane.showMessageDialog(Monopoly_final.frame, "You have to add a price to sell");
                }
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(31, 31, 31)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(sellerComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                                .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(tileComboBox, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                .addGap(47, 47, 47)
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(buyerComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(jLabel4)
                                                        .addComponent(price, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                                .addComponent(submitButton)
                                                .addGap(96, 96, 96)))
                                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel1)
                                        .addComponent(jLabel2))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(sellerComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(buyerComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(45, 45, 45)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel3)
                                        .addComponent(jLabel4))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(tileComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(price, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(submitButton)
                                .addContainerGap(54, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0))
        );

        pack();
        setVisible(true);
    }

    private void refreshOwnedTiles() {
        String[] playerOwnedTiles;
        playerOwnedTiles = new String[40];
        for (int i = 0; i < 40; i++) {
            for (int j = 0; j < Monopoly_final.players.get(Monopoly_final.currentPlayer).getOwnedTiles().size(); j++) {
                if (Monopoly_final.board.getTiles().get(i).getName().equals(Monopoly_final.players.get(Monopoly_final.currentPlayer).getOwnedTiles().get(j).getName())) {
                    playerOwnedTiles[i] = Monopoly_final.board.getTiles().get(i).getName();
                }
            }
        }
        tileComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(playerOwnedTiles));
    }

    private void tileGroupCheck(Player buyer, Tile boughtTile) {
        Lot boughtLot = null, tempLot = null;
        int counter = 0;
        if (boughtTile.getType().equals(TileType.LOT)) {
            boughtLot = (Lot) boughtTile;
            for (int i = 0; i < buyer.getOwnedTiles().size(); i++) {
                if (buyer.getOwnedTiles().get(i).getType().equals(TileType.LOT)) {
                    tempLot = (Lot) buyer.getOwnedTiles().get(i);
                    if (boughtLot.getGroup().equals(tempLot.getGroup())) {
                        counter += 1;
                    }
                }
            }
            if (counter == 2 && (boughtLot.getGroup().equals(TileGroup.G1) || boughtLot.getGroup().equals(TileGroup.G8))) {
                for (int i = 0; i < buyer.getOwnedTiles().size(); i++) {
                    if (buyer.getOwnedTiles().get(i).getType().equals(TileType.LOT)) {
                        tempLot = (Lot) buyer.getOwnedTiles().get(i);
                        if (boughtLot.getGroup().equals(tempLot.getGroup())) {
                            tempLot.setCanBuild(true);
                        }
                    }
                }
            } else if (counter == 3 && (tempLot.getGroup().equals(TileGroup.G2) || tempLot.getGroup().equals(TileGroup.G3)
                    || tempLot.getGroup().equals(TileGroup.G4) || tempLot.getGroup().equals(TileGroup.G5)
                    || tempLot.getGroup().equals(TileGroup.G6) || tempLot.getGroup().equals(TileGroup.G7))) {
                for (int i = 0; i < buyer.getOwnedTiles().size(); i++) {
                    if (buyer.getOwnedTiles().get(i).getType().equals(TileType.LOT)) {
                        tempLot = (Lot) buyer.getOwnedTiles().get(i);
                        if (boughtLot.getGroup().equals(tempLot.getGroup())) {
                            tempLot.setCanBuild(true);
                        }
                    }
                }
            } else {
                for (int i = 0; i < buyer.getOwnedTiles().size(); i++) {
                    if (buyer.getOwnedTiles().get(i).getType().equals(TileType.LOT)) {
                        tempLot = (Lot) buyer.getOwnedTiles().get(i);
                        if (boughtLot.getGroup().equals(tempLot.getGroup())) {
                            tempLot.setCanBuild(false);
                        }
                    }
                }
            }
        }
        for (int i = 0; i < Monopoly_final.players.get(Monopoly_final.currentPlayer).getOwnedTiles().size(); i++) {
            if (Monopoly_final.players.get(Monopoly_final.currentPlayer).getOwnedTiles().get(i).getType().equals(TileType.LOT)) {
                tempLot = (Lot) Monopoly_final.players.get(Monopoly_final.currentPlayer).getOwnedTiles().get(i);
                if (boughtLot.getGroup().equals(tempLot.getGroup())) {
                    tempLot.setCanBuild(false);
                }
            }
        }
    }
}
