/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.util.ArrayList;
import javax.swing.JLabel;
import model.Lot;
import model.OtherTiles;
import model.RailRoad;
import model.Utilities;

/**
 *<p>Board.java - A játéktábla osztálya.</p>
 * 
 *<p>Ennek az osztálynak a segítségével rendelünk hozzá Tile objektumokat a MonopolyFrame-en elhelyezkedő JPanelekhez.</p>
 * 
 * 
 * @see Tile
 * 
 * @author Osbáth Gergely
 */
public class Board {

    private ArrayList<Tile> tiles;
    private ArrayList<JLabel> info;
    private ArrayList<JLabel> houseNumber;
    
    /**
     * Az osztály konstruktora: létrehozza a táblát, inicializálja a táblához szükséges tömböket.
     * @param frame Megadjuk a konstruktornak, hogy 'melyik' framere hozza létre nekünk a táblát.
     */
    public Board(MonopolyFrame frame) {
        tiles = new ArrayList<Tile>();
        info = new ArrayList<JLabel>();
        houseNumber = new ArrayList<JLabel>();
        initializeBoard(frame);
    }

    public ArrayList<JLabel> getHouseNumber() {
        return houseNumber;
    }

    public ArrayList<Tile> getTiles() {
        return tiles;
    }
    
    public ArrayList<JLabel> getInfo(){
        return info;
    }
    /**
     * Ez a függvény felel azért, hogy minden panel egy Tile objektumnak feleljen meg és a paneleken belül létrehozza
     * az egyes játékosok panelját is.
     * @param frame Megadjuk a függvénynek, hogy 'melyik' framere hozza létre nekünk a mezőket.
     */
    private void initializeBoard(MonopolyFrame frame) {
        Tile tile00 = new OtherTiles("Start", TileType.START);
        tile00.getPlayerPanel().add(frame.getPlayer0001());
        tile00.getPlayerPanel().add(frame.getPlayer0002());
        tile00.getPlayerPanel().add(frame.getPlayer0003());
        tile00.getPlayerPanel().add(frame.getPlayer0004());
        tile00.getPlayerPanel().add(frame.getPlayer0005());
        tile00.getPlayerPanel().add(frame.getPlayer0006());
        
        info.add(new JLabel(""));

        Tile tile01 = new Lot("Mediterranean Avenue",60,2,TileType.LOT, TileGroup.G1,30);
        tile01.getPlayerPanel().add(frame.getPlayer0101());
        tile01.getPlayerPanel().add(frame.getPlayer0102());
        tile01.getPlayerPanel().add(frame.getPlayer0103());
        tile01.getPlayerPanel().add(frame.getPlayer0104());
        tile01.getPlayerPanel().add(frame.getPlayer0105());
        tile01.getPlayerPanel().add(frame.getPlayer0106());
        
        info.add(frame.getjLabel3());

        Tile tile02 = new OtherTiles("Community Chest", TileType.COMMUNITYCHEST);
        tile02.getPlayerPanel().add(frame.getPlayer0201());
        tile02.getPlayerPanel().add(frame.getPlayer0202());
        tile02.getPlayerPanel().add(frame.getPlayer0203());
        tile02.getPlayerPanel().add(frame.getPlayer0204());
        tile02.getPlayerPanel().add(frame.getPlayer0205());
        tile02.getPlayerPanel().add(frame.getPlayer0206());
        
        info.add(new JLabel(""));
        
        Tile tile03 = new Lot("Baltic Avenue",60,4,TileType.LOT, TileGroup.G1,30);
        tile03.getPlayerPanel().add(frame.getPlayer0301());
        tile03.getPlayerPanel().add(frame.getPlayer0302());
        tile03.getPlayerPanel().add(frame.getPlayer0303());
        tile03.getPlayerPanel().add(frame.getPlayer0304());
        tile03.getPlayerPanel().add(frame.getPlayer0305());
        tile03.getPlayerPanel().add(frame.getPlayer0306());
        
        info.add(frame.getjLabel6());
        
        Tile tile04 = new OtherTiles("Tax", TileType.TAX);
        tile04.getPlayerPanel().add(frame.getPlayer0401());
        tile04.getPlayerPanel().add(frame.getPlayer0402());
        tile04.getPlayerPanel().add(frame.getPlayer0403());
        tile04.getPlayerPanel().add(frame.getPlayer0404());
        tile04.getPlayerPanel().add(frame.getPlayer0405());
        tile04.getPlayerPanel().add(frame.getPlayer0406());
        
        info.add(new JLabel(""));
        
        Tile tile05 = new RailRoad("Reading Railroad",200,TileType.RAILROAD);
        tile05.getPlayerPanel().add(frame.getPlayer0501());
        tile05.getPlayerPanel().add(frame.getPlayer0502());
        tile05.getPlayerPanel().add(frame.getPlayer0503());
        tile05.getPlayerPanel().add(frame.getPlayer0504());
        tile05.getPlayerPanel().add(frame.getPlayer0505());
        tile05.getPlayerPanel().add(frame.getPlayer0506());
        
        info.add(frame.getjLabel9());
        
        Tile tile06 = new Lot("Oriental Avenue",100,6,TileType.LOT, TileGroup.G2,50);
        tile06.getPlayerPanel().add(frame.getPlayer0601());
        tile06.getPlayerPanel().add(frame.getPlayer0602());
        tile06.getPlayerPanel().add(frame.getPlayer0603());
        tile06.getPlayerPanel().add(frame.getPlayer0604());
        tile06.getPlayerPanel().add(frame.getPlayer0605());
        tile06.getPlayerPanel().add(frame.getPlayer0606());
        
        info.add(frame.getjLabel12());
        
        Tile tile07 = new OtherTiles("Chance", TileType.CHANCE);
        tile07.getPlayerPanel().add(frame.getPlayer0701());
        tile07.getPlayerPanel().add(frame.getPlayer0702());
        tile07.getPlayerPanel().add(frame.getPlayer0703());
        tile07.getPlayerPanel().add(frame.getPlayer0704());
        tile07.getPlayerPanel().add(frame.getPlayer0705());
        tile07.getPlayerPanel().add(frame.getPlayer0706());
        
        info.add(new JLabel(""));
        
        Tile tile08 = new Lot("Vermont Avenue",100,6,TileType.LOT, TileGroup.G2,50);
        tile08.getPlayerPanel().add(frame.getPlayer0801());
        tile08.getPlayerPanel().add(frame.getPlayer0802());
        tile08.getPlayerPanel().add(frame.getPlayer0803());
        tile08.getPlayerPanel().add(frame.getPlayer0804());
        tile08.getPlayerPanel().add(frame.getPlayer0805());
        tile08.getPlayerPanel().add(frame.getPlayer0806());
        
        info.add(frame.getjLabel15());
        
        Tile tile09 = new Lot("Connecticut Avenue",120,8,TileType.LOT, TileGroup.G2,60);
        tile09.getPlayerPanel().add(frame.getPlayer0901());
        tile09.getPlayerPanel().add(frame.getPlayer0902());
        tile09.getPlayerPanel().add(frame.getPlayer0903());
        tile09.getPlayerPanel().add(frame.getPlayer0904());
        tile09.getPlayerPanel().add(frame.getPlayer0905());
        tile09.getPlayerPanel().add(frame.getPlayer0906());
        
        info.add(frame.getjLabel18());
        
        Tile tile10 = new OtherTiles("Jail", TileType.JAIL);
        tile10.getPlayerPanel().add(frame.getPlayer1001());
        tile10.getPlayerPanel().add(frame.getPlayer1002());
        tile10.getPlayerPanel().add(frame.getPlayer1003());
        tile10.getPlayerPanel().add(frame.getPlayer1004());
        tile10.getPlayerPanel().add(frame.getPlayer1005());
        tile10.getPlayerPanel().add(frame.getPlayer1006());
        
        info.add(new JLabel(""));
        
        Tile tile11 = new Lot("St. Charles Place",140,10,TileType.LOT, TileGroup.G3,70);
        tile11.getPlayerPanel().add(frame.getPlayer1101());
        tile11.getPlayerPanel().add(frame.getPlayer1102());
        tile11.getPlayerPanel().add(frame.getPlayer1103());
        tile11.getPlayerPanel().add(frame.getPlayer1104());
        tile11.getPlayerPanel().add(frame.getPlayer1105());
        tile11.getPlayerPanel().add(frame.getPlayer1106());
        
        info.add(frame.getjLabel21());
        
        Tile tile12 = new Utilities("Electric Company",150,TileType.UTILITIES);
        tile12.getPlayerPanel().add(frame.getPlayer1201());
        tile12.getPlayerPanel().add(frame.getPlayer1202());
        tile12.getPlayerPanel().add(frame.getPlayer1203());
        tile12.getPlayerPanel().add(frame.getPlayer1204());
        tile12.getPlayerPanel().add(frame.getPlayer1205());
        tile12.getPlayerPanel().add(frame.getPlayer1206());
        
        info.add(frame.getjLabel24());
        
        Tile tile13 = new Lot("States Avenue",140,10,TileType.LOT, TileGroup.G3,70);
        tile13.getPlayerPanel().add(frame.getPlayer1301());
        tile13.getPlayerPanel().add(frame.getPlayer1302());
        tile13.getPlayerPanel().add(frame.getPlayer1303());
        tile13.getPlayerPanel().add(frame.getPlayer1304());
        tile13.getPlayerPanel().add(frame.getPlayer1305());
        tile13.getPlayerPanel().add(frame.getPlayer1306());
        
        info.add(frame.getjLabel84());
        
        Tile tile14 = new Lot("Virginia Avenue",160,12,TileType.LOT, TileGroup.G3,80);
        tile14.getPlayerPanel().add(frame.getPlayer1401());
        tile14.getPlayerPanel().add(frame.getPlayer1402());
        tile14.getPlayerPanel().add(frame.getPlayer1403());
        tile14.getPlayerPanel().add(frame.getPlayer1404());
        tile14.getPlayerPanel().add(frame.getPlayer1405());
        tile14.getPlayerPanel().add(frame.getPlayer1406());
        
        info.add(frame.getjLabel30());
        
        Tile tile15 = new RailRoad("Pennsylvania Railroad",200,TileType.RAILROAD);
        tile15.getPlayerPanel().add(frame.getPlayer1501());
        tile15.getPlayerPanel().add(frame.getPlayer1502());
        tile15.getPlayerPanel().add(frame.getPlayer1503());
        tile15.getPlayerPanel().add(frame.getPlayer1504());
        tile15.getPlayerPanel().add(frame.getPlayer1505());
        tile15.getPlayerPanel().add(frame.getPlayer1506());
        
        info.add(frame.getjLabel33());
        
        Tile tile16 = new Lot("St. James Place",180,14,TileType.LOT, TileGroup.G4,90);
        tile16.getPlayerPanel().add(frame.getPlayer1601());
        tile16.getPlayerPanel().add(frame.getPlayer1602());
        tile16.getPlayerPanel().add(frame.getPlayer1603());
        tile16.getPlayerPanel().add(frame.getPlayer1604());
        tile16.getPlayerPanel().add(frame.getPlayer1605());
        tile16.getPlayerPanel().add(frame.getPlayer1606());
        
        info.add(frame.getjLabel36());
        
        Tile tile17 = new OtherTiles("Community Chest", TileType.COMMUNITYCHEST);
        tile17.getPlayerPanel().add(frame.getPlayer1701());
        tile17.getPlayerPanel().add(frame.getPlayer1702());
        tile17.getPlayerPanel().add(frame.getPlayer1703());
        tile17.getPlayerPanel().add(frame.getPlayer1704());
        tile17.getPlayerPanel().add(frame.getPlayer1705());
        tile17.getPlayerPanel().add(frame.getPlayer1706());
        
        info.add(new JLabel(""));
        
        Tile tile18 = new Lot("Tennessee Avenue",180,14,TileType.LOT, TileGroup.G4,90);
        tile18.getPlayerPanel().add(frame.getPlayer1801());
        tile18.getPlayerPanel().add(frame.getPlayer1802());
        tile18.getPlayerPanel().add(frame.getPlayer1803());
        tile18.getPlayerPanel().add(frame.getPlayer1804());
        tile18.getPlayerPanel().add(frame.getPlayer1805());
        tile18.getPlayerPanel().add(frame.getPlayer1806());
        
        info.add(frame.getjLabel39());
        
        Tile tile19 = new Lot("New York Avenue",200,16,TileType.LOT, TileGroup.G4,100);
        tile19.getPlayerPanel().add(frame.getPlayer1901());
        tile19.getPlayerPanel().add(frame.getPlayer1902());
        tile19.getPlayerPanel().add(frame.getPlayer1903());
        tile19.getPlayerPanel().add(frame.getPlayer1904());
        tile19.getPlayerPanel().add(frame.getPlayer1905());
        tile19.getPlayerPanel().add(frame.getPlayer1906());
        
        info.add(frame.getjLabel42());
        
        Tile tile20 = new OtherTiles("Free Park", TileType.FREEPARK);
        tile20.getPlayerPanel().add(frame.getPlayer2001());
        tile20.getPlayerPanel().add(frame.getPlayer2002());
        tile20.getPlayerPanel().add(frame.getPlayer2003());
        tile20.getPlayerPanel().add(frame.getPlayer2004());
        tile20.getPlayerPanel().add(frame.getPlayer2005());
        tile20.getPlayerPanel().add(frame.getPlayer2006());
        
        info.add(new JLabel(""));
        
        Tile tile21 = new Lot("Kentucky Avenue",220,18,TileType.LOT, TileGroup.G5,110);
        tile21.getPlayerPanel().add(frame.getPlayer2101());
        tile21.getPlayerPanel().add(frame.getPlayer2102());
        tile21.getPlayerPanel().add(frame.getPlayer2103());
        tile21.getPlayerPanel().add(frame.getPlayer2104());
        tile21.getPlayerPanel().add(frame.getPlayer2105());
        tile21.getPlayerPanel().add(frame.getPlayer2106());
        
        info.add(frame.getjLabel45());
        
        Tile tile22 = new OtherTiles("Chance", TileType.CHANCE);
        tile22.getPlayerPanel().add(frame.getPlayer2201());
        tile22.getPlayerPanel().add(frame.getPlayer2202());
        tile22.getPlayerPanel().add(frame.getPlayer2203());
        tile22.getPlayerPanel().add(frame.getPlayer2204());
        tile22.getPlayerPanel().add(frame.getPlayer2205());
        tile22.getPlayerPanel().add(frame.getPlayer2206());
        
        info.add(new JLabel(""));
        
        Tile tile23 = new Lot("Indiana Avenue",220,18,TileType.LOT, TileGroup.G5,110);
        tile23.getPlayerPanel().add(frame.getPlayer2301());
        tile23.getPlayerPanel().add(frame.getPlayer2302());
        tile23.getPlayerPanel().add(frame.getPlayer2303());
        tile23.getPlayerPanel().add(frame.getPlayer2304());
        tile23.getPlayerPanel().add(frame.getPlayer2305());
        tile23.getPlayerPanel().add(frame.getPlayer2306());
        
        info.add(frame.getjLabel48());
        
        Tile tile24 = new Lot("Illinois Avenue",240,20,TileType.LOT, TileGroup.G5,120);
        tile24.getPlayerPanel().add(frame.getPlayer2401());
        tile24.getPlayerPanel().add(frame.getPlayer2402());
        tile24.getPlayerPanel().add(frame.getPlayer2403());
        tile24.getPlayerPanel().add(frame.getPlayer2404());
        tile24.getPlayerPanel().add(frame.getPlayer2405());
        tile24.getPlayerPanel().add(frame.getPlayer2406());
        
        info.add(frame.getjLabel51());
        
        Tile tile25 = new RailRoad("B. & O. Railroad",200,TileType.RAILROAD);
        tile25.getPlayerPanel().add(frame.getPlayer2501());
        tile25.getPlayerPanel().add(frame.getPlayer2502());
        tile25.getPlayerPanel().add(frame.getPlayer2503());
        tile25.getPlayerPanel().add(frame.getPlayer2504());
        tile25.getPlayerPanel().add(frame.getPlayer2505());
        tile25.getPlayerPanel().add(frame.getPlayer2506());
        
        info.add(frame.getjLabel54());
        
        Tile tile26 = new Lot("Atlantic Avenue",260,22,TileType.LOT, TileGroup.G6,130);
        tile26.getPlayerPanel().add(frame.getPlayer2601());
        tile26.getPlayerPanel().add(frame.getPlayer2602());
        tile26.getPlayerPanel().add(frame.getPlayer2603());
        tile26.getPlayerPanel().add(frame.getPlayer2604());
        tile26.getPlayerPanel().add(frame.getPlayer2605());
        tile26.getPlayerPanel().add(frame.getPlayer2606());
        
        info.add(frame.getjLabel57());
        
        Tile tile27 = new Lot("Ventnor Avenue",260,22,TileType.LOT, TileGroup.G6,130);
        tile27.getPlayerPanel().add(frame.getPlayer2701());
        tile27.getPlayerPanel().add(frame.getPlayer2702());
        tile27.getPlayerPanel().add(frame.getPlayer2703());
        tile27.getPlayerPanel().add(frame.getPlayer2704());
        tile27.getPlayerPanel().add(frame.getPlayer2705());
        tile27.getPlayerPanel().add(frame.getPlayer2706());
        
        info.add(frame.getjLabel60());
        
        Tile tile28 = new Utilities("Water works",150,TileType.UTILITIES);
        tile28.getPlayerPanel().add(frame.getPlayer2801());
        tile28.getPlayerPanel().add(frame.getPlayer2802());
        tile28.getPlayerPanel().add(frame.getPlayer2803());
        tile28.getPlayerPanel().add(frame.getPlayer2804());
        tile28.getPlayerPanel().add(frame.getPlayer2805());
        tile28.getPlayerPanel().add(frame.getPlayer2806());
        
        info.add(frame.getjLabel63());
        
        Tile tile29 = new Lot("Marvin Gardens",280,24,TileType.LOT, TileGroup.G6,140);
        tile29.getPlayerPanel().add(frame.getPlayer2901());
        tile29.getPlayerPanel().add(frame.getPlayer2902());
        tile29.getPlayerPanel().add(frame.getPlayer2903());
        tile29.getPlayerPanel().add(frame.getPlayer2904());
        tile29.getPlayerPanel().add(frame.getPlayer2905());
        tile29.getPlayerPanel().add(frame.getPlayer2906());
        
        info.add(frame.getjLabel66());
        
        Tile tile30 = new OtherTiles("Go To Jail", TileType.GOTOJAIL);
        tile30.getPlayerPanel().add(frame.getPlayer3001());
        tile30.getPlayerPanel().add(frame.getPlayer3002());
        tile30.getPlayerPanel().add(frame.getPlayer3003());
        tile30.getPlayerPanel().add(frame.getPlayer3004());
        tile30.getPlayerPanel().add(frame.getPlayer3005());
        tile30.getPlayerPanel().add(frame.getPlayer3006());
        
        info.add(new JLabel(""));
        
        Tile tile31 = new Lot("Pacific Avenue",300,26,TileType.LOT, TileGroup.G7,150);
        tile31.getPlayerPanel().add(frame.getPlayer3101());
        tile31.getPlayerPanel().add(frame.getPlayer3102());
        tile31.getPlayerPanel().add(frame.getPlayer3103());
        tile31.getPlayerPanel().add(frame.getPlayer3104());
        tile31.getPlayerPanel().add(frame.getPlayer3105());
        tile31.getPlayerPanel().add(frame.getPlayer3106());
        
        info.add(frame.getjLabel87());
        
        Tile tile32 = new Lot("North Carolina Avenue",300,26,TileType.LOT, TileGroup.G7,150);
        tile32.getPlayerPanel().add(frame.getPlayer3201());
        tile32.getPlayerPanel().add(frame.getPlayer3202());
        tile32.getPlayerPanel().add(frame.getPlayer3203());
        tile32.getPlayerPanel().add(frame.getPlayer3204());
        tile32.getPlayerPanel().add(frame.getPlayer3205());
        tile32.getPlayerPanel().add(frame.getPlayer3206());
        
        info.add(frame.getjLabel69());
        
        Tile tile33 = new OtherTiles("Community Chest", TileType.COMMUNITYCHEST);
        tile33.getPlayerPanel().add(frame.getPlayer3301());
        tile33.getPlayerPanel().add(frame.getPlayer3302());
        tile33.getPlayerPanel().add(frame.getPlayer3303());
        tile33.getPlayerPanel().add(frame.getPlayer3304());
        tile33.getPlayerPanel().add(frame.getPlayer3305());
        tile33.getPlayerPanel().add(frame.getPlayer3306());
        
        info.add(new JLabel(""));
        
        Tile tile34 = new Lot("Pennsylvania Avenue",320,28,TileType.LOT, TileGroup.G7,160);
        tile34.getPlayerPanel().add(frame.getPlayer3401());
        tile34.getPlayerPanel().add(frame.getPlayer3402());
        tile34.getPlayerPanel().add(frame.getPlayer3403());
        tile34.getPlayerPanel().add(frame.getPlayer3404());
        tile34.getPlayerPanel().add(frame.getPlayer3405());
        tile34.getPlayerPanel().add(frame.getPlayer3406());
        
        info.add(frame.getjLabel72());
        
        Tile tile35 = new RailRoad("Short Line",200,TileType.RAILROAD);
        tile35.getPlayerPanel().add(frame.getPlayer3501());
        tile35.getPlayerPanel().add(frame.getPlayer3502());
        tile35.getPlayerPanel().add(frame.getPlayer3503());
        tile35.getPlayerPanel().add(frame.getPlayer3504());
        tile35.getPlayerPanel().add(frame.getPlayer3505());
        tile35.getPlayerPanel().add(frame.getPlayer3506());
        
        info.add(frame.getjLabel75());
        
        Tile tile36 = new OtherTiles("Chane", TileType.CHANCE);
        tile36.getPlayerPanel().add(frame.getPlayer3601());
        tile36.getPlayerPanel().add(frame.getPlayer3602());
        tile36.getPlayerPanel().add(frame.getPlayer3603());
        tile36.getPlayerPanel().add(frame.getPlayer3604());
        tile36.getPlayerPanel().add(frame.getPlayer3605());
        tile36.getPlayerPanel().add(frame.getPlayer3606());
        
        info.add(new JLabel(""));
        
        Tile tile37 = new Lot("Park Place",350,35,TileType.LOT, TileGroup.G8,175);
        tile37.getPlayerPanel().add(frame.getPlayer3701());
        tile37.getPlayerPanel().add(frame.getPlayer3702());
        tile37.getPlayerPanel().add(frame.getPlayer3703());
        tile37.getPlayerPanel().add(frame.getPlayer3704());
        tile37.getPlayerPanel().add(frame.getPlayer3705());
        tile37.getPlayerPanel().add(frame.getPlayer3706());
        
        info.add(frame.getjLabel78());
        
        Tile tile38 = new OtherTiles("Tax", TileType.TAX);
        tile38.getPlayerPanel().add(frame.getPlayer3801());
        tile38.getPlayerPanel().add(frame.getPlayer3802());
        tile38.getPlayerPanel().add(frame.getPlayer3803());
        tile38.getPlayerPanel().add(frame.getPlayer3804());
        tile38.getPlayerPanel().add(frame.getPlayer3805());
        tile38.getPlayerPanel().add(frame.getPlayer3806());
        
        info.add(new JLabel(""));
        
        Tile tile39 = new Lot("Boardwalk",400,50,TileType.LOT, TileGroup.G8,200);
        tile39.getPlayerPanel().add(frame.getPlayer3901());
        tile39.getPlayerPanel().add(frame.getPlayer3902());
        tile39.getPlayerPanel().add(frame.getPlayer3903());
        tile39.getPlayerPanel().add(frame.getPlayer3904());
        tile39.getPlayerPanel().add(frame.getPlayer3905());
        tile39.getPlayerPanel().add(frame.getPlayer3906());
        
        info.add(frame.getjLabel81());
        
        houseNumber.add(new JLabel(""));            //start
        houseNumber.add(frame.getjLabel2());
        houseNumber.add(new JLabel(""));            //community chest
        houseNumber.add(frame.getjLabel5());
        houseNumber.add(new JLabel(""));            //income tax
        houseNumber.add(frame.getjLabel8());        //reading railroad
        houseNumber.add(frame.getjLabel11());
        houseNumber.add(new JLabel(""));            //chance
        houseNumber.add(frame.getjLabel14());
        houseNumber.add(frame.getjLabel17());
        houseNumber.add(new JLabel(""));            //jail
        houseNumber.add(frame.getjLabel20());
        houseNumber.add(frame.getjLabel23());      //electric company
        houseNumber.add(frame.getjLabel83());
        houseNumber.add(frame.getjLabel29());
        houseNumber.add(frame.getjLabel32());
        houseNumber.add(frame.getjLabel35());
        houseNumber.add(new JLabel(""));            //community chest
        houseNumber.add(frame.getjLabel38());
        houseNumber.add(frame.getjLabel41());
        houseNumber.add(new JLabel(""));            //free park
        houseNumber.add(frame.getjLabel44());
        houseNumber.add(new JLabel(""));            //chance
        houseNumber.add(frame.getjLabel47());
        houseNumber.add(frame.getjLabel50());
        houseNumber.add(frame.getjLabel53());       //b&q railroad
        houseNumber.add(frame.getjLabel56());
        houseNumber.add(frame.getjLabel59());
        houseNumber.add(frame.getjLabel62());       //water works
        houseNumber.add(frame.getjLabel65());
        houseNumber.add(new JLabel(""));            //goto jail
        houseNumber.add(frame.getjLabel86());
        houseNumber.add(frame.getjLabel68());
        houseNumber.add(new JLabel(""));            //community chest
        houseNumber.add(frame.getjLabel71());
        houseNumber.add(frame.getjLabel74());       //short line
        houseNumber.add(new JLabel(""));            //chance
        houseNumber.add(frame.getjLabel77());
        houseNumber.add(new JLabel(""));            //luxury tax
        houseNumber.add(frame.getjLabel80());
                
        tiles.add(tile00);
        tiles.add(tile01);
        tiles.add(tile02);
        tiles.add(tile03);
        tiles.add(tile04);
        tiles.add(tile05);
        tiles.add(tile06);
        tiles.add(tile07);
        tiles.add(tile08);
        tiles.add(tile09);
        tiles.add(tile10);
        tiles.add(tile11);
        tiles.add(tile12);
        tiles.add(tile13);
        tiles.add(tile14);
        tiles.add(tile15);
        tiles.add(tile16);
        tiles.add(tile17);
        tiles.add(tile18);
        tiles.add(tile19);
        tiles.add(tile20);
        tiles.add(tile21);
        tiles.add(tile22);
        tiles.add(tile23);
        tiles.add(tile24);
        tiles.add(tile25);
        tiles.add(tile26);
        tiles.add(tile27);
        tiles.add(tile28);
        tiles.add(tile29);
        tiles.add(tile30);
        tiles.add(tile31);
        tiles.add(tile32);
        tiles.add(tile33);
        tiles.add(tile34);
        tiles.add(tile35);
        tiles.add(tile36);
        tiles.add(tile37);
        tiles.add(tile38);
        tiles.add(tile39);
    }
}
