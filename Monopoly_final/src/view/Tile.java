/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.util.ArrayList;
import javax.swing.JPanel;
import model.Player;

/**
 * <p>Tile.java - Mező osztálya.</p>
 *
 * @author Osbáth Gergely
 */
public class Tile {

    private ArrayList<JPanel> playerPanel;
    private TileType type;
    private static int counter = 0;
    private String name;

    private Player ownedBy;

/**
*Az osztály konstruktora: beállítja a típust, létrehoz neki egy játékosPanel tömböt, és a tulajdonosát kezdeti null-ra állítja.
*@param type Egy TileType-ot (Mezőtípust) vár bemenő paraméterként, ez alapján hozza létre a Tile-t (mezőt).
*/  public Tile(TileType type) {
        this.type = type;
        playerPanel = new ArrayList<>();
        this.ownedBy = null;
    }

    public ArrayList<JPanel> getPlayerPanel() {
        return this.playerPanel;
    }

    public TileType getType() {
        return this.type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Player getOwnedBy() {
        return ownedBy;
    }

    public void setOwnedBy(Player player) {
        this.ownedBy = player;
    }
}
