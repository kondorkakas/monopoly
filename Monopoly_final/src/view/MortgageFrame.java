/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import model.Lot;
import model.RailRoad;
import model.Utilities;
import monopoly_final.Monopoly_final;

/**
 * <p>MortGageFrame.java - létrehozza a jelzálog kölcsön igényléséhez/törlesztéséhez szükséges ablakot.</p>
 *
 * <p> Fő komponensei, alkotóelemei:</p>
 * <ul>
 *  <li>JComboBox: 2 combobox (legördülő menü):
 *      <ul>
 *          <li>Az aktuális player.</li>
 *          <li>A jelzálogba kívánt adni/visszavásárolni kívánt telek</li>
 *      </ul>
 *  </li>
 * </ul>
 *
 * @author Osbáth Gergely
 */
public class MortgageFrame extends JFrame {

    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JComboBox<String> playerComboBox;
    private javax.swing.JButton submitButton;
    private javax.swing.JComboBox<String> tileComboBox;

    public MortgageFrame() {

        jPanel1 = new javax.swing.JPanel();
        playerComboBox = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        tileComboBox = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        submitButton = new javax.swing.JButton();

        setPreferredSize(new java.awt.Dimension(200, 250));

        jPanel1.setPreferredSize(new java.awt.Dimension(200, 250));

        playerComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]{Monopoly_final.players.get(Monopoly_final.currentPlayer).getName()}));

        playerComboBox.setPreferredSize(new java.awt.Dimension(105, 20));
        playerComboBox.setEnabled(false);

        jLabel1.setText("Player:");

        refreshOwnedTiles();
        tileComboBox.setPreferredSize(new java.awt.Dimension(105, 20)); // this can be changed to 200 so you can see price
        tileComboBox.setMaximumRowCount(40);
        jLabel3.setText("Tiles:");

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);

        submitButton.setText("Submit");
        submitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Tile tile = Monopoly_final.board.getTiles().get((tileComboBox.getSelectedIndex()));
                if (tile.getOwnedBy() == Monopoly_final.players.get(Monopoly_final.currentPlayer)) {
                    if (tile.getType() == TileType.LOT) {
                        Lot lot = (Lot) tile;
                        if (lot.isMortgaged() == false) {
                            lot.setMortgaged(true);
                            lot.setHouses(0);
                            lot.setHotel(0);
                            Monopoly_final.players.get(Monopoly_final.currentPlayer).increaseMoney(lot.getMortgageValue());
                        } else {
                            lot.setMortgaged(false);
                            Monopoly_final.players.get(Monopoly_final.currentPlayer).decreaseMoney(lot.getMortgageValue() * 1.10);
                        }
                    }
                    if (tile.getType() == TileType.RAILROAD) {
                        RailRoad lot = (RailRoad) tile;
                        if (lot.isMortgaged() == false) {
                            lot.setMortgaged(true);
                            Monopoly_final.players.get(Monopoly_final.currentPlayer).increaseMoney(lot.getMortgageValue());
                        } else {
                            lot.setMortgaged(false);
                            Monopoly_final.players.get(Monopoly_final.currentPlayer).decreaseMoney(lot.getMortgageValue() * 1.10);
                        }
                    }
                    if (tile.getType() == TileType.UTILITIES) {
                        Utilities lot = (Utilities) tile;
                        if (lot.isMortgaged() == false) {
                            lot.setMortgaged(true);
                            Monopoly_final.players.get(Monopoly_final.currentPlayer).increaseMoney(lot.getMortgageValue());
                        } else {
                            lot.setMortgaged(false);
                            Monopoly_final.players.get(Monopoly_final.currentPlayer).decreaseMoney(lot.getMortgageValue() * 1.10);
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(Monopoly_final.frame, "You don't own this Tile.");
                }
                Monopoly_final.players.get(Monopoly_final.currentPlayer).updateGui();
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(31, 31, 31)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(playerComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                                .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(tileComboBox, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                .addGap(67, 67, 67))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                                .addComponent(submitButton)
                                                .addGap(67, 67, 67)))
                                .addContainerGap(31, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel1))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(playerComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(25, 25, 25)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel3))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(tileComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(25, 25, 25)
                                .addComponent(submitButton)
                                .addContainerGap(54, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0))
        );

        pack();
        setVisible(true);
    }

    private void refreshOwnedTiles() {
        String[] playerOwnedTiles;
        playerOwnedTiles = new String[40];
        for (int i = 0; i < 40; i++) {
            for (int j = 0; j < Monopoly_final.players.get(Monopoly_final.currentPlayer).getOwnedTiles().size(); j++) {
                if (Monopoly_final.board.getTiles().get(i).getName().equals(Monopoly_final.players.get(Monopoly_final.currentPlayer).getOwnedTiles().get(j).getName())) {
                    if (Monopoly_final.board.getTiles().get(i).getType() == TileType.LOT) {
                        Lot lot = (Lot) Monopoly_final.board.getTiles().get(i);
                        playerOwnedTiles[i] = lot.getName() + " (" + lot.getMortgageValue() + ")";
                    }
                    if (Monopoly_final.board.getTiles().get(i).getType() == TileType.RAILROAD) {
                        RailRoad lot = (RailRoad) Monopoly_final.board.getTiles().get(i);
                        playerOwnedTiles[i] = lot.getName() + " (" + lot.getMortgageValue() + ")";
                    }
                    if (Monopoly_final.board.getTiles().get(i).getType() == TileType.UTILITIES) {
                        Utilities lot = (Utilities) Monopoly_final.board.getTiles().get(i);
                        playerOwnedTiles[i] = lot.getName() + " (" + lot.getMortgageValue() + ")";
                    }
                }
            }
        }
        tileComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(playerOwnedTiles));
        tileComboBox.setRenderer(new BasicComboBoxRenderer() {
            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                if (isSelected) {
                    setBackground(list.getSelectionBackground());
                    setForeground(list.getSelectionForeground());
                    if (index > -1) {
                        if (Monopoly_final.board.getTiles().get(index).getType() == TileType.LOT) {
                            Lot lot = (Lot) Monopoly_final.board.getTiles().get(index);
                            list.setToolTipText("Mortgage Value: " + lot.getMortgageValue() + "Mortgaged: " + lot.isMortgaged());
                        }
                        if (Monopoly_final.board.getTiles().get(index).getType() == TileType.RAILROAD) {
                            RailRoad lot = (RailRoad) Monopoly_final.board.getTiles().get(index);
                            list.setToolTipText("Mortgage Value: " + lot.getMortgageValue() + "Mortgaged: " + lot.isMortgaged());
                        }
                        if (Monopoly_final.board.getTiles().get(index).getType() == TileType.UTILITIES) {
                            Utilities lot = (Utilities) Monopoly_final.board.getTiles().get(index);
                            list.setToolTipText("Mortgage Value: " + lot.getMortgageValue() + "Mortgaged: " + lot.isMortgaged());
                        }
                    }
                } else {
                    setBackground(list.getBackground());
                    setForeground(list.getForeground());
                }
                setFont(list.getFont());
                setText((value == null) ? "" : value.toString());

                return this;
            }
        });
    }

}
