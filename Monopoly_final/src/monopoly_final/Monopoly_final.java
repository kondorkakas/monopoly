/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly_final;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import model.Card;
import model.ChanceCard;
import model.CommunityChestCard;
import model.Lot;
import model.Player;
import view.*;

/**
 *<p>Monopoly_final.java - A program belépési pontja</p>
 * 
 * <p>Adattagjai:</p>
 * <ul>
 *  <li>gameOver: vége van-e?</li>
 *  <li>players: a játékosokat tároló tömb</li>
 *  <li>DeckOfChanceCards: szerencsekártya pakli</li>
 *  <li>DeckOfDiscardedChanceCards: a felhasznált szerencsekártyákat tartalmazó pakli</li>
 *  <li>DeckOfCommunityCards: meglepetéskártya pakli</li>
 *  <li>DeckOfDiscardedCommunityCards: a felhasznált meglepetéskártyákat tartalmazó pakli</li>
 *  <li>board: a játéktábla</li>
 *  <li>frame: a játék ablaka</li>
 *  <li>currentPlayer: az aktuálisan soronlévő játékos</li>
 * </ul>
 * 
 * @author Osbáth Gergely
 */
public class Monopoly_final {

    private boolean gameOver;
    public static ArrayList<Player> players;
    public static ArrayList<Card> DeckOfChanceCards;
    public static ArrayList<Card> DeckOfDiscardedChanceCards;
    public static ArrayList<Card> DeckOfCommunityChestCards;
    public static ArrayList<Card> DeckOfDiscardedCommunityChestCards;
    public static Board board;
    public static MonopolyFrame frame;
    public static int currentPlayer = 0;
    
    //public static Player bank;
    
    /**
     * Az osztály konstruktora.
     *<p>Itt állítjuk be a játékosok számát, nevüket, inicializáljuk a paklikat</p>
     */
    public Monopoly_final() {
        
        players = new ArrayList<Player>();
        
        int numberOfPlayers;
        numberOfPlayers = Integer.parseInt(JOptionPane.showInputDialog("How many players?"));
        
        for(int i=0;i<numberOfPlayers;++i){
            String s = JOptionPane.showInputDialog("Player " + Integer.toString(i+1) + " name: ");
            players.add(new Player(s));
        }

        frame = new MonopolyFrame();
        board = new Board(frame);
        
        DeckOfChanceCards = new ArrayList<Card>();
        DeckOfDiscardedChanceCards = new ArrayList<Card>();
        DeckOfCommunityChestCards = new ArrayList<Card>();
        DeckOfDiscardedCommunityChestCards = new ArrayList<Card>();
        
        this.gameOver = false;
       
        Monopoly_final.frame.getPlayer1_Balance().setText("" + players.get(0).getBalance());
        Monopoly_final.frame.getPlayer1_Name().setText(players.get(0).getName());
        Monopoly_final.frame.getPlayer1_InJail().setText("No");
        
            
        for(Player p : players){
            board.getTiles().get(0).getPlayerPanel().get(p.getPlayerID()).setBackground(p.getColor());
        }
        
        initChanceDeck();
        initCommunityChestDeck();
        
                
        frame.setVisible(true);
    }
            
        
    public static Player getPlayerById(int id){
        for(Player p : players){
           if(id == p.getPlayerID())
               return p;
        }
        return null;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public boolean getGameOver() {
        return this.gameOver;
    }
    
    public static void newGame(){
        Monopoly_final.frame.setVisible(false);
        Monopoly_final mf = new Monopoly_final();
    }

    public void setGameOver() {
        this.gameOver = false;
    }
    
    public void initChanceDeck(){

        Monopoly_final.DeckOfChanceCards.add(new ChanceCard("Go to Start.",0,0));
        Monopoly_final.DeckOfChanceCards.add(new ChanceCard("Bank pays you dividend of $50.",-1,50));
        Monopoly_final.DeckOfChanceCards.add(new ChanceCard("Advance to Illinois Avenue. If you pass Start, collect 200$",24,0));
        Monopoly_final.DeckOfChanceCards.add(new ChanceCard("Advance to St. Charles Place. If you pass Start, collect 200$",11,0));
        Monopoly_final.DeckOfChanceCards.add(new ChanceCard("Advance token to nearest Utility. If unowned, you may buy it from the Bank. If owned, throw dice and pay owner a total 10 times the amount thrown.",-1,0));
        Monopoly_final.DeckOfChanceCards.add(new ChanceCard("Advance token to the nearest Railroad and pay owner twice the rental to which he/she {he} is otherwise entitled. If Railroad is unowned, you may buy it from the Bank.",-1,0));
        Monopoly_final.DeckOfChanceCards.add(new ChanceCard("Go back 3 spaces.",-1,0));
        Monopoly_final.DeckOfChanceCards.add(new ChanceCard("Get out of Jail Free. This card may be kept until needed, or traded/sold.",-1,0));
        Monopoly_final.DeckOfChanceCards.add(new ChanceCard("Go to Jail. Go directly to Jail. Do not pass GO, do not collect $200.",10,0)); // lehet hogy addol 200-at a startért
        Monopoly_final.DeckOfChanceCards.add(new ChanceCard("Make general repairs on all your property: For each house pay $25, For each hotel $100.",-1,0)); //TODO: még addolni kell a speciális effectet.
        Monopoly_final.DeckOfChanceCards.add(new ChanceCard("Pay poor tax of $15",-1,-15));
        Monopoly_final.DeckOfChanceCards.add(new ChanceCard("Take a trip to Reading Railroad. If you pass Go, collect $200.",5,0));
        Monopoly_final.DeckOfChanceCards.add(new ChanceCard("Take a walk on the Boardwalk. Advance token to Boardwalk.",39,0));
        Monopoly_final.DeckOfChanceCards.add(new ChanceCard("You have been elected Chairman of the Board. Pay each player $50.",-1,0));
        Monopoly_final.DeckOfChanceCards.add(new ChanceCard("Your building and loan matures. Receive $150.",-1,150));
        Monopoly_final.DeckOfChanceCards.add(new ChanceCard("You have won a crossword competition. Collect $100.",-1,100));
        
        Collections.shuffle(DeckOfChanceCards);
    }
    
        public void initCommunityChestDeck(){

        Monopoly_final.DeckOfCommunityChestCards.add(new CommunityChestCard("Go to Start.",0,0));
        Monopoly_final.DeckOfCommunityChestCards.add(new CommunityChestCard("Bank error in your favor. Collect $200.",-1,200));
        Monopoly_final.DeckOfCommunityChestCards.add(new CommunityChestCard("Doctor's fees. Pay $50.",-1,-50));
        Monopoly_final.DeckOfCommunityChestCards.add(new CommunityChestCard("From sale of stock you get $50.",-1,50));
        Monopoly_final.DeckOfCommunityChestCards.add(new CommunityChestCard("Get out of Jail Free. This card may be kept until needed, or traded/sold.",-1,0));
        Monopoly_final.DeckOfCommunityChestCards.add(new CommunityChestCard("Grand Opera Night. Collect $50 from every player for opening night seats.",-1,0));
        Monopoly_final.DeckOfCommunityChestCards.add(new CommunityChestCard("Holiday Fund matures. Receive $100.",-1,100));
        Monopoly_final.DeckOfCommunityChestCards.add(new CommunityChestCard("Go to Jail. Go directly to Jail. Do not pass GO, do not collect $200.",10,0)); // lehet hogy addol 200-at a startért
        Monopoly_final.DeckOfCommunityChestCards.add(new CommunityChestCard("Income tax refund. Collect $20.",-1,20));
        Monopoly_final.DeckOfCommunityChestCards.add(new CommunityChestCard("It is your birthday. Collect $10 from every player.",-1,0));
        Monopoly_final.DeckOfCommunityChestCards.add(new CommunityChestCard("Life insurance matures – Collect $100",-1,100));
        Monopoly_final.DeckOfCommunityChestCards.add(new CommunityChestCard("Hospital Fees. Pay $50.",-1,-50));
        Monopoly_final.DeckOfCommunityChestCards.add(new CommunityChestCard("School fees. Pay $50.",-1,-50));
        Monopoly_final.DeckOfCommunityChestCards.add(new CommunityChestCard("Receive $25 consultancy fee.",-1,25));
        Monopoly_final.DeckOfCommunityChestCards.add(new CommunityChestCard("You are assessed for street repairs: Pay $40 per house and $115 per hotel you own.",-1,0));
        Monopoly_final.DeckOfCommunityChestCards.add(new CommunityChestCard("You have won second prize in a beauty contest. Collect $10.",-1,10));
        Monopoly_final.DeckOfCommunityChestCards.add(new CommunityChestCard("You inherit $100.",-1,100));

        Collections.shuffle(DeckOfCommunityChestCards);
    }

    public static void main(String[] args) {
        
        Monopoly_final mf = new Monopoly_final();
        
    }

}
